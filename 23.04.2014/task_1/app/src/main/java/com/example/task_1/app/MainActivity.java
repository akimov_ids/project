package com.example.task_1.app;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Button butCall = (Button) findViewById(R.id.button2);
        butCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Здесь используется мой реальный номер//
                //Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:0995614981"));
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:0995614981"));
                startActivity(intent);
            }
        });

        Button butSendSMS = (Button) findViewById(R.id.button);
        butSendSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentSms = new Intent(Intent.ACTION_SENDTO, Uri.parse("sms:0995614981"));
                intentSms.putExtra("sms_body", "Massage content");
                startActivity(intentSms);

            }
        });

        Button butSendMail = (Button) findViewById(R.id.button3);
        butSendMail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, "akimov_n.n@mail.ru");
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Subject");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT,"My massage content");
                startActivity(emailIntent);

            }
        });

        Button butOpenWeb = (Button) findViewById(R.id.button4);
        butOpenWeb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intentWeb = new Intent(Intent.ACTION_VIEW);
                intentWeb.setData(Uri.parse("https://www.google.com.ua/"));
                startActivity(intentWeb);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
