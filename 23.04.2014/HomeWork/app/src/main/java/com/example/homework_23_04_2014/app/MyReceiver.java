package com.example.homework_23_04_2014.app;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Николай on 21.04.14.
 */
public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MY_TAG", "RECEIVER");
        intent = new Intent(context,SecondActivity.class);
        try{
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);

        }catch(Exception ex){
            Log.d("MY_TAG", "RECEIVER_error = "+ex.getMessage());
        }

    }
}
