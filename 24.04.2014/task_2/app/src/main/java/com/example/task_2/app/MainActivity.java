package com.example.task_2.app;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

public class MainActivity extends ActionBarActivity {
    private TextView textView;
    public Calculator calk;
    public EditText editText;
    Button butCalc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = (TextView) findViewById(R.id.textView);
        editText = (EditText) findViewById(R.id.editText);
        //calk = new Calculator();
        //calk.execute(20);
        butCalc = (Button)findViewById(R.id.button);
        butCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //textView = (TextView) findViewById(R.id.textView);

                calk = new Calculator();
                calk.execute(Long.parseLong(editText.getText().toString()));
            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class Calculator extends AsyncTask<Long, Void, Long> {
        private Long result;// = 1;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //tvInfo.setText("Begin");
            result = 1l;
        }

        @Override
        protected Long doInBackground(Long... params) {
             // Log.d("MY_TAG", "RES = "+params[0]);
            //textView = (TextView) findViewById(R.id.textView);
            //textView.setText("Result");
            try {
                //TimeUnit.SECONDS.sleep(2);
                for(int i=1; i<=params[0]; i++) {
                    result = result*i;
                    //textView.setText("Result = "+result);
                    //myHandler.sendEmptyMessage(m);
                    //Thread.sleep(100);
                    //TimeUnit.MILLISECONDS.sleep(100);

                }
            } catch (Exception e) {
                //e.printStackTrace();
                //Log.d("MY_TAG", "Errror ="+e.getMessage());
            }
            return result;
        }

        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
             textView.setText("Result = "+result);
        }
    }


    //====

    /*class Calculatorr extends AsyncTask<void, void, void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            tvInfo.setText("Begin");
        }

        @Override
        protected Void doInBackground(Void params) {
            try {
                //TimeUnit.SECONDS.sleep(2);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
            return;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            tvInfo.setText("End");
        }
    }*/


}
