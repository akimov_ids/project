package com.example.task3.app;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity  implements LoaderManager.LoaderCallbacks<String> {

    final String LOG_TAG = "myLogs";
    static final int LOADER_TIME_ID = 1;

    TextView textView;
    EditText editText;
    Button butCalc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView);
        editText = (EditText) findViewById(R.id.editText);
        butCalc = (Button) findViewById(R.id.button);
        butCalc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Loader<String> loader;
                Bundle bndl = new Bundle();
                try{
                    Log.d(LOG_TAG, "EDIT TEXT: " + editText.getText().toString());
                    long nm = Long.parseLong(editText.getText().toString());
                    bndl.putLong(MyLoader.NUMBER, nm);
                    getSupportLoaderManager().initLoader(LOADER_TIME_ID, bndl, MainActivity.this);
                }catch(Exception ex){
                    Log.d(LOG_TAG, "Loader error: " + ex.getMessage());
                }

                loader = getSupportLoaderManager().getLoader(LOADER_TIME_ID);
                loader.forceLoad();
            }
        });
}

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        Loader<String> loader = null;
        if (id == LOADER_TIME_ID) {
            loader = new MyLoader(this, args);
            Log.d(LOG_TAG, "onCreateLoader: " + loader.hashCode());
        }
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String result) {
        Log.d(LOG_TAG, "onLoadFinished for loader " + loader.hashCode()
                + ", result = " + result);
        textView.setText(result);
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
        Log.d(LOG_TAG, "onLoaderReset for loader " + loader.hashCode());
    }

    public void getTimeClick(View v) {
        Loader<String> loader;
        loader = getSupportLoaderManager().getLoader(LOADER_TIME_ID);
        loader.forceLoad();
    }

    public void observerClick(View v) {
    }

}