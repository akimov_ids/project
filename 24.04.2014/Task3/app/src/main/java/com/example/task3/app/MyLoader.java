package com.example.task3.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;

/**
 * Created by Николай on 20.04.14.
 */
public class MyLoader extends AsyncTaskLoader<String> {

    final String LOG_TAG = "myLogs";
    final int PAUSE = 10;
    public final static String NUMBER = "number_for_calc";
    private long number;
    String format;

    public MyLoader(Context context, Bundle args) {
        super(context);
        this.number = args.getLong(NUMBER);
        Log.d(LOG_TAG, hashCode() + " create TimeAsyncLoader");
   }

    @Override
    public String loadInBackground() {
        long result = 1;
        for(int i=1;i<this.number;i++){
            Log.d("LOG_TAG", hashCode() + " loadInBackground start");
            result *= i;
        }

        return ""+result;
    }

}