package com.example.task_1.app;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

//import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class MainActivity extends ActionBarActivity {
    private TextView textView;
    private EditText editText;
    public int value;
    public Button butCalc;
    private Handler myHandler = new Handler(Looper.getMainLooper()) {



        @Override
        public void handleMessage(Message inputMessage) {
            textView.setText("Result "+inputMessage.what);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.textView2);
        editText = (EditText) findViewById(R.id.editText);


        butCalc = (Button) findViewById(R.id.button);
        butCalc.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                MainActivity.this.value = Integer.parseInt(editText.getText().toString());

                Thread myThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        int result = MainActivity.this.value;
                        for(int i=1; i<=10; i++) {
                            result = result*i;
                            //myHandler.sendEmptyMessage(m);
                            try{
                                Thread.sleep(400);
                                myHandler.sendEmptyMessage(result);
                            }catch (Exception ex){}
                        }
                    }
                });
                myThread.start();
            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
