package com.example.myapplication.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements View.OnClickListener {
    private Intent intent;

    public final static String MY_KEY = "result";
    public final static int REQUEST_CODE_ONE = 1;
    public final static int REQUEST_CODE_TWO = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button butGo = (Button) findViewById(R.id.button);
        butGo.setOnClickListener(this);

        Button butGoRequestTwo = (Button) findViewById(R.id.button2);
        butGoRequestTwo.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        TextView textView = (TextView ) findViewById(R.id.textView);
        if(resultCode == RESULT_OK){

            switch(requestCode) {
                case REQUEST_CODE_ONE:
                    textView.setText("RESULT = REQUEST_CODE_ONE "+data.getStringExtra(MY_KEY));
                    break;
                case REQUEST_CODE_TWO:
                    textView.setText("RESULT = REQUEST_CODE_TWO "+data.getStringExtra(MY_KEY));
                    break;
            }
        }

        //textView.setText("RESULT = "+data.getStringExtra(MY_KEY));

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        switch(view.getId()) {
            case R.id.button:
                intent = new Intent(MainActivity.this, SecondActivity.class);
                startActivityForResult(intent, REQUEST_CODE_ONE);
                break;
            case R.id.button2:
                intent = new Intent(MainActivity.this, SecondActivityRequestTwo.class);
                startActivityForResult(intent, REQUEST_CODE_TWO);
                break;
        }


    }

}
