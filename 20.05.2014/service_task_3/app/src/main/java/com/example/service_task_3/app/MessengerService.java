package com.example.service_task_3.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Nikolay on 21.05.14.
 */
public class MessengerService  extends Service {

    NotificationManager mNM;
    ArrayList<Messenger> mClients = new ArrayList<Messenger>();
    int mValue = 0;

    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    static final int MSG_SET_VALUE = 3;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    Log.d("MY_TAG", "REGISTER");
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                case MSG_SET_VALUE:
                    //=
                    mValue = msg.arg1;
                    Log.d("my","Valuew = "+mValue);
                    //mValue = 8;
                    new Thread(new Runnable() {
                        //int mmValue = 4;
                        @Override
                        public void run() {

                            mValue = fact(mValue);
                            /*for(int i=1; i<=20; i++) {
                                mValue = mValue*i;
                                try{
                                    Thread.sleep(400);
                                }catch (InterruptedException ex){}
                                //Log.d(LOG_TAG, "RUN ="+i);
                            }*/

                            for (int i=mClients.size()-1; i>=0; i--) {
                                try {
                                    mClients.get(i).send(Message.obtain(null,MSG_SET_VALUE, mValue, 0));
                                } catch (RemoteException e) {
                                    mClients.remove(i);
                                }
                            }
                        }
                    }).start();

                    //=


                    break;
                default:
                    super.handleMessage(msg);
            }
        }
    }

    final Messenger mMessenger = new Messenger(new IncomingHandler());

    @Override
    public void onCreate() {
        /*mNM = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        showNotification();*/
    }

    int fact(int N) {
        if(N < 0) return 0;
        if (N == 0) return 1;
        else return N * fact(N - 1);
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
        //mNM.cancel(R.string.remote_service_started);
        Toast.makeText(this, R.string.remote_service_stopped, Toast.LENGTH_SHORT).show();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }
}