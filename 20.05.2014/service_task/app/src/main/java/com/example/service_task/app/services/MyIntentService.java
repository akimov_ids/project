package com.example.service_task.app.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.util.Log;

import com.example.service_task.app.MainActivity;

import java.util.concurrent.TimeUnit;

/**
 * Created by Николай on 28.05.14.
 */
public class MyIntentService  extends IntentService {

    final String LOG_TAG = "myLogs";
    PendingIntent pendingIntent;

    public MyIntentService() {
        super("myname");
    }

    public void onCreate() {
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        long number = intent.getLongExtra(MainActivity.KEY_NUMBER, -1);
        pendingIntent = intent.getParcelableExtra(MainActivity.KEY_PINTENT);

        number = fact(number);
        try{
            pendingIntent.send(MyIntentService.this,MyService.TASK_CODE,new Intent().putExtra(MyService.RESULT_CODE,number));
        }catch (PendingIntent.CanceledException ex){
            ex.printStackTrace();
        }
    }

    long fact(long N) {
        if(N < 0) return 0;
        if (N == 0) return 1;
        else return N * fact(N - 1);
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "onDestroy");
    }
}