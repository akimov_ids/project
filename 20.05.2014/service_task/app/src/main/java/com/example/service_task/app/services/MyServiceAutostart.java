package com.example.service_task.app.services;

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.service_task.app.MainActivity;
import com.example.service_task.app.MyBroadReceiv;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nikolay on 20.05.14.
 */
public class MyServiceAutostart  extends Service {

    final String LOG_TAG = "myLogs";
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        long number = 1 + (int)(Math.random() * ((10 - 1) + 1));

        MyRun mr = new MyRun(number);
        es.execute(mr);
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {
        long number;

        public MyRun(long number) {
            this.number = number;
        }

        public void run() {
            Intent intent = new Intent("calib");
            number = fact(number);
            intent.putExtra(MyBroadReceiv.KEY_ACTION_AUTO_START,1);
            intent.putExtra(MainActivity.KEY_NUMBER,number);

            sendBroadcast(intent);
        }

        long fact(long N) {
            if(N < 0) return 0;
            if (N == 0) return 1;
            else return N * fact(N - 1);
        }
    }
}
