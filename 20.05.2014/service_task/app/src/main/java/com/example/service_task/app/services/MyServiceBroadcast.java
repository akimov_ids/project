package com.example.service_task.app.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.service_task.app.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Nikolay on 20.05.14.
 */
public class MyServiceBroadcast extends Service {

    final String LOG_TAG = "myLogs";
    ExecutorService es;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(2);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        //long number = intent.getLongExtra(MainActivity.KEY_NUMBER, 1);
        long number = intent.getLongExtra(MainActivity.KEY_NUMBER, 1);

        MyRun mr = new MyRun(number);
        es.execute(mr);
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {

        long number;

        public MyRun(long number) {
            this.number = number;
        }

        public void run() {
            Intent intent = new Intent(MainActivity.REG_BROADCAST_SERVICE);
            Log.d("DS","ALARM");
            number = fact(number);

            intent.putExtra(MainActivity.ACTION_BROADCASST,MainActivity.GET_RESULT_SERVICE);
            intent.putExtra(MainActivity.RESULT_NUMBER,number);
            sendBroadcast(intent);
        }

        long fact(long N) {
            if(N < 0) return 0;
            if (N == 0) return 1;
            else return N * fact(N - 1);
        }
    }
}
