package com.example.service_task.app.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.service_task.app.MainActivity;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Николай on 17.05.14.
 */
public class MyService  extends Service {

    public final static String LOG_TAG = "myLogs";

    public final static int TASK_CODE = 24;
    public final static String RESULT_CODE = "result";

    ExecutorService es;
    PendingIntent pendingIntent;
    Object someRes;

    public void onCreate() {
        super.onCreate();
        es = Executors.newFixedThreadPool(1);
        someRes = new Object();
    }

    public void onDestroy() {
        super.onDestroy();
        someRes = null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        pendingIntent = intent.getParcelableExtra(MainActivity.KEY_PINTENT);
        long number = intent.getLongExtra(MainActivity.KEY_NUMBER, 1);
        MyRun mr = new MyRun(number,pendingIntent);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        return null;
    }

    class MyRun implements Runnable {
        long number;
        PendingIntent pendingIntent;

        public MyRun(long number, PendingIntent pendingIntent) {
            this.number = number;
            this.pendingIntent = pendingIntent;
        }

        public void run() {
            number = fact(number);
            try{

                pendingIntent.send(MyService.this,TASK_CODE,new Intent().putExtra(RESULT_CODE,number));
                //pendingIntent.send();
            }catch (PendingIntent.CanceledException ex){
                ex.printStackTrace();
            }
            stop();
        }

        long fact(long N) {
            if(N < 0) return 0;
            if (N == 0) return 1;
            else return N * fact(N - 1);
        }

        void stop() {
            stopSelf();
        }
    }
}
