package com.example.service_task.app;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.service_task.app.services.MyIntentService;
import com.example.service_task.app.services.MyService;
import com.example.service_task.app.services.MyServiceBroadcast;
import com.example.service_task.app.services.MyServiceLocalBind;

public class MainActivity extends ActionBarActivity {

    public static final String KEY_NUMBER = "numberrr";
    public static final String RESULT_NUMBER = "result_number";
    public static final String KEY_PINTENT = "pintent";
    public static final String ACTION_BROADCASST = "action_broadcast";

    public static final int START_SERVICE = 0;
    public static final int GET_RESULT_SERVICE = 1;
    public static final int AUTOSTART_SERVICE = 2;

    public static final String REG_BROADCAST_SERVICE = "0";
    public final int REQUEST_CALC_FACTORIAL = 14;

    Button butStartService;
    Button butStartLocalBind;
    Button butStartAlarm;
    Button butIntentService;

    Intent intent;
    Intent intentLocalBind;
    ServiceConnection sConn;
    PendingIntent pendingIntent;
    MyServiceLocalBind myServiceLocalBind;
    BroadcastReceiver br;

    EditText editText;
    TextView textView;

    long number;
    boolean bound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        intent = new Intent("com.example.service_task.app.services.MyService");
        pendingIntent = createPendingResult(REQUEST_CALC_FACTORIAL,new Intent(),0);

        editText = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.textView2);
        butStartService = (Button) findViewById(R.id.button);
        butIntentService = (Button) findViewById(R.id.button4);

        intentLocalBind = new Intent(MainActivity.this, MyServiceLocalBind.class);

        br = new BroadcastReceiver() {
            long result;
            public void onReceive(Context context, Intent intent) {

               switch (intent.getIntExtra(ACTION_BROADCASST,-1)){
                   case START_SERVICE:
                       Log.d(MyService.LOG_TAG, "MyBroadcastReceiver_Start");
                       startService(new Intent(MainActivity.this, MyServiceBroadcast.class).putExtra(KEY_NUMBER, number));
                       break;
                   case GET_RESULT_SERVICE:
                       result = intent.getLongExtra(RESULT_NUMBER,1);
                       textView.setText(""+result);
                       break;
               }
            }
        };

        sConn = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(MyService.LOG_TAG, "MainActivity onServiceConnected");
                myServiceLocalBind = ((MyServiceLocalBind.MyBinder) binder).getService();
                bound = true;
            }
            public void onServiceDisconnected(ComponentName name) {
                Log.d(MyService.LOG_TAG, "MainActivity onServiceDisconnected");
                bound = false;
            }
        };

        butStartService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String value = editText.getText().toString();
                if(!isNumber(value)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Enter correct data!")
                           .setCancelable(false)
                           .setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    number = Long.parseLong(editText.getText().toString());
                    startService(new Intent(MainActivity.this, MyService.class).putExtra(KEY_NUMBER,
                            number).putExtra(KEY_PINTENT,pendingIntent));
                }
            }
        });

        butStartLocalBind = (Button) findViewById(R.id.button2);
        butStartLocalBind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(MyService.LOG_TAG,"CLICK_LOCAL_BIND");
                String value = editText.getText().toString();

                if(!isNumber(value)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Enter correct data!")
                            .setCancelable(false)
                            .setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    long result = myServiceLocalBind.calcFactorial(Long.parseLong(value));
                    textView.setText(""+result);
                }
           }
        });


        butStartAlarm = (Button) findViewById(R.id.button3);
        butStartAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IntentFilter intFilt = new IntentFilter(REG_BROADCAST_SERVICE);
                registerReceiver(br, intFilt);

                String value = editText.getText().toString();
                if(!isNumber(value)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Enter correct data!")
                            .setCancelable(false)
                            .setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    AlarmManager am=(AlarmManager)getSystemService(Context.ALARM_SERVICE);
                    Intent intent = new Intent(REG_BROADCAST_SERVICE);

                    number = Long.parseLong(editText.getText().toString());
                    intent.putExtra(ACTION_BROADCASST, START_SERVICE);
                    PendingIntent pi = PendingIntent.getBroadcast(MainActivity.this, 0, intent, 0);
                    am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()+10000, 0 , pi);
                }
            }
        });


        butIntentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String value = editText.getText().toString();

                if(!isNumber(value)){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage("Enter correct data!")
                            .setCancelable(false)
                            .setNegativeButton("OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else{
                    Intent intent = new Intent(MainActivity.this, MyIntentService.class);
                    intent.putExtra(KEY_NUMBER,Long.parseLong(editText.getText().toString())).
                            putExtra(KEY_PINTENT,pendingIntent);
                    startService(intent);
                }

            }
        });
        bindService(intentLocalBind, sConn, BIND_AUTO_CREATE);
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    public boolean isNumber(String value) {

        try {
            Long.parseLong(value);

        } catch (NumberFormatException ex) {
            return false;
        }
        if(value.charAt(0)=='0'&&value.length()>0)
            return false;

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CALC_FACTORIAL)
            if(resultCode == MyService.TASK_CODE)
                textView.setText("Result = "+data.getLongExtra(MyService.RESULT_CODE, 0));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
