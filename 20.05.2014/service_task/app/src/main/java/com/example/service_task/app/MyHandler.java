package com.example.service_task.app;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Nikolay on 20.05.14.
 */
public class MyHandler extends Handler{
    @Override
    public void close() {}

    @Override
    public void flush() {}

    @Override
    public void publish(LogRecord logRecord) {}
}
