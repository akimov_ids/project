package com.example.service_task.app;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import com.example.service_task.app.services.MyService;
import com.example.service_task.app.services.MyServiceAutostart;
import com.example.service_task.app.services.MyServiceBroadcast;

/**
 * Created by Nikolay on 20.05.14.
 */
public class MyBroadReceiv extends BroadcastReceiver {

    NotificationManager nm;
    final String LOG_TAG = "myLogs";
    public static final String KEY_ACTION_AUTO_START = "KEY_ACTION_AUTO_START";
    public static final String REG_BROADCAST_AUTO = "REG_BROADCAST_AUTO";

    public void onReceive(Context context, Intent intent) {
        Log.d(LOG_TAG, "onReceive " + intent.getAction());

        switch(intent.getIntExtra(KEY_ACTION_AUTO_START,-1)){
            case 1:
                nm = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
                Notification notif = new Notification(R.drawable.ic_launcher, "Factorial= "+intent.
                        getLongExtra(MainActivity.KEY_NUMBER,-1),
                        System.currentTimeMillis());

                PendingIntent pIntent = PendingIntent.getActivity(context, 0, intent, 0);
                notif.setLatestEventInfo(context, "Result factorial", "factorial = "+intent.
                        getLongExtra(MainActivity.KEY_NUMBER,-1), pIntent);

                notif.flags |= Notification.FLAG_AUTO_CANCEL;
                nm.notify(1, notif);
                break;
            default:
                context.startService(new Intent(context,MyServiceAutostart.class));
                break;
        }
    }
}