package com.example.service_task.app.services;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutorService;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Николай on 18.05.14.
 */
public class MyServiceLocalBind extends Service {

    public final String LOG_TAG = "myLogs";

    MyBinder binder = new MyBinder();
    //Thread threadCalcFactorial;
    //ExecutorService es;
    Handler handler;

    Timer timer;
    TimerTask tTask;
    long interval = 1000;
    long number;

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "MyService onCreate");
        timer = new Timer();
        handler = new Handler() {
            @Override
            public void close() {}

            @Override
            public void flush() {}

            @Override
            public void publish(LogRecord logRecord) {}
        };
    }


    public long calcFactorial(long number){
        this.number = number;
        number = fact(number);
        return number;
    }

    long fact(long N) {
        if(N < 0) return 0;
        if (N == 0) return 1;
        else {
            return N * fact(N - 1);}
    }

    public IBinder onBind(Intent arg0) {
        return binder;
    }

   public class MyBinder extends Binder {
        public MyServiceLocalBind getService() {
            return MyServiceLocalBind.this;
        }
    }

    class MyRun implements Runnable {
        long number;
        PendingIntent pendingIntent;

        public MyRun(long number) {
            this.number = number;
        }

        public void run() {
            for(int i=1; i<=20; i++) {
                number = number*i;
                try{
                    Thread.sleep(400);
                }catch (InterruptedException ex){}
                Log.d(LOG_TAG, "RUN ="+i);
            }
        }
    }
}
