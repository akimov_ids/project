package com.example.service_task_2.app;

import android.app.Service;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;


public class ForegroundService  extends Service {
    public static final String EXTRA_PLAYLIST="EXTRA_PLAYLIST";
    public static final String EXTRA_SHUFFLE="EXTRA_SHUFFLE";
    private boolean isPlaying=false;

    NotificationCompat.Builder builder;
    NotificationManager notificationManager;
    int step = 0;
    Thread thread;
    boolean breakWork = true;
    ExecutorService es;
    MyRun mr;

    final int ID_FOREGROUND = 4;
    public final static int TASK_CODE = 0;
    public final static String RESULT_CODE = "result";


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        PendingIntent pi;
        PendingIntent piForMain;
        //this.pendingIntent = pendingIntent;
        piForMain = intent.getParcelableExtra(MainActivity.KEY_PINTENT);

        builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle("ForegroundService");
                //.setContentText("count = " + 24);

        Intent i = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(i);
        pi = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pi);

        notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        startForeground(ID_FOREGROUND, builder.build());

        mr = new MyRun(pi,piForMain);
        es.execute(mr);

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Log.d(LOG_TAG, "MyService onCreate");
        es = Executors.newFixedThreadPool(1);
        //someRes = new Object();
    }


    @Override
    public void onDestroy() {
        Log.d("my","Destroy");
        mr.stop(false);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return(null);
    }

  private void stop() {
        if (isPlaying) {
            Log.w(getClass().getName(), "Got to stop()!");
            isPlaying=false;
            stopForeground(true);
        }
    }


    class MyRun implements Runnable {
        boolean breakWork = true;
        PendingIntent pi;
        PendingIntent piForMain;

        public MyRun(PendingIntent pi,PendingIntent piForMain){
            this.pi = pi;
            this.piForMain = piForMain;
        }

        public void run() {
            while(breakWork){
                step++;
                Log.d("MyLogs", "stepNumber = " + step);
                builder.setContentText("stepNumber = " + step);
                notificationManager.notify(ID_FOREGROUND, builder.build());

                try {
                    piForMain.send(ForegroundService.this,TASK_CODE,new Intent().putExtra(RESULT_CODE,step));
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }catch(PendingIntent.CanceledException ex){
                    ex.printStackTrace();
                }
            }
        }

        void stop(boolean breakWork) {
            this.breakWork = breakWork;
            stopSelf();
        }
    }
}