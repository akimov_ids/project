package com.example.service_task_2.app;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Николай on 17.05.14.
 */
public class MyService extends Service {

    public final static String LOG_TAG = "myLogs";

    public final static int TASK_CODE = 0;
    public final static String RESULT_CODE = "result";

    ExecutorService es;
    PendingIntent pendingIntent;
    Object someRes;
    MyRun mr;
    Intent argResult;

    public void onCreate() {
        super.onCreate();
        Log.d(LOG_TAG, "MyService onCreate");
        es = Executors.newFixedThreadPool(1);
        someRes = new Object();
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "MyService onDestroy");
        mr.stop(false);
        someRes = null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOG_TAG, "MyService onStartCommand");
        es.execute(mr);
        return super.onStartCommand(intent, flags, startId);
    }

    public IBinder onBind(Intent arg0) {
        Log.d("BIND", "Bind start");

        return null;
    }

    class MyRun implements Runnable {
        int number;
        PendingIntent pendingIntent;
        boolean breakWork = true;
        int i = 0;


        public MyRun(int number, PendingIntent pendingIntent) {
            this.number = number;
            this.pendingIntent = pendingIntent;
        }

        public void run() {

            while(breakWork) {

                try{
                    Thread.sleep(1000);
                    pendingIntent.send(MyService.this,TASK_CODE,new Intent().putExtra(RESULT_CODE,i++));
                    Log.d(LOG_TAG, "RUN ="+i);

                }catch (InterruptedException ex){
                    ex.printStackTrace();
                }catch (PendingIntent.CanceledException ex){
                    ex.printStackTrace();
                }
            }
        }

        void stop(boolean breakWork) {
            //Log.d(LOG_TAG, "MyRun#" + startId + " end, stopSelf(" + startId + ")");
            this.breakWork = breakWork;
            stopSelf();
        }
    }
}
