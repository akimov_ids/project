package com.example.service_task_2.app;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OldMainActivity extends ActionBarActivity {

    public static final String KEY_NUMBER = "number";
    public static final String RESULT_NUMBER = "result_number";
    public static final String KEY_PINTENT = "pintent";
    public static final String ACTION_BROADCASST = "action_broadcast";

    public static final int START_SERVICE = 0;
    public static final int GET_RESULT_SERVICE = 1;
    public static final int AUTOSTART_SERVICE = 2;

    public static final String REG_BROADCAST_SERVICE = "0";
    public final int TASK_CODE_1 = 1;

    Button buttonStart;
    Button buttonStop;
    TextView textView;

    PendingIntent pendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonStart = (Button)findViewById(R.id.button);
        buttonStop = (Button)findViewById(R.id.button2);
        textView = (TextView) findViewById(R.id.textView);

        pendingIntent = createPendingResult(TASK_CODE_1,new Intent(),0);


        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //startService(new Intent(MainActivity.this, MyService.class));
                startService(new Intent(OldMainActivity.this, MyService.class).putExtra(KEY_PINTENT, pendingIntent));
            }
        });

        buttonStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("my", "Stop is click");
                //stopService(new Intent(MainActivity.this, MyService.class));
                stopService(new Intent(OldMainActivity.this, MyService.class).putExtra(KEY_PINTENT, pendingIntent));

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Log.d(MyService.LOG_TAG, "requestCode = " + requestCode + ", resultCode = "+ resultCode);

        Log.d(MyService.LOG_TAG, "Result = " + data.getIntExtra(MyService.RESULT_CODE, 0));
        textView.setText("Result = "+data.getIntExtra(MyService.RESULT_CODE,0));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
