package com.example.service_task_2.app;

import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity {

    public static final String KEY_NUMBER = "number";
    public static final String KEY_PINTENT = "pintent";
    public final int TASK_CODE_1 = 1;

    Button butStart;
    Button butStop;
    TextView textView;

    PendingIntent pendingIntent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView)findViewById(R.id.textView);
        pendingIntent = createPendingResult(TASK_CODE_1,new Intent(),0);


        butStart = (Button) findViewById(R.id.button);
        butStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ForegroundService.class);
                startService(new Intent(MainActivity.this, ForegroundService.class).putExtra(KEY_PINTENT, pendingIntent));
            }
        });

        butStop = (Button)findViewById(R.id.button2);
        butStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ForegroundService.class);
                stopService(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Log.d(MyService.LOG_TAG, "requestCode = " + requestCode + ", resultCode = "+ resultCode);

        Log.d(MyService.LOG_TAG, "Result = " + data.getIntExtra(MyService.RESULT_CODE, 0));
        textView.setText("Result = "+data.getIntExtra(MyService.RESULT_CODE,0));
    }


    public void stopPlayer(View v) {
        stopService(new Intent(this, ForegroundService.class));
    }
}