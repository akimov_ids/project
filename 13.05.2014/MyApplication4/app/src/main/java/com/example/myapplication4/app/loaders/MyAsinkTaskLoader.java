package com.example.myapplication4.app.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.example.myapplication4.app.DBHelper;

/**
 * Created by Николай on 03.05.14.
 */
public class MyAsinkTaskLoader extends AsyncTask<Integer, Void, Cursor> {

    private DBHelper dbHelper;
    public final static int INSERT = 0;
    public final static int UPDATE = 1;
    public final static int DELETE = 2;

    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";

    ContentValues cv;
    String tableQuery;
    Bundle args;
    //Cursor cursor;

    public MyAsinkTaskLoader(ContentValues cv, Context context, String tableQuery, Bundle args){
        this.cv = cv;
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        this.tableQuery = tableQuery;
        this.args = args;
    }

    @Override
    protected Cursor doInBackground(Integer... key) {

        if (key[0] == INSERT){
            dbHelper.getWritableDatabase().insert(tableQuery,null,cv);

        }else if(key[0] == UPDATE){
            //dbHelper.getWritableDatabase().update(tableQuery,cv,args.getString(SELECT_COLUMN)+" = ?",args.getStringArray(SELECT_ARG));
            dbHelper.getWritableDatabase().update(tableQuery,cv,args.getString(SELECT_COLUMN)+" = ?",args.getStringArray(SELECT_ARG));
        }else if(key[0] == DELETE){
            //dbHelper.getWritableDatabase().update(tableQuery,cv,args.getString(SELECT_COLUMN)+" = ?",args.getStringArray(SELECT_ARG));
            dbHelper.getWritableDatabase().delete(tableQuery,"_id = ?",args.getStringArray(SELECT_ARG));
        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d("AS"," preE");
    }

    @Override
    protected void onPostExecute(Cursor result) {
        super.onPostExecute(result);
        Log.d("AS"," postE");


    }
}