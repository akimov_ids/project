package com.example.myapplication4.app.loaders;

/**
 * Created by Nikolay on 12.05.14.
 */
public interface MyImageLoadingListener {
    void onLoadingStarted();
    void onLoadingComplete();
}