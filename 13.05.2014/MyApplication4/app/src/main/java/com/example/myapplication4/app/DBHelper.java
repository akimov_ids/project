package com.example.myapplication4.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Nikolay on 07.05.14.
 */
public class DBHelper extends SQLiteOpenHelper {
    public final static  String ID_COLUMN = "_id";
    public final static  String LINK_PARSE_COLUMN = "link_parse";
    public final static  String ID_LONGT_COLUMN = "longt";
    public final static  String LAT_COLUMN = "lat";
    public final static  String ID_PARSE = "id_parse";
    public final static  String PATH_COLUMN = "path";

    public final static  String DB_NAME = "mydb";
    public final static  String TABLE = "fotos";

    private static final String DB_CREATE_FOTOS = "create table if not exists fotos(_id integer primary key autoincrement, link_parse text, longt text,lat text, path text, id_parse text);";

    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,int version) {
        super(context, name, factory, version);
    }

    // создаем и заполняем БД
    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_FOTOS);

    }

    /*public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }*/

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}