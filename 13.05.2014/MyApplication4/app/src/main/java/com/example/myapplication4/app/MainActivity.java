package com.example.myapplication4.app;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Location;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.widget.Button;

import com.example.myapplication4.app.fragment.FragmentLookPhoto;
import com.example.myapplication4.app.fragment.MainFragment;
import com.example.myapplication4.app.loaders.MyAsinkTaskLoader;
import com.example.myapplication4.app.loaders.MyCursorLoader;
import com.example.myapplication4.app.loaders.MyLoader;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class MainActivity extends FragmentActivity  implements SurfaceHolder.Callback, LoaderManager.LoaderCallbacks<Cursor>, SomeEventListener  {

    public final static String PATH_IMAGE_FROM_CAMERA = "0";

    FragmentTransaction fTrans;
    Intent intent;

    Button createPhoto;
    Button lookListImage;
    Button mapBut;
    Button butPhoto;
    String path;
    String pathPhoto;
    String[] long_lat;
    FileOutputStream outStream;
    Bitmap bitmap;
    ByteArrayOutputStream stream;
    LocationManager locationManager;
    MainFragment mainFragment;
    Location location;
    List<String> arrayListFromParse = new ArrayList<String>();
    List<String> arrayListFromDB = new ArrayList<String>();
    HashMap<String,String> urlMap = new HashMap<String, String>();
    //String uu;
    String path_fom_cam;

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        path = Environment.getExternalStorageDirectory()+"/myfoto";//+d.getTime()+"QQQQ.jpg";
        File dir = new File(Environment.getExternalStorageDirectory()+"/myfoto");
        if(!dir.exists() && !dir.isDirectory()) {
            dir.mkdir();
        }


        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        getSupportLoaderManager().initLoader(MyLoader.PARSE_LOADER, null, this);

        /*mainFragment = new MainFragment();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.addToBackStack("home");
        fTrans.add(R.id.frgmCont, mainFragment);
        fTrans.commit();*/

        mainFragment = new MainFragment();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.addToBackStack("home");
        fTrans.add(R.id.frgmCont, mainFragment);
        fTrans.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ///Bundle arg ;
        //arg = data.getExtras();
        //Log.d("path_intent",arg.getString("fileName"));
        if(resultCode == RESULT_OK){
            String serviceString = LOCATION_SERVICE;

            locationManager = (LocationManager)getSystemService(serviceString);
            String provider = LocationManager.NETWORK_PROVIDER;
            location = locationManager.getLastKnownLocation(provider);
            long_lat = updateWithNewLocation(location);

            Log.d("RES", "RESULT IN ACTIVITY");
            //stream = new ByteArrayOutputStream();
            pathPhoto = path_fom_cam;


            FragmentLookPhoto fragmentLookPhoto = new FragmentLookPhoto();
            fTrans = getSupportFragmentManager().beginTransaction();

            Bundle args = new Bundle();
            args.putString("path", pathPhoto);
            fragmentLookPhoto.setArguments(args);
            fTrans.addToBackStack("look_photo");
            fTrans.replace(R.id.frgmCont, fragmentLookPhoto);
            fTrans.commit();

            args.putString(MyLoader.LONG_T,long_lat[0]);
            args.putString(MyLoader.LAT_T,long_lat[1]);
            args.putString(MyLoader.PATH, pathPhoto);

            Loader<Cursor> loader = getSupportLoaderManager().initLoader(MyLoader.MAIN_LOADER, args, this);

            loader.forceLoad();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        Log.d("QW","HELLO");
        getSupportFragmentManager().popBackStack("home",0);
        //getSupportFragmentManager().popBackStack("home",0);
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onResume(){
        super.onResume();


    }


    private String[] updateWithNewLocation(Location location) {

        String latLongString;
        if (location != null) {
            double lat = location.getLatitude();
            double lng = location.getLongitude();
            latLongString = "Lat:" + lat + "\nLong:" + lng;
            return new String[]{""+lat,""+lng};
        } else {
            latLongString = "No location found";
            return new String[]{latLongString};
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        if(id == MyLoader.MAIN_LOADER){
            args = new Bundle();
            args.putString(MyLoader.LONG_T,long_lat[0]);
            args.putString(MyLoader.LAT_T,long_lat[1]);
            args.putString(MyLoader.PATH,pathPhoto);
            return new MyLoader(this,args);
        }else if(id == MyLoader.PARSE_LOADER){
            return new MyCursorLoader(this, "fotos");
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(loader.getId() == MyLoader.PARSE_LOADER){
            String name;
            for(data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                // The Cursor is now set to the right position
                name = data.getString(data.getColumnIndex("path"));
                Log.d("COL_I","4 = "+data.getString(data.getColumnIndex("path")));
                arrayListFromDB.add(name.substring(name.lastIndexOf("/") + 1));
            }

        Thread mt = new Thread(new Runnable() {
            String name;
            String nameRes;
            Bundle args;
            ByteArrayOutputStream streamFromPars;
            byte[] byteArray;

            @Override
            public void run() {
                Log.d("THREAD","Inaf thread");

                    HttpGet get = new HttpGet("https://api.parse.com/1/classes/mdat");
                    HttpClient client = new DefaultHttpClient();

                    get.setHeader("X-Parse-Application-Id","1jwroZZdc3fHDeBiZA5HHN2XndnwMgQLzjxT5D7X");
                    get.setHeader("X-Parse-REST-API-Key","89l9DAVYPTyoYMzfPaSEE3wExcobn4HraixJ9rRE");

                   try{
                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                    String responsBody = client.execute(get,responseHandler);
                    JSONObject response = new JSONObject(responsBody);

                    JSONArray responseArr =  response.getJSONArray("results");

                    for(int i = 0; i<responseArr.length(); i++){
                        response = responseArr.optJSONObject(i);
                        response = response.getJSONObject("picture");
                        Log.d("URL", response.getString("url"));

                        name = response.getString("url");
                        nameRes = name.substring(name.lastIndexOf("-") + 1);
                        urlMap.put(nameRes,name);
                        arrayListFromParse.add(nameRes);
                    }

                   }catch(JSONException ex){ex.printStackTrace();
                   }catch(IOException ex) {ex.printStackTrace();}

                Log.d("LIST_RESULT","");
                arrayListFromParse.removeAll(arrayListFromDB);

                    Log.d("LIST_RESULT","Size db = "+arrayListFromDB.size());
                    Log.d("LIST_RESULT","Size parse = "+arrayListFromParse.size());
                    Log.d("LIST_RESULT","Size parse = "+arrayListFromParse.size());


                    //===================NO_DELEETE===============//
                    if(arrayListFromParse.size()>0){
                        Iterator itr = arrayListFromParse.iterator();
                        URL url;
                        HttpURLConnection connection;
                        ContentValues cv = new ContentValues();
                        String nowElement;
                        Bitmap myBitmap;
                        InputStream input;
                        args = new Bundle();

                        while(itr.hasNext()){
                            streamFromPars = new ByteArrayOutputStream();
                            nowElement = (String)itr.next();
                            Log.d("MAP", urlMap.get(nowElement));

                            try{
                                url = new URL(urlMap.get(nowElement));

                                connection = (HttpURLConnection) url.openConnection();
                                connection.setDoInput(true);
                                connection.connect();
                                input = connection.getInputStream();
                                myBitmap = BitmapFactory.decodeStream(input);
                                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, streamFromPars);

                                byteArray = streamFromPars.toByteArray();
                                outStream = new FileOutputStream(path+"/"+nowElement);
                                outStream.write(byteArray);
                                outStream.close();
                                input.close();
                                connection.disconnect();
                                streamFromPars.close();
                            }catch(MalformedURLException ex){ex.printStackTrace();
                            }catch(IOException ex){ex.printStackTrace();}

                            Log.d("LOAD", "In while");
                            cv.put("longt", "0");
                            cv.put("lat","0");
                            cv.put("path",path+"/"+nowElement);
                            cv.put("link_parse ","1");

                            MyAsinkTaskLoader mas = new MyAsinkTaskLoader(cv, MainActivity.this, "fotos", null);
                            mas.execute(new Integer[]{MyAsinkTaskLoader.INSERT});
                        }
                    }
            }
        });
            mt.start();
        }
        getSupportLoaderManager().destroyLoader(MyLoader.MAIN_LOADER);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void someEvent(Bundle arg) {
        path_fom_cam = arg.getString(PATH_IMAGE_FROM_CAMERA);
    }
}




