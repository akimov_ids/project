package com.example.myapplication4.app;

import java.lang.ref.Reference;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Nikolay on 12.05.14.
 */
public abstract class Cache<K, V> {

    protected final Map<K, Reference<V>> softMap = new HashMap<K, Reference<V>>();

    public V get(K key) {
        if (softMap.containsKey(key)) {
            Reference<V> reference = softMap.get(key);
            return reference.get();
        } else {
            return null;
        }
    }

    public void put(K key, V value) {
        softMap.put(key, createReference(value));
    }

    public void clear() {
        softMap.clear();
    }

    protected abstract Reference<V> createReference(V value);
}