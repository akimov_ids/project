package com.example.myapplication4.app;

import android.os.Bundle;

/**
 * Created by Nikolay on 19.05.14.
 */
public interface SomeEventListener {
    public void someEvent(Bundle arg);
}