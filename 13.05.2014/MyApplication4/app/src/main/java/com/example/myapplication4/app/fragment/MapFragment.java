package com.example.myapplication4.app.fragment;

import android.annotation.TargetApi;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Paint;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.myapplication4.app.loaders.MyCursorLoader;
import com.example.myapplication4.app.R;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.jar.Attributes;

/**
 * Created by Николай on 05.05.14.
 */
public class MapFragment extends Fragment implements LocationListener, LoaderManager.LoaderCallbacks<Cursor> {

    private MapView mMapView;
    private GoogleMap mMap;
    private Bundle mBundle;
   // GoogleMap googleMap;
    //LocationManager locationManager;
    FragmentTransaction fTrans;
   // LatLng point1 = new LatLng(19.03160394, 72.8676239);
    HashMap<String,String> map;


    final static String TABLE =  "fotos";

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View inflatedView = inflater.inflate(R.layout.map_fragment, container, false);
            MapsInitializer.initialize(getActivity());
        getActivity().getActionBar().setTitle("Map");
        EditText editText1 = new EditText(getActivity());
        editText1.setText("Screen Pinned");

        /*mMapView = (MapView) inflatedView.findViewById(R.id.map);
        mMapView.onCreate(mBundle);

        setUpMapIfNeeded(inflatedView);*/
        return inflatedView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBundle = savedInstanceState;
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
    }

    private void setUpMapIfNeeded(View inflatedView) {
        if (mMap == null) {
            mMap = ((MapView) inflatedView.findViewById(R.id.map)).getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
    }

    @Override
    public void onResume() {
        super.onResume();




        mMapView = (MapView) getView().findViewById(R.id.map);
        mMapView.onCreate(mBundle);
        setUpMapIfNeeded(getView());
        mMapView.onResume();


        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getActionBar().setHomeButtonEnabled(true);
        getActivity().getSupportLoaderManager().restartLoader(0, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mMap = null;
    }

    @Override
    public void onDestroy() {
        mMapView.onDestroy();
        super.onDestroy();

        getActivity().getSupportLoaderManager().destroyLoader(0);
    }

    @Override
    public void onLocationChanged(Location location) {
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(),TABLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
      map = new HashMap();
      int path_col = data.getColumnIndex("path");
      int lat_col = data.getColumnIndex("lat");
      int longt_col = data.getColumnIndex("longt");
      int id_col = data.getColumnIndex("_id");
      int scale = 1;

      int resultWidth = 40;
      int resultHeight = 40;

      BitmapFactory.Options options;
      File image;
      Bitmap bmp;

        while (data.moveToNext()) {
            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            image = new  File(data.getString(path_col));
            BitmapFactory.decodeFile(image.getAbsolutePath(),options);

            if(options.outWidth>options.outHeight){
                scale = options.outWidth/resultWidth;
            }else{
                scale = options.outHeight/resultHeight;
            }

            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);

            mMap.addMarker(new MarkerOptions().position(new LatLng(data.getDouble(longt_col), data.getDouble(lat_col))).title(data.getString(id_col)).icon(BitmapDescriptorFactory.fromBitmap(bmp)));

            //mMap.addMarker(new MarkerOptions().position(new LatLng(data.getDouble(longt_col), data.getDouble(lat_col))).title("").icon(BitmapDescriptorFactory.fromBitmap(bmp)));

            map.put(data.getString(id_col), ""+data.getString(path_col));
            /*Log.d("AW", "0 - "+data.getString(0));
            Log.d("AW", "1 - "+data.getString(1));
            Log.d("AW", "2 - "+data.getString(2));
            Log.d("AW", "3 - "+data.getString(3));
            Log.d("AW", "4 - "+data.getString(4));*/

        }
        //=
        Marker source;
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener()
        {
            @Override
            public boolean onMarkerClick(Marker arg0) {
                long id = Long.parseLong((String)arg0.getTitle());

                FragmentLookPhoto fragmentLookPhoto = new FragmentLookPhoto();
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();

                Bundle args = new Bundle();
                args.putLong("id", id);
                args.putString("path", map.get(arg0.getTitle()));
                fragmentLookPhoto.setArguments(args);
                //mMap.clear();
                fTrans.replace(R.id.frgmCont, fragmentLookPhoto);
                fTrans.addToBackStack("look_photo");
                fTrans.commit();

                return true;
            }
        });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}