package com.example.myapplication4.app.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.myapplication4.app.DBHelper;

/**
 * Created by Николай on 20.04.14.
 */
public class MyLoader extends AsyncTaskLoader<Cursor> {

    public static String LONG_T = "LONGT";
    public static String LAT_T = "LAT";
    public static String DATA = "link_parse";
    public static String PATH = "PATH";

    public static final int MAIN_LOADER = 4;
    public static final int PARSE_LOADER = 5;


    //final String LOG_TAG = "myLogs";
    //final int PAUSE = 10;
    //public final static String NUMBER = "number_for_calc";
    //private long number;
    //String format;
    private DBHelper dbHelper;
    private String tableQuery;
    private ContentValues cv;

    public MyLoader(Context context, Bundle args) {
        super(context);
        //this.number = args.getLong(NUMBER);
        //Log.d(LOG_TAG, hashCode() + " create TimeAsyncLoader");
        cv = new ContentValues();
        tableQuery = "fotos";
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        cv.put(LONG_T,args.getString(LONG_T));
        cv.put(LAT_T,args.getString(LAT_T));
        cv.put(DATA,args.getString(DATA));
        cv.put(PATH,args.getString(PATH));


    }

    @Override
    public Cursor loadInBackground() {
        boolean df = true;
        dbHelper.getWritableDatabase().insert(tableQuery,null,cv);
        //while(df);
        return null;// ""+result;
    }

}