package com.example.myapplication4.app.fragment;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myapplication4.app.DBHelper;
import com.example.myapplication4.app.loaders.MyCursorLoader;
import com.example.myapplication4.app.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.utils.StorageUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Nikolay on 12.05.14.
 */
public class FragmentListPhoto  extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>  {

    final String LOG_TAG = "myLogs";
    ImageView imageView;
    ImageLoader imageLoader;

    FragmentTransaction fTrans;
    DisplayImageOptions options;
    String[] imageUrls;
    List<String> imageListUrls;
    ListView listView;
    //final String PATH_FOLDER = Environment.getExternalStorageDirectory()+"/myfoto";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
        getActivity().getSupportLoaderManager().initLoader(0, null, this);
        imageListUrls = new ArrayList<String>();
        Log.d("","");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.d(LOG_TAG, "Fragment1 onCreateView");

        View v = inflater.inflate(R.layout.fragment_list_foto, null);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        options = new DisplayImageOptions.Builder()
                .showStubImage(R.drawable.ic_launcher)
                .cacheInMemory()
                .cacheOnDisc()
                .build();

        listView = (ListView) v.findViewById(R.id.listViewPhoto);
        return v;
    }

    private void startImagePagerActivity(int position) {
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }
    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getActionBar().setHomeButtonEnabled(true);
        getActivity().getActionBar().setTitle("List Photo");
        //getFragmentManager().getSupportActionBar.setTitle("List Photo");

        imageListUrls.clear();
        getActivity().getSupportLoaderManager().restartLoader(0, null, this);

        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        //getActivity().getSupportLoaderManager().destroyLoader(0);
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(0);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(),MapFragment.TABLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        while (data.moveToNext()) {
            imageListUrls.add("file://"+data.getString(data.getColumnIndex("path")));
        }

        imageUrls = new String[imageListUrls.size()];
        imageListUrls.toArray(imageUrls);

        listView.setAdapter(new ItemAdapter(getActivity(),data));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("ID",""+id);

                FragmentLookPhoto fragmentLookPhoto = new FragmentLookPhoto();
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();

                Bundle args = new Bundle();
                args.putLong(FragmentLookPhoto.ID, id);

                Log.d("SUB","SUB = "+imageUrls[position].substring(7));
                args.putString(fragmentLookPhoto.PATH, imageUrls[position].substring(7));
                fragmentLookPhoto.setArguments(args);

                fTrans.replace(R.id.frgmCont, fragmentLookPhoto);
                fTrans.addToBackStack("look_photo");
                fTrans.commit();
           }
        });
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

class ItemAdapter extends CursorAdapter {

    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    Cursor cursor;

    public ItemAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        this.cursor = cursor;
    }

    private class ViewHolder {
        public TextView text;
        public ImageView image;
    }

    @Override
    public int getCount() {
        return imageUrls.length;
        //return 4;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        //return position;
        if(cursor != null){Log.d("WQ", "assd");}
        cursor.moveToPosition(position);
        //Log.d("Curs",""+cursor.getLong(cursor.getColumnIndex("_id")));
        return cursor.getLong(cursor.getColumnIndex(DBHelper.ID_COLUMN));
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        final ViewHolder holder;
        if (convertView == null) {
            view = getActivity().getLayoutInflater().inflate(R.layout.item_list_image, parent, false);
            holder = new ViewHolder();
            holder.text = (TextView) view.findViewById(R.id.text);
            holder.image = (ImageView) view.findViewById(R.id.image);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.text.setText("Item " + (position + 1));
        imageLoader.displayImage(imageUrls[position], holder.image, options, animateFirstListener);
        return view;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        return null;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
    }
}

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {
        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}