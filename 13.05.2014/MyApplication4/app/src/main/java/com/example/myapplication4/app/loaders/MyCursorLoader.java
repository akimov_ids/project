package com.example.myapplication4.app.loaders;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.util.Log;

import com.example.myapplication4.app.DBHelper;

/**
 * Created by Nikolay on 12.05.14.
 */
public class MyCursorLoader  extends CursorLoader {
    public static final String KEY_OPERATION = "KEY_OP";
    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";

    public static final int QUERY = 1;
    public static final int QUERY_ARG = 2;
    public static final int UPDATE = 3;

    private DBHelper dbHelper;
    private Cursor cursor;
    private Bundle args;
    private int OPERATION;
    private ContentValues cv;
    private String tableQuery;
    private Bundle arg;

    public MyCursorLoader(Context context,String tableQuery,ContentValues cv) {
        super(context);
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
        this.args = args;
    }

    public MyCursorLoader(Context context, String tableQuery) {
        super(context);
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        OPERATION = QUERY;
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
    }

    public MyCursorLoader(Context context, String tableQuery, Bundle arg, int KEY) {
        super(context);
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        OPERATION = KEY;
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
        this.arg = arg;
        Log.d("FS", "ss");
    }

    @Override
    public Cursor loadInBackground() {

        try{
 //           Thread.sleep(400);
        }catch(Exception ex){}

        switch (OPERATION){
            case QUERY:

                cursor = dbHelper.getReadableDatabase().query(tableQuery, null, null, null, null, null, null);
                //cursor = dbHelper.getWritableDatabase().query(tableQuery, null, null, null, null, null, null);
                break;
            case QUERY_ARG:
                String[] ar = arg.getStringArray(SELECT_ARG);
                String[] sl = arg.getStringArray(SELECT_COLUMN);
                Log.d("ARG","QWQWQW = "+ar[0]);
                Log.d("ARG","QWQWQW = "+sl[0]);

                //cursor = dbHelper.getWritableDatabase().query(tableQuery,arg.getStringArray(SELECT_COLUMN),"_id = ?",arg.getStringArray(SELECT_ARG),null,null,null);
                cursor = dbHelper.getReadableDatabase().query(tableQuery,arg.getStringArray(SELECT_COLUMN),"_id = ?",arg.getStringArray(SELECT_ARG),null,null,null);
                break;
        }
        return cursor;
    }
}