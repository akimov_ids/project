package com.example.myapplication4.app.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.example.myapplication4.app.DBHelper;
import com.example.myapplication4.app.loaders.MyAsinkTaskLoader;
import com.example.myapplication4.app.loaders.MyCursorLoader;
import com.example.myapplication4.app.R;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import org.apache.http.Header;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
//import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by Николай on 04.05.14.
 */
public class FragmentLookPhoto  extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener{

    public final static String ID = "id";
    public final static String PATH = "path";



    final String LOG_TAG = "myLogs";
    ImageView imageView;
    DisplayImageOptions options;
    ViewPager pager;
    ContentValues cv;
    Button butLoadImage;
    Button butDelete;
    String path;
    String imageName;
    private long id;
    File image;
    Bitmap bmp;
    Handler handler;


    public static FragmentLookPhoto newInstance(long id,String path) {
        FragmentLookPhoto f = new FragmentLookPhoto();
        Bundle args = new Bundle();
        args.putLong(ID,id);
        args.putString(PATH, path);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        getActivity().getActionBar().setTitle("Photo");
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_look_photo, null);
        imageView = (ImageView)v.findViewById(R.id.imageView);
        path = getArguments().getString(PATH);
        imageName = path.substring(path.lastIndexOf("/") + 1);

        cv = new ContentValues();
        //================OLD_VERSION==============//
        Log.d("ASW",getArguments().getString(DBHelper.PATH_COLUMN));
        image = new  File(getArguments().getString(PATH));
        butLoadImage = (Button) v.findViewById(R.id.button);
        butLoadImage.setOnClickListener(this);

        handler = new Handler() {
            public void handleMessage(android.os.Message msg) {
              getActivity().getSupportFragmentManager().popBackStackImmediate();
            };
        };

        butDelete = (Button) v.findViewById(R.id.button2);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread threadDelete = new Thread(new Runnable() {
                   String url;

                    @Override
                    public void run() {
                            HttpClient client = new DefaultHttpClient();
                            HashMap<String, String> idMap = new HashMap<String,String>();
                            String id_pars;
                            String path_pars;

                            //========GET_ID========//
                            String nameRes;
                                Log.d("THREAD","Inaf thread");

                                HttpGet get = new HttpGet("https://api.parse.com/1/classes/mdat");
                                get.setHeader("X-Parse-Application-Id","1jwroZZdc3fHDeBiZA5HHN2XndnwMgQLzjxT5D7X");
                                get.setHeader("X-Parse-REST-API-Key","89l9DAVYPTyoYMzfPaSEE3wExcobn4HraixJ9rRE");

                                try{
                                    ResponseHandler<String> responseHandler=new BasicResponseHandler();
                                    String responsBody = client.execute(get,responseHandler);
                                    JSONObject response = new JSONObject(responsBody);
                                    JSONArray responseArr =  response.optJSONArray("results");

                                    Log.d("LNGT",""+responseArr.length());

                                    for(int i = 0; i<responseArr.length(); i++){
                                        id_pars = responseArr.optJSONObject(i).getString("objectId");
                                        path_pars = responseArr.optJSONObject(i).getJSONObject("picture").getString("url");

                                        nameRes = path_pars.substring(path_pars.lastIndexOf("-") + 1);
                                        idMap.put(nameRes,id_pars);

                                        Log.d("URLL", responseArr.optJSONObject(i).getJSONObject("picture").getString("url"));
                                    }

                                }catch(JSONException ex){ex.printStackTrace();
                                }catch(IOException ex) {ex.printStackTrace();}


                        HttpDelete delete = new HttpDelete("https://api.parse.com/1/classes/mdat/"+idMap.get(imageName));
                            delete.setHeader("X-Parse-Application-Id","1jwroZZdc3fHDeBiZA5HHN2XndnwMgQLzjxT5D7X");
                            delete.setHeader("X-Parse-Master-Key","z9DGWzHb10nVpdFnONC47n0XXnHmd3qcxr2nl7ad");

                            try{
                                HttpResponse response = client.execute(delete);
                                Header[] headers =  response.getAllHeaders();
                                Log.d("SQ",""+response.getStatusLine());
                                Log.d("DEL", "Is delete");
                            }catch(IOException ex){ ex.printStackTrace();}


                       Log.d("ARG","ID = "+getArguments().getLong(ID));

                        Bundle arg = new Bundle();
                        Log.d("TS", getArguments().getString(PATH));
                        arg.putStringArray(MyAsinkTaskLoader.SELECT_ARG, new String[]{""+getArguments().getLong(ID)});

                        MyAsinkTaskLoader mat = new MyAsinkTaskLoader(null,getActivity(),DBHelper.TABLE,arg);
                        mat.execute(new Integer[]{MyAsinkTaskLoader.DELETE});

                        File file = new File(path);
                        file.delete();
                        handler.sendEmptyMessage(0);
                    }

                });
                threadDelete.start();
            }
        });

        //=

        int resultWidth = 400;
        int resultHeight = 400;

        //=
        int scale = 1;
        Log.d("IMG",image.getAbsolutePath());
        BitmapFactory.Options options = new BitmapFactory.Options();
        //options.inSampleSize = 1;
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getAbsolutePath(),options);
        if(options.outWidth>options.outHeight){
            scale = options.outWidth/resultWidth;
        }else{
            scale = options.outHeight/resultHeight;
        }

        ExifInterface exif;
        int orientation = -1;
        try{
            exif = new ExifInterface(image.getAbsolutePath());
            orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, 1 );
        }catch (IOException ex){ex.printStackTrace();}


        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);
        //bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);


        Matrix matrix = new Matrix();

        switch(orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                break;
        }


        bmp = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

        imageView.setImageBitmap(bmp);

        return v;
    }

    @Override
    public void onClick(View view) {
        Log.d("CLIC_LOAD","Is clicked");

        //===============PARSE============//
        Thread myThready = new Thread(new Runnable(){

            String urlParseFile = "https://api.parse.com/1/files/"+imageName;
            String urlAssocFile = "https://api.parse.com/1/classes/mdat";
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(urlParseFile);
            public void run() {

                HttpResponse response;
                byte[] arr = new byte[1024];
                byte[] byteArray;

                try{ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    FileInputStream streamIn = new FileInputStream(getArguments().getString(PATH));

                    Log.d("WQ", getArguments().getString("path"));

                    int read;
                    while ((read = streamIn.read(arr)) > -1){
                        stream.write(arr, 0, read);
                    };

                    byteArray = stream.toByteArray();
                    stream.close();
                    streamIn.close();

                    post.setHeader("X-Parse-Application-Id","1jwroZZdc3fHDeBiZA5HHN2XndnwMgQLzjxT5D7X");
                    post.setHeader("X-Parse-REST-API-Key","89l9DAVYPTyoYMzfPaSEE3wExcobn4HraixJ9rRE");
                    post.setHeader("Content-Type","image/jpeg");
                    post.setEntity(new ByteArrayEntity(byteArray));

                    response = client.execute(post);
                    Header[] headers =  response.getAllHeaders();
                    Log.d("SQ",""+response.getStatusLine());

                    imageName = headers[5].getValue();

                    imageName = imageName.substring(imageName.lastIndexOf("/")+1);
                    Log.d("s","s = "+imageName);

                    //=========================ASSOC=======================//
                    post = new HttpPost(urlAssocFile);

                    JSONObject jsonObj = new JSONObject();
                    JSONObject jsonObjPic = new JSONObject();

                    //jsonObj.put("objectId", "QW");
                    jsonObj.put("name", "myApp44");
                    jsonObjPic.put("name",imageName);
                    jsonObjPic.put("__type","File");
                    jsonObj.put("picture",jsonObjPic);

                    post.setHeader("X-Parse-Application-Id","1jwroZZdc3fHDeBiZA5HHN2XndnwMgQLzjxT5D7X");
                    post.setHeader("X-Parse-REST-API-Key","89l9DAVYPTyoYMzfPaSEE3wExcobn4HraixJ9rRE");
                    post.setHeader("Content-Type", "application/json");

                    String json = jsonObj.toString();
                    StringEntity se = new StringEntity(json);

                    post.setEntity(se);
                    Log.d("", "");
                    response = client.execute(post);
                    Log.d("", "");
                }catch(FileNotFoundException ex){
                    ex.printStackTrace();
                }catch(IOException ex){ex.printStackTrace();
                }catch(JSONException ex){ex.printStackTrace();}
            }
        });

        myThready.start();

        Bundle arg = new Bundle();
        arg.putString(MyAsinkTaskLoader.SELECT_COLUMN, "path");
        Log.d("TS", getArguments().getString(PATH));
        arg.putStringArray(MyAsinkTaskLoader.SELECT_ARG, new String[]{getArguments().getString(PATH)});

        cv.put("link_parse","1");
        //===PUT_ID==//
        MyAsinkTaskLoader mat = new MyAsinkTaskLoader(cv,getActivity(),"fotos",arg);
        mat.execute(new Integer[]{MyAsinkTaskLoader.UPDATE});
        butLoadImage.setEnabled(false);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();

        getActivity().getSupportLoaderManager().initLoader(1, null, this);
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getActionBar().setHomeButtonEnabled(true);
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(1);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Bundle arg = new Bundle();
        Log.d("QWERTY","e = "+getArguments().getLong(ID));
        arg.putStringArray(MyCursorLoader.SELECT_ARG,new String[]{""+getArguments().getLong(ID)});
        arg.putStringArray(MyCursorLoader.SELECT_COLUMN,new String[]{"link_parse"});
        return new MyCursorLoader(getActivity(),"fotos", arg, MyCursorLoader.QUERY_ARG);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data != null&&data.getCount()>0){
            data.moveToNext();
            if(data.getString(data.getColumnIndex("link_parse"))!=null){
                Log.d("QW","EW");
                butLoadImage.setEnabled(false);
                butLoadImage.setClickable(false);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}