package com.example.myapplication4.app.fragment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.myapplication4.app.MainActivity;
import com.example.myapplication4.app.R;
import com.example.myapplication4.app.SomeEventListener;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Created by Nikolay on 13.05.14.
 */
public class MainFragment  extends Fragment {

    FragmentTransaction fTrans;
    Button butPhoto;
    Button butMap;
    Button butListFoto;
    String fileName;
    Bundle arg;
    SomeEventListener someEventListener;

    final String LOG_TAG = "myLogs";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        someEventListener = (SomeEventListener)activity;
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fargment_main, null);

        butPhoto = (Button) v.findViewById(R.id.button);
        butPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File tempFile;
                Intent intent;
                try{

                    //File file  = new File("sdcard/qaza.jpg");
                    Date d = new Date();
                    File file  = new File(Environment.getExternalStorageDirectory()+"/myfoto/"+d.getTime()+"QQQQ.jpg");

                    file.createNewFile();

                    //tempFile =

                    fileName = file.getAbsolutePath();

                    arg = new Bundle();
                    arg.putString(MainActivity.PATH_IMAGE_FROM_CAMERA,fileName);
                    someEventListener.someEvent(arg);

                    Uri uri = Uri.fromFile(file);
                    intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
                    getActivity().startActivityForResult(intent,0);
                }catch (IOException ex){ex.printStackTrace();}
            }
        });

        butMap = (Button) v.findViewById(R.id.button3);
        butMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MapFragment mapFragment = new MapFragment();
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmCont, mapFragment);
                fTrans.addToBackStack("map");
                fTrans.commit();
            }
        });

        butListFoto = (Button) v.findViewById(R.id.button2);
        butListFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentListPhoto listPhoto = new FragmentListPhoto();
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmCont, listPhoto);
                fTrans.addToBackStack("list_photo");
                fTrans.commit();
            }
        });
        return v;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putString("fileName", fileName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("RES","RESULT IN FRAGMENT");
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();
        getActivity().getActionBar().setTitle("Menu");
        getActivity().getActionBar().setDisplayHomeAsUpEnabled(false);
        getActivity().getActionBar().setHomeButtonEnabled(false);

        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }



    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }




}
