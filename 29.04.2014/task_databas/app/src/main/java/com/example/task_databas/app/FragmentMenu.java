package com.example.task_databas.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.task_databas.fragment.list.FragmentListDetentions;
import com.example.task_databas.fragment.list.FragmentListDistricts;
import com.example.task_databas.fragment.list.FragmentListOfficers;
import com.example.task_databas.fragment.list.FragmentListSuspects;


/**
 * Created by Nikolay on 07.05.14.
 */
public class FragmentMenu  extends Fragment {

    final String LOG_TAG = "myLogs";
    Button butOfficers;
    Button butSuspect;
    Button butDistrict;
    Button butDetention;
    FragmentTransaction fTrans;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_menu, null);
        butOfficers = (Button) v.findViewById(R.id.button_officer);
        butOfficers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MY_TAG", "ON_CLICK");
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment_id, new FragmentListOfficers());
                fTrans.addToBackStack("fragment_list_officers");
                fTrans.commit();
            }
        });

        butSuspect = (Button) v.findViewById(R.id.button_suspect);
        butSuspect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MY_TAG", "ON_CLICK");
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment_id, new FragmentListSuspects());
                fTrans.addToBackStack("fragment_list_suspect");
                fTrans.commit();
            }
        });

         butDistrict = (Button) v.findViewById(R.id.button_district);
        butDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MY_TAG", "ON_CLICK");
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment_id, new FragmentListDistricts());
                fTrans.addToBackStack("three");
                fTrans.commit();

            }
        });

        butDetention = (Button) v.findViewById(R.id.button_detention);
        butDetention.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("MY_TAG", "ON_CLICK");
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.fragment_id, new FragmentListDetentions());
                fTrans.addToBackStack("list_detention");
                fTrans.commit();

            }
        });

        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();



        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }
}
