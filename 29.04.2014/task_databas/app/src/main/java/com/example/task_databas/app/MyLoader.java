package com.example.task_databas.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.util.Log;

/**
 * Created by Nikolay on 07.05.14.
 */
public class MyLoader extends CursorLoader {
    public static final String KEY_OPERATION = "KEY_OP";
    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";

    public static final int QUERY = 1;
    public static final int DELETE = 2;
    public static final int UPDATE = 3;

    private DBHelper dbHelper;
    private Cursor cursor;
    private Bundle args;
    private int OPERATION;
    private ContentValues cv;
    private String tableQuery;
    private Bundle arg;

    public MyLoader(Context context,String tableQuery,ContentValues cv) {
        super(context);
        dbHelper = new DBHelper(context,"mydb",null,1);
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
        this.args = args;
    }

    public MyLoader(Context context, String tableQuery) {
        super(context);
        dbHelper = new DBHelper(context,"mydb",null,1);
        OPERATION = QUERY;
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
    }

    public MyLoader(Context context, String tableQuery,ContentValues cv, Bundle arg, int KEY) {
        super(context);
        dbHelper = new DBHelper(context,"mydb",null,1);
        OPERATION = KEY;
        this.cv = cv;
        cursor = null;
        this.tableQuery = tableQuery;
        this.arg = arg;
        Log.d("FS","ss");
    }

    @Override
    public Cursor loadInBackground() {
        switch (OPERATION){
             case QUERY:
                cursor = dbHelper.getWritableDatabase().query(tableQuery, null, null, null, null, null, null);
                break;
            case DELETE:
                dbHelper.getWritableDatabase().delete(tableQuery, arg.getString(SELECT_COLUMN)+" = ?",arg.getStringArray(SELECT_ARG));
                break;
            case 8888:
                String table = "dtt as DT inner join officers as OFF on DT.id_officer = OFF._id inner join suspects as SUS on DT.id_sus = SUS._id inner join districts as DIS on DT.id_district = DIS._id";
                String columns[] = { "DT.name as Name", "OFF.name as Ofs","SUS.name as Ss", "DIS.name as Ds", "DT._id as ID" };
                String selection = "DT._id = ?";
                String[] selectionArgs = arg.getStringArray(SELECT_ARG);
                cursor = dbHelper.getWritableDatabase().query(table, columns, selection, selectionArgs, null, null, null);
                break;
        }
        return cursor;
    }
}