package com.example.task_databas.fragment.selected;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.task_databas.app.MyLoader;
import com.example.task_databas.app.R;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataDistrict;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataOfficer;
import com.example.task_databas.fragment.list.FragmentListDistricts;


/**
 * Created by Nikolay on 07.05.14.
 */
public class FragmentSelectedDistricts extends Fragment implements DialogInterface.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener  {

    final String LOG_TAG = "myLogs";

    Button butRedactData;
    Button butDelete;
    Bundle bundle;
    DialogFragment dlg1;
    FragmentTransaction fTrans;
    TextView textView;
    TextView textView2;
    long id;
    Bundle arg;
    String name;
    String count_people;

    public static FragmentSelectedDistricts newInstance(long id, String name, String count_people) {
        FragmentSelectedDistricts f = new FragmentSelectedDistricts();

        Bundle args = new Bundle();
        args.putLong("id", id);
        args.putString("name", name);
        args.putString("count_people", count_people);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle = new Bundle();
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        this.id = getArguments().getLong("id");
        this.name = getArguments().getString("name");
        this.count_people = getArguments().getString("count_people");

        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_selected_district, null);

        textView = (TextView) v.findViewById(R.id.textView);
        textView.setText(name);
        textView2 = (TextView) v.findViewById(R.id.textView2);
        textView2.setText(count_people);

        butDelete = (Button) v.findViewById(R.id.button2);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dl= onCreateDialog(0);
                Bundle arg = new Bundle();
                arg.putString("ID", ""+ id);
                dl.show();

            }
        });

        butRedactData = (Button) v.findViewById(R.id.button);
        butRedactData.setOnClickListener(this);



        return v;
    }

    @Override
    public void onClick(View view) {

        //==
        FragmentEnterDataDistrict fragEnterDistr = new FragmentEnterDataDistrict();
        Bundle args = new Bundle();
        args.putLong("id",id);
        args.putInt("operration", FragmentEnterDataDistrict.CORRECT);
        args.putString("name",name);
        args.putString("count_people", count_people);
        fragEnterDistr.setArguments(args);
        //===

        Log.d("MY_TAG", "ON_CLICK");
        fTrans = getActivity().getSupportFragmentManager().beginTransaction();
        fTrans.replace(R.id.fragment_id, fragEnterDistr);
        fTrans.addToBackStack("correct_district");
        fTrans.commit();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();
        //bundle.putString("KEY_QUERY","SELECT _id, name, rank FROM officers WHERE _id = ?");
        //bundle.putStringArray("KEY_ARGUMENT",new String[]{"1"});
        //someEventListener.someEvent(bundle,MainActivity.SELECT_OFFISER,this);
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }


    protected Dialog onCreateDialog(int id) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle("Delete");
        adb.setMessage("You are sure?");
        adb.setIcon(android.R.drawable.ic_dialog_info);
        adb.setPositiveButton("Yes", this);
        adb.setNegativeButton("No", this);


        return adb.create();
    }




    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case Dialog.BUTTON_POSITIVE:

                arg = new Bundle();
                arg.putString(MyLoader.SELECT_COLUMN,"_id");
                arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id});
                getActivity().getSupportLoaderManager().restartLoader(0, arg, this);
                getActivity().getSupportFragmentManager().popBackStackImmediate();

                break;
            case Dialog.BUTTON_NEGATIVE:
                break;
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyLoader(getActivity(), FragmentListDistricts.TABLE,null,args,MyLoader.DELETE);
        //return new MyLoader(getActivity(), FragmentListOfficers.TABLE,null,args,MyLoader.UPDATE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}