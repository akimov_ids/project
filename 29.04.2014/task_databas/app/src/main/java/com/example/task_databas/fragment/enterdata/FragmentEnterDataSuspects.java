package com.example.task_databas.fragment.enterdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.task_databas.app.MyAsinkTaskLoader;
import com.example.task_databas.app.MyLoader;
import com.example.task_databas.app.R;
import com.example.task_databas.fragment.list.FragmentListOfficers;
import com.example.task_databas.fragment.list.FragmentListSuspects;


/**
 * Created by Nikolay on 07.05.14.
 */
public class FragmentEnterDataSuspects extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public final static int ADD = 0;
    public final static int CORRECT = 1;
    final String LOG_TAG = "myLogs";
    EditText editText;
    EditText editText2;
    Bundle bundle;
    private long id;
    private int OPERRATION;
    String name;
    String article;
    Button button;
    AlertDialog.Builder builder;
    AlertDialog dataDialog;
    ContentValues cv;

    public static FragmentEnterDataSuspects newInstance(long id, int OPERRATION, String name, String article) {
        FragmentEnterDataSuspects f = new FragmentEnterDataSuspects();

        Bundle args = new Bundle();
        args.putLong("id", id);
        args.putInt("operration", OPERRATION);
        args.putString("name", name);
        args.putString("article",article);
        f.setArguments(args);
        return f;
    }
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle = new Bundle();
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_enter_data_suspect, null);
        cv = new ContentValues();
        editText = (EditText) v.findViewById(R.id.editText);
        editText2 = (EditText) v.findViewById(R.id.editText2);
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please enter all data");
        builder.setNegativeButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dataDialog = builder.create();
        OPERRATION = getArguments().getInt("operration");
        button = (Button) v.findViewById(R.id.btnYes);
        button.setOnClickListener(this);
        if(OPERRATION == CORRECT){
            button.setText("Edit");
            editText.setText(getArguments().getString("name"));
            editText2.setText(getArguments().getString("article"));
            this.id = getArguments().getLong("id");}
        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        if(editText.getText().toString().equals("")||
                editText2.getText().toString().equals("")){
            dataDialog.show();
        }else{

            if(OPERRATION == ADD){
                //getActivity().getSupportLoaderManager().restartLoader(0, null, this);
                Cursor cur;

                cv.put("name",editText.getText().toString());
                cv.put("article",editText2.getText().toString());

                MyAsinkTaskLoader mat = new MyAsinkTaskLoader(cv,getActivity(), FragmentListSuspects.TABLE,null);
                mat.execute(new Integer[]{MyAsinkTaskLoader.INSERT});
                getActivity().getSupportFragmentManager().popBackStackImmediate();

            }else{
                Bundle arg = new Bundle();
                arg.putString(MyLoader.SELECT_COLUMN,"_id");
                arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id});

                cv.put("name",editText.getText().toString());
                cv.put("article",editText2.getText().toString());

                MyAsinkTaskLoader mat = new MyAsinkTaskLoader(cv,getActivity(),FragmentListSuspects.TABLE,arg);
                mat.execute(new Integer[]{MyAsinkTaskLoader.UPDATE});
                getActivity().getSupportFragmentManager().popBackStackImmediate();

                //getActivity().getSupportLoaderManager().restartLoader(1, null, this);
//                    getActivity().getSupportFragmentManager().popBackStackImmediate();
            }


            /*if(id == ADD){
                getActivity().getSupportLoaderManager().restartLoader(2, null, this);
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }else{
                getActivity().getSupportLoaderManager().restartLoader(1, null, this);
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }*/
        }
    }


    public void onResume() {
        super.onResume();
        Log.d(LOG_TAG, "Fragment1 onResume");

    }

    public void onDismiss(DialogInterface dialog) {
        //super.onDismiss(dialog);
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        //super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        ContentValues cv = new ContentValues();
        if(OPERRATION == ADD){

            cv.put("name",editText.getText().toString());
            cv.put("article",editText2.getText().toString());
            return new MyLoader(getActivity(),"suspects", cv);
        }else if(OPERRATION == CORRECT){

            Bundle arg = new Bundle();
            arg.putString(MyLoader.SELECT_COLUMN,"_id");
            arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id});

            cv.put("name",editText.getText().toString());
            cv.put("article",editText2.getText().toString());
            return new MyLoader(getActivity(), FragmentListSuspects.TABLE,cv,arg,MyLoader.UPDATE);
        }else {
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}


