package com.example.task_databas.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

/**
 * Created by Николай on 03.05.14.
 */
public class MyAsinkTaskLoader extends AsyncTask<Integer, Void, Cursor> {

    private DBHelper dbHelper;
    public final static int INSERT = 0;
    public final static int UPDATE = 1;

    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";

    ContentValues cv;
    String tableQuery;
    Bundle args;
    //Cursor cursor;

    public MyAsinkTaskLoader(ContentValues cv, Context context,String tableQuery,Bundle args){
        this.cv = cv;
        dbHelper = new DBHelper(context,"mydb",null,1);
        this.tableQuery = tableQuery;
        this.args = args;
    }

    @Override
    protected Cursor doInBackground(Integer... key) {

        if (key[0] == INSERT){
            dbHelper.getWritableDatabase().insert(tableQuery,null,cv);
        }else if(key[0] == UPDATE){
            dbHelper.getWritableDatabase().update(tableQuery,cv,args.getString(SELECT_COLUMN)+" = ?",args.getStringArray(SELECT_ARG));
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Cursor result) {
        super.onPostExecute(result);

    }
}