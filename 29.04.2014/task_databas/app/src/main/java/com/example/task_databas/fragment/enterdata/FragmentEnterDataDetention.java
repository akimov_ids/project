package com.example.task_databas.fragment.enterdata;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.task_databas.app.MyAsinkTaskLoader;
import com.example.task_databas.app.MyLoader;
import com.example.task_databas.app.R;
import com.example.task_databas.fragment.list.FragmentListDetentions;
import com.example.task_databas.fragment.list.FragmentListDistricts;
import com.example.task_databas.fragment.list.FragmentListOfficers;

/**
 * Created by Николай on 01.05.14.
 */
public class FragmentEnterDataDetention extends Fragment implements View.OnClickListener, LoaderManager.LoaderCallbacks<Cursor> {

    public final static int ADD = 0;
    public final static int CORRECT = 1;
    final String LOG_TAG = "myLogs";
    EditText editText;
    EditText editText2;
    Bundle bundle;
    private long id;
    private int OPERRATION;
    String[] fromOff = new String[] { "rank","name" };
    String[] fromSus = new String[] {"article", "name" };
    String[] fromDistr = new String[] {"count_people", "name"};
    SimpleCursorAdapter scAdapterOff;
    SimpleCursorAdapter scAdapterSus;
    SimpleCursorAdapter scAdapterDistr;
    Spinner spinnerOff;
    Spinner spinnerSus;
    Spinner spinnerDistr;
    long id_off;
    long id_sus;
    long id_distr;
    Button button;
    ContentValues cv;
    Bundle arg;
    AlertDialog.Builder builder;
    AlertDialog dataDialog;

    int[] to = new int[] { R.id.ivImg, R.id.tvText };
    public static FragmentEnterDataDetention newInstance(long id, int OPERRATION) {
        FragmentEnterDataDetention f = new FragmentEnterDataDetention();
        Bundle args = new Bundle();
        args.putLong("id", id);
        args.putInt("operration", OPERRATION);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle = new Bundle();
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        OPERRATION = getArguments().getInt("operration");
        this.id = getArguments().getLong("id");
        View v = inflater.inflate(R.layout.fragment_enter_data_detention, null);
        editText = (EditText) v.findViewById(R.id.editText);
        this.id_off = -1;
        this.id_distr = -1;
        this.id_sus = -1;
        builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Please enter all data");
        cv = new ContentValues();
        builder.setNegativeButton("OK",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        dataDialog = builder.create();

        button = (Button) v.findViewById(R.id.butC);
        button.setOnClickListener(this);
        if(OPERRATION == CORRECT){
            button.setText("Edit");
        }
        spinnerOff = (Spinner) v.findViewById(R.id.spinner);
        spinnerSus = (Spinner) v.findViewById(R.id.spinner2);
        spinnerDistr = (Spinner) v.findViewById(R.id.spinner3);
        spinnerOff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.d("SPIN","id = "+id);
                id_off = id;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spinnerSus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                Log.d("SPIN","id = "+id);
                id_sus = id;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

        spinnerDistr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                Log.d("SPIN","id = "+id);
                id_distr = id;
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
        return v;
    }

    public void onClick(View v) {
        Log.d(LOG_TAG, "Dialog 1: " + ((Button) v).getText());
        if(editText.getText().toString().equals("")||
           this.id_distr==-1||this.id_sus==-1||this.id_off==-1){
            dataDialog.show();
        }else{
            if(OPERRATION == ADD){
                cv.put("name",editText.getText().toString());
                cv.put("id_officer",""+id_off);
                cv.put("id_sus",""+id_sus);
                cv.put("id_district",""+id_distr);

                MyAsinkTaskLoader mat = new MyAsinkTaskLoader(cv,getActivity(), FragmentListDetentions.TABLE,null);
                mat.execute(new Integer[]{MyAsinkTaskLoader.INSERT});
                getActivity().getSupportFragmentManager().popBackStackImmediate();

            }else{
                arg = new Bundle();
                arg.putString(MyLoader.SELECT_COLUMN,"_id");
                arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id});
                cv.put("name",editText.getText().toString());
                cv.put("id_officer",""+id_off);
                cv.put("id_sus",""+id_sus);
                cv.put("id_district",""+id_distr);

                MyAsinkTaskLoader mat = new MyAsinkTaskLoader(cv,getActivity(),FragmentListDetentions.TABLE,arg);
                mat.execute(new Integer[]{MyAsinkTaskLoader.UPDATE});
                getActivity().getSupportFragmentManager().popBackStackImmediate();
            }
        }
    }


    public void onResume() {
        super.onResume();
        scAdapterOff = new SimpleCursorAdapter(getActivity(), R.layout.item, null, fromOff, to, 0);
        scAdapterSus = new SimpleCursorAdapter(getActivity(), R.layout.item, null, fromSus, to, 0);
        scAdapterDistr = new SimpleCursorAdapter(getActivity(), R.layout.item, null, fromDistr, to, 0);
        getActivity().getSupportLoaderManager().initLoader(3, null, this);
        getActivity().getSupportLoaderManager().initLoader(4, null, this);
        getActivity().getSupportLoaderManager().initLoader(5, null, this);
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onDismiss(DialogInterface dialog) {
        Log.d(LOG_TAG, "Dialog 1: onDismiss");
    }

    public void onCancel(DialogInterface dialog) {
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(id == 3){
            return new MyLoader(getActivity(),"officers");
        }else if(id == 4){
            return new MyLoader(getActivity(),"suspects");
        }else if(id == 5) {
            return new MyLoader(getActivity(),"districts");
        }if(id == 9){
            cv = new ContentValues();
            cv.put("name",editText.getText().toString());
            cv.put("id_officer",""+id_off);
            cv.put("id_sus",""+id_sus);
            cv.put("id_district",""+id_distr);
            return new MyLoader(getActivity(),"dtt", cv);

        }else if(id == 7){
            cv = new ContentValues();
            arg = new Bundle();
            arg.putString(MyLoader.SELECT_COLUMN,"_id");
            arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id});
            cv.put("name",editText.getText().toString());
            cv.put("id_officer",""+id_off);
            cv.put("id_sus",""+id_sus);
            cv.put("id_district",""+id_distr);
            return new MyLoader(getActivity(), FragmentListDetentions.TABLE,cv,arg,MyLoader.UPDATE);
        }else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(loader.getId() == 3){
            scAdapterOff.swapCursor(data);
            spinnerOff.setAdapter(scAdapterOff);
        }else if(loader.getId() == 4){
            scAdapterSus.swapCursor(data);
            spinnerSus.setAdapter(scAdapterSus);
        }else if(loader.getId() == 5){
            scAdapterDistr.swapCursor(data);
            spinnerDistr.setAdapter(scAdapterDistr);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }

    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportLoaderManager().destroyLoader(3);
        getActivity().getSupportLoaderManager().destroyLoader(4);
        getActivity().getSupportLoaderManager().destroyLoader(5);
        getActivity().getSupportLoaderManager().destroyLoader(6);
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(3);
        getActivity().getSupportLoaderManager().destroyLoader(4);
        getActivity().getSupportLoaderManager().destroyLoader(5);
        getActivity().getSupportLoaderManager().destroyLoader(6);
        getActivity().getSupportLoaderManager().destroyLoader(7);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        getActivity().getSupportLoaderManager().destroyLoader(3);
        getActivity().getSupportLoaderManager().destroyLoader(4);
        getActivity().getSupportLoaderManager().destroyLoader(5);
        getActivity().getSupportLoaderManager().destroyLoader(6);
        getActivity().getSupportLoaderManager().destroyLoader(7);
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }
}
