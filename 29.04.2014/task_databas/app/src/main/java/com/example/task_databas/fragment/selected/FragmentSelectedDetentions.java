package com.example.task_databas.fragment.selected;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.task_databas.app.MyLoader;
import com.example.task_databas.app.R;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataDetention;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataOfficer;
import com.example.task_databas.fragment.list.FragmentListDetentions;
import com.example.task_databas.fragment.list.FragmentListOfficers;

/**
 * Created by Николай on 01.05.14.
 */
public class FragmentSelectedDetentions extends Fragment implements DialogInterface.OnClickListener, LoaderManager.LoaderCallbacks<Cursor>,View.OnClickListener  {

    final String LOG_TAG = "myLogs";

    Button butRedactData;
    Button butDelete;
    Bundle bundle;
    DialogFragment dlg1;
    FragmentTransaction fTrans;
    TextView textView;
    TextView textView2;
    TextView textView3;
    TextView textView4;
    long id;
    long id_det;
    Bundle arg;

    public static FragmentSelectedDetentions newInstance(long id) {
        FragmentSelectedDetentions f = new FragmentSelectedDetentions();
        Bundle args = new Bundle();
        args.putLong("id", id);
       f.setArguments(args);

        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        bundle = new Bundle();
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.id_det = getArguments().getLong("id");
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_selected_detentions, null);
        this.id_det = getArguments().getLong("id");
        textView = (TextView) v.findViewById(R.id.textView);
        textView2 = (TextView) v.findViewById(R.id.textView2);
        textView3 = (TextView) v.findViewById(R.id.textView3);
        textView4 = (TextView) v.findViewById(R.id.textView4);

        butDelete = (Button) v.findViewById(R.id.button2);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dialog dl= onCreateDialog(0);
                Bundle arg = new Bundle();
                arg.putString("ID", ""+ id);
                dl.show();
            }
        });

        butRedactData = (Button) v.findViewById(R.id.button);
        butRedactData.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {

        Log.d("MY_TAG", "ON_CLICK");
        fTrans = getActivity().getSupportFragmentManager().beginTransaction();

        FragmentEnterDataDetention fragEnterODet = new FragmentEnterDataDetention();
        Bundle args = new Bundle();
        args.putLong("id",id_det);
        args.putInt("operration", FragmentEnterDataDetention.CORRECT);

        fragEnterODet.setArguments(args);

        fTrans.replace(R.id.fragment_id, fragEnterODet);
        fTrans.addToBackStack("correct_detention");
        fTrans.commit();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();

        getActivity().getSupportLoaderManager().initLoader(10, null, this);
        Log.d(LOG_TAG, "Fragment1 onResume");
    }

    public void onPause() {
        super.onPause();
        getActivity().getSupportLoaderManager().destroyLoader(10);

        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        //getActivity().getSupportLoaderManager().initLoader(10, null, this);
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(10);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
          super.onDetach();
        getActivity().getSupportLoaderManager().destroyLoader(10);
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }


    protected Dialog onCreateDialog(int id) {

        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle("Delete");
        adb.setMessage("You are sure?");
        adb.setIcon(android.R.drawable.ic_dialog_info);
        adb.setPositiveButton("Yes", this);
        adb.setNegativeButton("No", this);


        return adb.create();
    }


    @Override
    public void onClick(DialogInterface dialog, int which) {

        switch (which) {
            case Dialog.BUTTON_POSITIVE:

                arg = new Bundle();
                arg.putString(MyLoader.SELECT_COLUMN,"_id");
                arg.putStringArray(MyLoader.SELECT_ARG, new String[]{"" + this.id_det});
                getActivity().getSupportLoaderManager().initLoader(0, arg, this);
                getActivity().getSupportFragmentManager().popBackStackImmediate();

                break;
            case Dialog.BUTTON_NEGATIVE:
                break;
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(id == 0){
            return new MyLoader(getActivity(), FragmentListDetentions.TABLE,null,args,MyLoader.DELETE);
        //return new MyLoader(getActivity(), FragmentListOfficers.TABLE,null,args,MyLoader.UPDATE);
        }else if(id == 10){
            Bundle bun = new Bundle();
            //bun.putStringArray(MyLoader.SELECT_ARG,new String[]{""+id_det,id_off, id_sus, id_distr});
            bun.putStringArray(MyLoader.SELECT_ARG,new String[]{""+id_det});
            return new MyLoader(getActivity(), null,null,bun,8888);
        }else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
       if(loader.getId() == 10){
           data.moveToNext();
           //data.moveToPosition(2);
           textView.setText(data.getString(0));
           textView2.setText(data.getString(1));
           textView3.setText(data.getString(2));
           textView4.setText(data.getString(3));
            data.close();
           //Log.d("SQ","sd3SSS = "+data.getCount());
           //Log.d("SQ","sd4 = "+data.getString(4));
       }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}