package com.example.task_databas.fragment.list;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.task_databas.app.MyLoader;
import com.example.task_databas.app.R;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataOfficer;
import com.example.task_databas.fragment.enterdata.FragmentEnterDataSuspects;
import com.example.task_databas.fragment.selected.FragmentSelectedOfficer;
import com.example.task_databas.fragment.selected.FragmentSelectedSuspects;


/**
 * Created by Nikolay on 07.05.14.
 */
public class FragmentListSuspects extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    Button butCorrectSuspects;
    Bundle bundle;
    SimpleCursorAdapter scAdapter;
    FragmentTransaction fTrans;
    public final static String TABLE = "suspects";
    Cursor cursor;


    final String LOG_TAG = "myLogs";
    String[] from = new String[] {"name", "article"};
    int[] to = new int[] { R.id.ivImg, R.id.tvText };
    ListView lvData;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Log.d(LOG_TAG, "Fragment1 onAttach");
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");

        scAdapter = new SimpleCursorAdapter(getActivity(), R.layout.item, null, from, to, 0);
        getActivity().getSupportLoaderManager().initLoader(2, null, this);
     }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_list_suspect, null);

        lvData = (ListView) v.findViewById(R.id.lvData);
        butCorrectSuspects = (Button) v.findViewById(R.id.button);
        butCorrectSuspects.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("MY_TAG", "ON_CLICK");
                fTrans = getActivity().getSupportFragmentManager().beginTransaction();
                FragmentEnterDataSuspects fragEnterSus = new FragmentEnterDataSuspects();
                Bundle args = new Bundle();
                args.putInt("operration", FragmentEnterDataOfficer.ADD);
                fragEnterSus.setArguments(args);
                fTrans.replace(R.id.fragment_id, fragEnterSus);
                fTrans.addToBackStack("add_suspect");
                fTrans.commit();
            }
        });

        lvData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Log.d(LOG_TAG, "itemClick: position = " + position + ", id = "+ id);
                cursor = scAdapter.getCursor();//.move(position).
                cursor.moveToPosition(position);
                Log.d("CURS","cur = "+cursor.getString(1));

                fTrans = getActivity().getSupportFragmentManager().beginTransaction();

                FragmentSelectedSuspects fragSelSus = new FragmentSelectedSuspects();
                Bundle args = new Bundle();
                args.putLong("id",id);
                args.putString("name",cursor.getString(1));
                args.putString("article",cursor.getString(2));
                fragSelSus.setArguments(args);

                fTrans.replace(R.id.fragment_id, fragSelSus);
                fTrans.addToBackStack("selected_suspects");
                fTrans.commit();

            }
        });
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }

    public void onStart() {
        super.onStart();
        Log.d(LOG_TAG, "Fragment1 onStart");
    }

    public void onResume() {
        super.onResume();

        getActivity().getSupportLoaderManager().restartLoader(2, null, this);
    }

    public void onPause() {
        super.onPause();
        Log.d(LOG_TAG, "Fragment1 onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d(LOG_TAG, "Fragment1 onStop");
    }

    public void onDestroyView() {
        super.onDestroyView();
        Log.d(LOG_TAG, "Fragment1 onDestroyView");
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(2);
        Log.d(LOG_TAG, "Fragment1 onDestroy");
    }

    public void onDetach() {
        super.onDetach();
        getActivity().getSupportLoaderManager().destroyLoader(2);
        Log.d(LOG_TAG, "Fragment1 onDetach");
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        //ContentValues cv;

        //return null;
        return new MyLoader(getActivity(),TABLE);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        scAdapter.swapCursor(data);
        lvData.setAdapter(scAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }


}

