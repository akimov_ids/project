package com.example.task_databas.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Nikolay on 07.05.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_CREATE_OFFICERS = "create table if not exists officers(_id integer primary key autoincrement, name text, rank text);";
    private static final String DB_CREATE_SUSPECTS = "create table if not exists suspects(_id integer primary key autoincrement, name text, article text);";
    private static final String DB_CREATE_DETENTIONS = "create table if not exists dtt(_id integer primary key autoincrement, " +
            "name text, " +
            "id_officer integer, " +
            "id_sus integer," +
            "id_district integer," +
            "FOREIGN KEY (id_officer) references officers(_id) ON DELETE CASCADE,    " +
            "FOREIGN KEY (id_sus) references suspects(_id) ON DELETE CASCADE," +
            "FOREIGN KEY (id_district) references districts(_id) ON DELETE CASCADE);";
    private static final String DB_CREATE_DISTRICT = "create table if not exists districts(_id integer primary key autoincrement, name text, count_people integer, count_crimes integer);";



    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory,
                    int version) {
        super(context, name, factory, version);
    }

    // создаем и заполняем БД
    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_OFFICERS);
            db.execSQL(DB_CREATE_SUSPECTS);
            db.execSQL(DB_CREATE_DISTRICT);
             db.execSQL(DB_CREATE_DETENTIONS);

    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }
}