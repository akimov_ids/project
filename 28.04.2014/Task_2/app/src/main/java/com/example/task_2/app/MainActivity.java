package com.example.task_2.app;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;

public class MainActivity extends ActionBarActivity {
    Intent intent;
    Button button;
    Button buttonGridView;
    Button buttonSpinner;
    Button buttonExpListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(MainActivity.this, ListViewActivity.class);
                //intent = new Intent(MainActivity.this,GridViewActivity.class);
                startActivity(intent);

            }
        });

        buttonGridView = (Button) findViewById(R.id.button2);
        buttonGridView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent = new Intent(MainActivity.this,GridViewActivity.class);
                startActivity(intent);

            }
        });

        buttonSpinner = (Button) findViewById(R.id.button3);
        buttonSpinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent = new Intent(MainActivity.this, SpinnerActivity.class);
                startActivity(intent);

            }
        });

        buttonExpListView = (Button) findViewById(R.id.button4);
        buttonExpListView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //intent = new Intent(MainActivity.this, ListViewActivity.class);
                intent = new Intent(MainActivity.this, ExpActivity.class);
                startActivity(intent);

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
