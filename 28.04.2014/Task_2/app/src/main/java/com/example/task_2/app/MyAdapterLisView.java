package com.example.task_2.app;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Nikolay on 28.04.14.
 */
public class MyAdapterLisView  extends ArrayAdapter<String> {
    private final Context context;
    private final String[] values;

    public MyAdapterLisView(Context context, String[] values) {
        super(context, R.layout.list_view_layout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_view_layout, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.textView);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.imageView);
        textView.setText(values[position]);
        String s = values[position];
        if (s.startsWith("Windows7") || s.startsWith("iPhone")
                || s.startsWith("Solaris")) {
            imageView.setImageResource(R.drawable.ic_launcher_no);
        } else {
            imageView.setImageResource(R.drawable.ic_launcher);
        }

        return rowView;
    }
}