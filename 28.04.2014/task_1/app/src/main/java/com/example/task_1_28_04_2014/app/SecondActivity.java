package com.example.task_1_28_04_2014.app;

//import android.app.FragmentTransaction;
import android.app.ActionBar;
import android.app.ListActivity;
import android.app.ListFragment;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;

import android.app.Activity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class SecondActivity extends ListActivity{

    String[] groupFrom;
    String[] childFrom;
    int[] childTo;
    int[] groupTo;
    GridView gridView;
    ListView listView;
    Spinner spiner;


    ArrayList<Map<String, String>> groupData;
    ArrayList<Map<String, String>> childDataItem;
    ArrayList<ArrayList<Map<String, String>>> childData;
    ArrayAdapter<String> adapter;
    SimpleExpandableListAdapter adapterS;
    Map<String, String> m;
    ExpandableListView expListView;
    FrameLayout fl;
    String[] groups = new String[] { "Зима", "Весна", "Лето", "Осень" };

    String[] winterMonths = new String[] { "Декабрь", "Январь", "Февраль" };
    String[] springMonths = new String[] { "Март", "Апрель", "Май" };
    String[] summerMonths = new String[] { "Июнь", "Июль", "Август" };
    String[] autumnMonths = new String[] { "Сентябрь", "Октябрь", "Ноябрь" };
    String[] numbers = new String[] {
            "A", "B", "C", "D", "E",
            "F", "G", "H", "I", "J",
            "K", "L", "M", "N", "O",
            "P", "Q", "R", "S", "T",
            "U", "V", "W", "X", "Y", "Z"};
    String[] data = {"one", "two", "three", "four", "five"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, numbers);

        int str = getIntent().getIntExtra(MainActivity.ACCESS_KEY,-1);
        switch(str) {
            case MainActivity.KEY_ONE:

                try{
                setContentView(R.layout.sec);
                gridView = new GridView(this);
               fl = (FrameLayout)findViewById(R.id.myfrm);
                    gridView.setNumColumns(4);
                    gridView.setAdapter(adapter);
                    fl.addView(gridView);
                }catch(Exception ex){
                    Log.d("MY_TAG", "Error = " + ex.getMessage());
                }
                break;

            case MainActivity.KEY_TWO:
                try{
                setContentView(R.layout.sec);
                listView = new ListView(this);
                FrameLayout fl = (FrameLayout)findViewById(R.id.myfrm);
                listView.setAdapter(adapter);
                fl.addView(listView);
                }catch (Exception ex){Log.d("MY_TAG","Error = "+ex.getMessage());}
                break;

         case MainActivity.KEY_THREE:
                setContentView(R.layout.sec_spinner);
                spiner = new Spinner(this);

             adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, data);
             adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

             spiner.setAdapter(adapter);
             spiner.setPrompt("Title");
             spiner.setSelection(2);
             spiner.setMinimumHeight(200);
             spiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                 @Override
                 public void onItemSelected(AdapterView<?> parent, View view,
                                            int position, long id) {
                       Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
                 }
                 @Override
                 public void onNothingSelected(AdapterView<?> arg0) {
                 }
             });

             fl = (FrameLayout)findViewById(R.id.myfrm);
             fl.addView(spiner);
             break;
            case MainActivity.KEY_FOUR:
                setContentView(R.layout.sec);
                groupData = new ArrayList<Map<String, String>>();
                for (String group : groups) {
                    m = new HashMap<String, String>();
                    m.put("groupName", group);
                    groupData.add(m);
                }

                groupFrom = new String[] { "groupName" };
                groupTo = new int[] { android.R.id.text1 };
                childData = new ArrayList<ArrayList<Map<String, String>>>();

              childDataItem = new ArrayList<Map<String, String>>();
                for (String month : winterMonths) {
                    m = new HashMap<String, String>();
                    m.put("monthName", month);
                    childDataItem.add(m);
                }

                childData.add(childDataItem);

                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : springMonths) {
                    m = new HashMap<String, String>();
                    m.put("monthName", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);

                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : summerMonths) {
                    m = new HashMap<String, String>();
                    m.put("monthName", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);

                childDataItem = new ArrayList<Map<String, String>>();
                for (String month : autumnMonths) {
                    m = new HashMap<String, String>();
                    m.put("monthName", month);
                    childDataItem.add(m);
                }
                childData.add(childDataItem);
                childFrom = new String[] { "monthName" };
                childTo = new int[] { android.R.id.text1 };

                try{
                    setContentView(R.layout.sec);
                    fl = (FrameLayout)findViewById(R.id.myfrm);
                    adapterS = new SimpleExpandableListAdapter(
                        this, groupData,
                        android.R.layout.simple_expandable_list_item_1, groupFrom,
                        groupTo, childData, android.R.layout.simple_list_item_1,
                        childFrom, childTo);
                expListView = new ExpandableListView(this);
                expListView.setAdapter(adapterS);
                fl.addView(expListView);
                }catch (Exception ex){Log.d("MY_TAG","Error = "+ex.getMessage());}
               break;
              case MainActivity.KEY_FIVE:
                setListAdapter(adapter);
                break;
        }
    }

}