package com.example.task_1_28_04_2014.app;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

public class MainActivity extends ActionBarActivity implements View.OnClickListener{

    final static int KEY_ONE = 0;
    final static int KEY_TWO = 1;
    final static int KEY_THREE = 2;
    final static int KEY_FOUR = 3;
    final static int KEY_FIVE = 4;
    final static String ACCESS_KEY = "KEY_INTRF";


    Intent intent;
    Button buttonOne;
    Button buttonTwo;
    Button buttonThree;
    Button buttonFour;
    Button buttonFive;
    Button buttonSix;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intent = new Intent(this,SecondActivity.class);
        buttonOne = (Button)findViewById(R.id.button);
        buttonOne.setOnClickListener(this);

        buttonTwo = (Button)findViewById(R.id.button2);
        buttonTwo.setOnClickListener(this);

        buttonThree = (Button)findViewById(R.id.button3);
        buttonThree.setOnClickListener(this);

        buttonFour = (Button)findViewById(R.id.button4);
        buttonFour.setOnClickListener(this);

        buttonFive = (Button)findViewById(R.id.button5);
        buttonFive.setOnClickListener(this);

        buttonSix = (Button)findViewById(R.id.button6);
        buttonSix.setOnClickListener(this);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        //intent.putExtra("KEY_ADAPTER","ADAPTER_ONE");
        //intent = new Intent(this,SecondActivity.class);
        switch(view.getId()) {
           case R.id.button:
               intent = new Intent(this,FragmentActivity.class);
               startActivity(intent);
               break;
            case R.id.button2:
                intent = new Intent(this,SecondActivity.class);
                intent.putExtra(ACCESS_KEY,KEY_ONE);
                startActivity(intent);
                break;
            case R.id.button3:
                intent = new Intent(this,SecondActivity.class);
                intent.putExtra(ACCESS_KEY,KEY_TWO);
                startActivity(intent);
                break;
            case R.id.button4:
                intent = new Intent(this,SecondActivity.class);
                intent.putExtra(ACCESS_KEY,KEY_THREE);
                startActivity(intent);
                break;
            case R.id.button5:
                intent = new Intent(this,SecondActivity.class);
                intent.putExtra(ACCESS_KEY,KEY_FOUR);
                startActivity(intent);
                break;
            case R.id.button6:
                intent = new Intent(this,SecondActivity.class);
                intent.putExtra(ACCESS_KEY,KEY_FIVE);
                startActivity(intent);
                break;
        }
        //intent = new Intent(this,FragmentActivity.class);
        //startActivity(intent);



    }
}
