package com.example.myapplication2.app;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.util.Calendar;
import java.util.TimeZone;

public class MainActivity extends ActionBarActivity {
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    protected void onResume()    {
        super.onResume();

        /*Calendar cal = Calendar.getInstance();
        Intent intent = new Intent(Intent.ACTION_EDIT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra("beginTime", cal.getTimeInMillis());
        intent.putExtra("allDay", false);
        intent.putExtra("rrule", "FREQ=DAILY");
        intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
        intent.putExtra("title", "A Test Event from android app");
        startActivity(intent);*/

        Button button = (Button) findViewById(R.id.button);

        /*button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {



                // get calendar
                Calendar cal = Calendar.getInstance();
                Uri EVENTS_URI = Uri.parse(getCalendarUriBase(MainActivity.this) + "events");
                ContentResolver cr = getContentResolver();

                // event insert
                ContentValues values = new ContentValues();
                values.put("calendar_id", 1);
                values.put("title", "Reminder Title");
                values.put("allDay", 0);
                values.put("dtstart", cal.getTimeInMillis() + 11*60*1000); // event starts at 11 minutes from now
                values.put("dtend", cal.getTimeInMillis()+60*60*1000); // ends 60 minutes from now
                values.put("description", "Reminder description");
                //values.put("visibility", 1);
                values.put("hasAlarm", 1);
                values.put("eventTimezone", TimeZone.getDefault().getID());

                Uri event = cr.insert(EVENTS_URI, values);

// reminder insert
                Uri REMINDERS_URI = Uri.parse(getCalendarUriBase(MainActivity.this) + "reminders");
                values = new ContentValues();
                values.put( "event_id", Long.parseLong(event.getLastPathSegment()));
                values.put( "method", 1 );
                values.put( "minutes", 10 );
                cr.insert( REMINDERS_URI, values );
            }
        });*/


    }


    private String getCalendarUriBase(Activity act) {

        String calendarUriBase = null;
        Uri calendars = Uri.parse("content://calendar/calendars");
        Cursor managedCursor = null;
        try {
            managedCursor = act.managedQuery(calendars, null, null, null, null);
        } catch (Exception e) {
        }
        if (managedCursor != null) {
            calendarUriBase = "content://calendar/";
        } else {
            calendars = Uri.parse("content://com.android.calendar/calendars");
            try {
                managedCursor = act.managedQuery(calendars, null, null, null, null);
            } catch (Exception e) {
            }
            if (managedCursor != null) {
                calendarUriBase = "content://com.android.calendar/";
            }
        }
        return calendarUriBase;
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
