package com.example.debtor_task.app.framework_code;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.example.debtor_task.app.MainActivity;

import java.io.File;
import java.util.Date;

/**
 * Created by Nikolay on 02.07.14.
 */
public class ManageFiles {
    private File file;
    private static ManageFiles manageFiles = new ManageFiles();

    public void deleteFile(String path) {
        file = new File(path);
        if(file.exists() && file.canWrite() && file.isFile())
            file.delete();
    }

    public String getRealPathFromURI(Uri contentURI, Context context) {
        String result;
        Cursor cursor = context.getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    public File createFile(String dirPhotoDeptor) {
        File dir = new File(dirPhotoDeptor);
        return new File(dir,  new Date().getTime()+".jpg");
    }

    public void createDir(String dirName) {
        File dir = new File(Environment.getExternalStorageDirectory()+"/"+dirName);
        if(!dir.exists() && !dir.isDirectory()) dir.mkdir();
    }

    public static ManageFiles getInstance() {
        return manageFiles;
    }

}
