package com.example.debtor_task.app.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.framework_code.Constants;
import com.example.debtor_task.app.parse_object.UserPars;
import com.example.debtor_task.app.display_fragments.LoginUserFragment;
import com.example.debtor_task.app.display_fragments.RegisterUserFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Nikolay on 10.06.14.
 */
public class LoginRegisterService  extends Service {

    public final static String LOGINED_PARSE_USERNAME = "LOGINED_PARSE_USERNAME";
    public final static String LOGINED_PARSE_PASS = "LOGINED_PARSE_PASS";

    public final static String KEY_SERVICE_TASK = "KEY_SERVICE_TASK";
    public final static int SERVICE_TASK_LOGINING = 0;
    public final static int SERVICE_TASK_REGISTRATION = 1;

    LoginRunnable loginRunnable;
    RegisterRunnable registerRunnable;

    Intent intent;
    ExecutorService es;


    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

         switch(intent.getIntExtra(KEY_SERVICE_TASK,-1)) {
            case SERVICE_TASK_LOGINING:
                loginRunnable = new LoginRunnable(intent.getStringExtra(LOGINED_PARSE_USERNAME),
                                                  intent.getStringExtra(LOGINED_PARSE_PASS));

                es = Executors.newFixedThreadPool(1);
                es.execute(loginRunnable);
                break;
            case SERVICE_TASK_REGISTRATION:
                registerRunnable = new RegisterRunnable(intent.getStringExtra(LOGINED_PARSE_USERNAME),
                        intent.getStringExtra(LOGINED_PARSE_PASS));
                es = Executors.newFixedThreadPool(2);
                es.execute(registerRunnable);
                break;
        }
        return Service.START_NOT_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        es.shutdown();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

   class LoginRunnable implements Runnable {

       String username;
       String pass;

       LoginRunnable(String username, String pass){
        this.username = username;
        this.pass = pass;
       }

       @Override
       public void run() {

           HttpClient client = new DefaultHttpClient();
           HttpGet get = new HttpGet("https://api.parse.com/1/login?username="+
                   username+"&password="+pass);

           get.setHeader("X-Parse-Application-Id", Constants.PARSE_ID);
           get.setHeader("X-Parse-REST-API-Key",Constants.PARSE_KEY);

           try{
               ResponseHandler<String> responseHandler=new BasicResponseHandler();
               String responsBody = client.execute(get,responseHandler);

               JSONObject response = new JSONObject(responsBody);
               LoginUserFragment.LOGINING_ID = response.getString("objectId");

               intent = new Intent(MainActivity.ACTION_BROADCASST_LOGIN);
               intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                       LoginUserFragment.BORADCAST_LOGIN_OK);
               sendBroadcast(intent);

           }catch(UnsupportedEncodingException ex){
               ex.printStackTrace();
           }catch(IOException ex){
               intent = new Intent(MainActivity.ACTION_BROADCASST_LOGIN);
               intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                       LoginUserFragment.BORADCAST_NO_CORRECT_LOGIN_OR_PASS);
               sendBroadcast(intent);

           }catch(JSONException ex){ex.printStackTrace();}
           stopSelf();
       }
    }

    class RegisterRunnable implements Runnable {

        String username;
        String pass;

        RegisterRunnable(String username, String pass){
            this.username = username;
            this.pass = pass;
        }

        @Override
        public void run() {
            UserPars userPars = new UserPars();
            userPars.setUsername(username);
            userPars.setPassword(pass);

            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("https://api.parse.com/1/users");
            HttpResponse response;

            post.setHeader("X-Parse-Application-Id", Constants.PARSE_ID);
            post.setHeader("X-Parse-REST-API-Key", Constants.PARSE_KEY);
            post.setHeader("Content-Type","application/json");

            try{
                StringEntity se = new StringEntity(gson.toJson(userPars));
                post.setEntity(se);
                response = client.execute(post);

                if(response.getStatusLine().getStatusCode() == 400){
                    throw new MyClasssException();
                }

                intent = new Intent(MainActivity.ACTION_BROADCASST_REGISTER);
                intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                        RegisterUserFragment.BORADCAST_SUCCESSFUL_REGISTRATION);

                intent.putExtra(RegisterUserFragment.RESULT_MASSAGE,getString(R.string.registration_is_successful));

                sendBroadcast(intent);
                stopSelf();
            }catch(UnsupportedEncodingException ex){
                ex.printStackTrace();
            }catch(IOException ex){ex.printStackTrace();
            }catch (MyClasssException ex){
                ex.getMyMassage();
            }
        }
    }

    class MyClasssException extends Exception{
        public void getMyMassage(){
            intent = new Intent(MainActivity.ACTION_BROADCASST_REGISTER);
            intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                    RegisterUserFragment.BORADCAST_ALREADY_REGISTER_LOGIN);
            intent.putExtra(RegisterUserFragment.RESULT_MASSAGE,getString(R.string.this_login_already_register));
            sendBroadcast(intent);

        }
    }

}