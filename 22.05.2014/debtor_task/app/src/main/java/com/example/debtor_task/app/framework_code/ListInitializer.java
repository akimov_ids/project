package com.example.debtor_task.app.framework_code;

import android.database.Cursor;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Nikolay on 27.06.14.
 */
public interface ListInitializer {
    void initialize(Cursor cursor);
    void setView(View view);
    TextView getTextView();
    ImageView getImageView();
    String getColumn();
}
