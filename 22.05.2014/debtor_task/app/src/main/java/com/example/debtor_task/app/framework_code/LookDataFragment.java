package com.example.debtor_task.app.framework_code;

//import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.R;

/**
 * Created by Nikolay on 22.05.14.
 */
public abstract class LookDataFragment extends FragmentManageData implements LoaderManager.LoaderCallbacks<Cursor> {

    final String LOG_TAG = "myLogs";

    public static final String KEY_OPERATION = "KEY_OP";

    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";
    public static final String SELECTION = "SELECTION";
    public static final int QUERY = 1;
    public static final int QUERY_ARG = 2;
    public static final int JOIN_QUERY_ARG = 4;
    public static final int JOIN_QUERY = 5;
    public static final int JOIN_QUERY_ARG_N = 6;

    private String tableQuery;
    private int typeQuery;

    public ActionBar actionBar;

    public abstract void takeData(Cursor data);

    public final void getData(Bundle args, String tableQuery, int typeQuery){
        this.tableQuery = tableQuery;
        this.typeQuery = typeQuery;
        getActivity().getSupportLoaderManager().initLoader(1, args, this);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
    }

    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportLoaderManager().destroyLoader(1);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), tableQuery, args,
                typeQuery);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        takeData(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    public static class MyCursorLoader extends CursorLoader {


        private DBHelper dbHelper;
        private Cursor cursor;
        private int OPERATION;
        private String tableQuery;
        private Bundle arg;

        public MyCursorLoader(Context context, String tableQuery, Bundle arg, int KEY) {
            super(context);
            dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
            OPERATION = KEY;
            cursor = null;
            this.tableQuery = tableQuery;
            this.arg = arg;
        }

        @Override
        public Cursor loadInBackground() {

            switch (OPERATION){
                case QUERY:

                    cursor = dbHelper.getReadableDatabase().query(tableQuery, null, null, null, null, null, null);
                    break;
                case QUERY_ARG:
                    String[] ar = arg.getStringArray(SELECT_ARG);
                    String[] sl = arg.getStringArray(SELECT_COLUMN);

                    cursor = dbHelper.getReadableDatabase().query(tableQuery,
                            arg.getStringArray(SELECT_COLUMN),
                            arg.getString(SELECTION)+" = ?",
                            arg.getStringArray(SELECT_ARG),
                            null,null,null);
                    break;
                case JOIN_QUERY_ARG:
                    String table = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                    String columns[] = { "DT."+DBHelper.D_NAME,
                            "DT."+DBHelper.D_SURNAME,
                            "DT."+DBHelper.D_EMAIL,
                            "DT."+DBHelper.D_PHONE,
                            "DT."+DBHelper.D_PATH,
                            "OFF."+DBHelper.MM_PATH_PHOTO_MATTER,
                            "OFF."+DBHelper.MM_SUMM,
                            "OFF."+DBHelper.MM_DATE_TAKE,
                            "OFF."+DBHelper.MM_DATE_RETURN,
                            "OFF."+DBHelper.MM_ID_DEBTOR,
                            "OFF."+DBHelper.MM_ID_PARSE,
                            "OFF."+DBHelper.MM_ID_PARSE_DATA};
                    String selection = "OFF._id = ?";
                    String[] selectionArgs = arg.getStringArray(SELECT_ARG);
                    cursor = dbHelper.getWritableDatabase().query(table, columns, selection, selectionArgs, null, null, null);
                    break;

                case JOIN_QUERY:
                    String tableLS = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                    String columnsLS[] = {
                            "DT."+DBHelper.D_PATH,
                            "DT."+DBHelper.D_NAME,
                            "OFF."+DBHelper.MM_SUMM,
                            "OFF."+DBHelper.MM_DATE_TAKE,
                            "OFF."+DBHelper.MM_DATE_RETURN,
                            "OFF."+DBHelper.MM_ID_PARSE};

                    String[] selectionArgsLS = arg.getStringArray(SELECT_ARG);
                    cursor = dbHelper.getWritableDatabase().query(tableLS, null, null, null, null, null, null);
                    break;

                case JOIN_QUERY_ARG_N:
                    table = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                    String columnsN[] = { "DT."+DBHelper.D_NAME,
                            "DT."+DBHelper.D_SURNAME,
                            "DT."+DBHelper.D_EMAIL,
                            "DT."+DBHelper.D_PHONE,
                            "DT."+DBHelper.D_PATH,
                            "OFF."+DBHelper.MM_PATH_PHOTO_MATTER,
                            "OFF."+DBHelper.MM_SUMM,
                            "OFF."+DBHelper.MM_DATE_TAKE,
                            "OFF."+DBHelper.MM_DATE_RETURN,
                            "OFF."+DBHelper.MM_ID_PARSE_DATA,
                            "OFF."+DBHelper.MM_ID};
                    selection = arg.getString(SELECT_COLUMN)+" = ?";
                    String[] selectionArgsN = arg.getStringArray(SELECT_ARG);
                    cursor = dbHelper.getWritableDatabase().query(table, columnsN, selection, selectionArgsN, null, null, null);
                    break;

            }
            return cursor;
        }
    }
}
