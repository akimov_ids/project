package com.example.debtor_task.app.framework_code;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public abstract class FragmentManageDataList extends FragmentManageData implements LoaderManager.LoaderCallbacks<Cursor> {

    private ListView listView;
    private String tableQuery;
    private int queryType;
    protected static ListInitializer listInitializer;
    private ActionBar actionBar;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    public void recompleteList(ListView listView,Bundle args,String tableQuery,int queryType) {
        getActivity().getSupportLoaderManager().destroyLoader(0);
        completeList(listView, args, tableQuery, queryType);
    }

    public void completeList(ListView listView,Bundle args,String tableQuery,int queryType) {
        this.listView = listView;
        this.tableQuery = tableQuery;
        this.queryType = queryType;
        getActivity().getSupportLoaderManager().initLoader(0, args, this);
    }

    public void setInitializer(ListInitializer listInitializer){
        this.listInitializer = listInitializer;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity().getSupportLoaderManager().destroyLoader(0);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new MyCursorLoader(getActivity(), tableQuery,args,queryType);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {

        if(data.getCount()>0){
            listView.setAdapter(new ItemAdapter(getActivity(),data));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    public static class ItemAdapter extends CursorAdapter {

        private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
        private ImageLoader imageLoader;
        private DisplayImageOptions options;

        Cursor cursor;
        Activity activity;
        public ItemAdapter(Activity activity, Cursor cursor) {
            super(activity, cursor);
            this.cursor = cursor;
            this.activity = activity;
            imageLoader = ImageLoader.getInstance();
            imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
            options = new DisplayImageOptions.Builder().showImageOnFail(R.drawable.no_photo).build();
        }

        @Override
        public long getItemId(int position) {
            cursor.moveToPosition(position);
            return cursor.getLong(cursor.getColumnIndex(DBHelper.D_ID));
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View view = convertView;

            if (convertView == null)
                view = activity.getLayoutInflater().inflate(R.layout.item_list_image, parent, false);

            cursor.moveToPosition(position);
            listInitializer.setView(view);
            listInitializer.initialize(cursor);
            String column = listInitializer.getColumn();
            if(cursor.getString(cursor.getColumnIndex(column)) != null&&
                    !cursor.getString(cursor.getColumnIndex(column)).equals("")){
                imageLoader.displayImage("file://" + cursor.getString(cursor.getColumnIndex(column)), listInitializer.getImageView(), options, animateFirstListener);
            }else{
                imageLoader.displayImage("assets://no_photo.png", listInitializer.getImageView(), options, animateFirstListener);}

            return view;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
            return null;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {}
    }


    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {
        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
                ImageView imageView = (ImageView) view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}

