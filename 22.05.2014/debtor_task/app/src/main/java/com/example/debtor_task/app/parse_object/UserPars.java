package com.example.debtor_task.app.parse_object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Николай on 23.05.14.
 */
public class UserPars {
    @Expose
    @SerializedName("username")
    public String username;
    @Expose
    @SerializedName("password")
    public String password;

    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
