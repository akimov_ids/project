package com.example.debtor_task.app.parse_object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nikolay on 02.06.14.
 */
public class ArrearsParse {

    @Expose
    @SerializedName("dateTake")
    public String dateTake;

    @Expose
    @SerializedName("dateReturn")
    public String dateReturn;

    @Expose
    @SerializedName("summ")
    public String summ;

    @Expose
    @SerializedName("idParseDebtor")
    public String idParseDebtor;

    @Expose
    @SerializedName("idParseData")
    public String idParseData;

    @Expose
    @SerializedName("id_parse_user")
    public String id_parse_user;

    @Expose
    @SerializedName("path_photo_metter")
    public String path_photo_metter;

    public void setDateTake(String dateTake) {
        this.dateTake = dateTake;
    }
    public void setDateReturn(String dateReturn) {this.dateReturn = dateReturn;}

    public void setSumm(String summ) {this.summ = summ;}

    public void setIdParseDeptor(String idParseDebtor) {
        this.idParseDebtor = idParseDebtor;
    }

    public void setIdParseData(String idParseData) {
        this.idParseData = idParseData;
    }

    public void setIdParseUser(String id_parse_user) {
        this.id_parse_user = id_parse_user;
    }

    public void setPathPhotoMetter(String path_photo_metter){
        this.path_photo_metter = path_photo_metter;
    }
}
