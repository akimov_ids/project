package com.example.debtor_task.app.framework_code;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.example.debtor_task.app.MainActivity;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Nikolay on 18.06.14.
 */
public class ManageWeb {
    private final String PATH_TO_FILE = "https://api.parse.com/1/files/";
    private final String PATH_TO_CLASSES = "https://api.parse.com/1/classes/";


    static final String UTF8 = "utf-8";

    String PARSE_ID;
    String PARSE_KEY;
    Header[] headers;

    public ManageWeb() {}
    public ManageWeb(String PARSE_ID,String PARSE_KEY) {
        this.PARSE_ID = PARSE_ID;
        this.PARSE_KEY = PARSE_KEY;
    }

    private ArrayList<String> getArrayList(JSONArray jArray){
        ArrayList<String> listData = new ArrayList<String>();
        if (jArray != null) {
            for (int i=0;i<jArray.length();i++){
                try{
                    listData.add(jArray.get(i).toString());
                }catch (JSONException ex){ex.printStackTrace();}
            }
        }
        return listData;
    }

    public String uploadFile(String pathPhotoMatter) {
        String line = PATH_TO_FILE+getNameFromPath(pathPhotoMatter);
        HttpPost post = new HttpPost(line);
        byte[] arr = new byte[1024];
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        byte[] byteArray;
        HttpResponse response;
        HttpClient client = new DefaultHttpClient();
        //client.getParams().setParameter("http.protocol.content-charset", "UTF-8");

        try{
            FileInputStream streamIn = new FileInputStream(pathPhotoMatter);
            int read;
            while ((read = streamIn.read(arr)) > -1){
                stream.write(arr, 0, read);
            };

            byteArray = stream.toByteArray();
            stream.close();
            streamIn.close();

            setHeadersImage(post);
            post.setEntity(new ByteArrayEntity(byteArray));

            response = client.execute(post);
            headers =  response.getAllHeaders();
            return headers[5].getValue();
        }catch (IOException ex){ex.printStackTrace();}

        return null;
    }

    public String uploadObject(String jsonString,String className) {

        HttpClient client = new DefaultHttpClient();
        //client.getParams().setParameter("http.protocol.content-charset", "UTF-8");
        HttpPost post = new HttpPost(PATH_TO_CLASSES+className);

        setHeadersJson(post);

        try{
            StringEntity se = new StringEntity(jsonString, HTTP.UTF_8);
            post.setEntity(se);

            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            String responsBody = client.execute(post,responseHandler);
            JSONObject responseJSON = new JSONObject(responsBody);

            return  responseJSON.getString(MainActivity.PARSE_RESULT_COLUMN_OBJECTID);
        }catch(UnsupportedEncodingException ex){
            ex.printStackTrace();
        }catch(IOException ex){ex.printStackTrace();
        }catch(JSONException ex){ex.printStackTrace();}

        return null;
    }
    public void deleteData(String object,String id) {
        HttpClient client = new DefaultHttpClient();
        HttpDelete delete = new HttpDelete(PATH_TO_CLASSES+object+"/"+id);
        setHeadersAuthentication(delete);
        try{
            client.execute(delete);
        }catch(IOException ex){ ex.printStackTrace();}
    }

    public JSONArray loadObject(String url, String where) {
        String query;
        HttpGet get;
        String responsBody;
        JSONObject response;


        try{
            query = URLEncoder.encode("{\"id_parse_user\":\"" + where + "\"}", UTF8);
            url = url + query;

            get = new HttpGet(url);
            HttpClient client = new DefaultHttpClient();
            //client.getParams().setParameter("http.protocol.content-charset", "UTF-8");
            setHeadersAuthentication(get);
            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            responsBody = client.execute(get,responseHandler);
            response = new JSONObject(responsBody);

            return response.getJSONArray("results");
        }catch(UnsupportedEncodingException  ex){ex.printStackTrace();
        }catch(JSONException ex){ ex.printStackTrace();
        }catch(IOException ex){ex.printStackTrace();}

        return null;
    }

    /*public void updateObject(String url,String calssName,String idObject){
        HttpPut put = new HttpPut(url);
    }*/

    public String updateObject(String jsonString,String className,String idObject) {

        HttpClient client = new DefaultHttpClient();
        //client.getParams().setParameter("http.protocol.content-charset", "UTF-8");
        HttpPut put = new HttpPut(PATH_TO_CLASSES+className+"/"+"jBNoe3BXbE");
        setHeadersJson(put);
        try{
            StringEntity se = new StringEntity(jsonString);
            put.setEntity(se);

            ResponseHandler<String> responseHandler=new BasicResponseHandler();
            String responsBody = client.execute(put,responseHandler);
            JSONObject responseJSON = new JSONObject(responsBody);
            return  responseJSON.getString(MainActivity.PARSE_RESULT_COLUMN_OBJECTID);
        }catch(UnsupportedEncodingException ex){
            ex.printStackTrace();
        }catch(IOException ex){ex.printStackTrace();
        }catch(JSONException ex){ex.printStackTrace();}
        return null;
    }



    public boolean hasConnection(final Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        wifiInfo = cm.getActiveNetworkInfo();
        if (wifiInfo != null && wifiInfo.isConnected()) {
            return true;
        }
        return false;
    }

    private String getNameFromPath(String path){
        return path.substring(path.lastIndexOf("/") + 1);
    }

    public void setHeadersJson(AbstractHttpMessage http) {
        setHeadersAuthentication(http);
        http.setHeader("Content-Type","application/json;");
    }

    public void setHeadersImage(AbstractHttpMessage http) {
        setHeadersAuthentication(http);
        http.setHeader("Content-Type","image/jpeg");
    }

    private void setHeadersAuthentication(AbstractHttpMessage http) {
        http.setHeader("X-Parse-Application-Id",PARSE_ID);
        http.setHeader("X-Parse-REST-API-Key", PARSE_KEY);
    }



}


