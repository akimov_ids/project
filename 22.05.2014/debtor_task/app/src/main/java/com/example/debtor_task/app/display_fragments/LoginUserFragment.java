package com.example.debtor_task.app.display_fragments;

import android.annotation.TargetApi;
//import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;

import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.DebtorsAppMenu;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.framework_code.Dialoger;
import com.example.debtor_task.app.framework_code.ManageWeb;
import com.example.debtor_task.app.services.LoginRegisterService;
import com.example.debtor_task.app.services.UpdateFromParse;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Николай on 23.05.14.
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class LoginUserFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{

    private final  int HANDLE_JAMP_MENU = 0;
    public final String SELECT_TABLE = "SELECT_TABLE";

    Button butRegister;
    Button butLogin;
    EditText editTextLogin;
    EditText editTextPass;
    ProgressDialog pd;
    BroadcastReceiver br;
    InputMethodManager imm;
    DebtorsAppMenu debtorsAppMenu;
    Dialoger dialoger;
    ManageWeb manageWeb;

    Handler handler;

    Intent intentUpdateFromParseService;
    HashMap<String,Long> idMap = new HashMap<String, Long>();
    ArrayList<String> arrayListArrearsFromDB = new ArrayList<String>();
    ArrayList<String> arrayListDebtorsFromDB = new ArrayList<String>();

    public static String LOGINING_ID;
    public static final int BORADCAST_NO_CORRECT_LOGIN_OR_PASS = 0;
    public static final int BORADCAST_LOGIN_OK = 1;
    ActionBar actionBar;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        menu.clear();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actionBar = ((ActionBarActivity)activity).getSupportActionBar();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manageWeb = new ManageWeb();
        debtorsAppMenu = new DebtorsAppMenu(getActivity().getSupportFragmentManager());
        dialoger = new Dialoger(getActivity());
        actionBar.setTitle(getResources().getString(R.string.logining));

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg)
            {
                if(msg.what == HANDLE_JAMP_MENU)
                {
                    debtorsAppMenu.goToItem(DebtorsAppMenu.LIST_DEBTORS,null);
                }
            }
        };

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                switch(intent.getIntExtra(MainActivity.KEY_TASK_BROADCASST,-1)){
                    case BORADCAST_NO_CORRECT_LOGIN_OR_PASS:
                        pd.cancel();
                        dialoger.alertDialor(getResources().getString(R.string.no_correct_login_or_password));
                        break;
                    case BORADCAST_LOGIN_OK:
                        pd.cancel();
                        intentUpdateFromParseService = new Intent(getActivity(), UpdateFromParse.class);
                        Bundle bundle = new Bundle();
                        bundle.putString(SELECT_TABLE, DBHelper.TABLE_DEPTOR);
                        getLoaderManager().initLoader(MainActivity.PARSE_LOADER_DEBTORS, bundle, LoginUserFragment.this);
                        break;
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter(MainActivity.ACTION_BROADCASST_LOGIN);
        getActivity().registerReceiver(br, intentFilter);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_user_fragment, container, false);

        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        editTextLogin = (EditText) v.findViewById(R.id.editText);
        editTextPass = (EditText) v.findViewById(R.id.editText2);

        pd = new ProgressDialog(getActivity());
        pd.setTitle(getResources().getString(R.string.logining));
        pd.setMessage(getResources().getString(R.string.wait));
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        butLogin = (Button) v.findViewById(R.id.button);
        butLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
                pd.show();
                if(!manageWeb.hasConnection(getActivity())){
                    pd.cancel();
                    dialoger.alertDialor(getResources().getString(R.string.no_internet_connection));
                }else if(isCorrectEnterData()){

                    Intent intent = new Intent(getActivity(), LoginRegisterService.class);
                    intent.putExtra(LoginRegisterService.LOGINED_PARSE_USERNAME,editTextLogin.getText().toString())
                            .putExtra(LoginRegisterService.LOGINED_PARSE_PASS, editTextPass.getText().toString())
                            .putExtra(LoginRegisterService.KEY_SERVICE_TASK, LoginRegisterService.SERVICE_TASK_LOGINING);

                    getActivity().startService(intent);
                }
            }
        });


        butRegister = (Button) v.findViewById(R.id.button2);
        butRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                debtorsAppMenu.goToItem(DebtorsAppMenu.REGISTRATION,null);
            }
        });
        return v;
    }


    boolean isCorrectEnterData(){
        boolean filledField = !(editTextLogin.getText().toString().equals("")||
                editTextPass.getText().toString().equals(""));

        if (filledField) {
            return true;
        } else {
            dialoger.alertDialor(getResources().getString(R.string.enter_all_values));
        }
        return false;
    }

    public void onDestroyView() {
        super.onDestroyView();
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(br);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //return new MyCursorLoader(getActivity(), args.getString(SELECT_TABLE));

        switch(id) {
            case MainActivity.PARSE_LOADER_DEBTORS:
                return new MyCursorLoader(getActivity(), DBHelper.TABLE_DEPTOR);
            case MainActivity.PARSE_LOADER_ARREARS:
                return new MyCursorLoader(getActivity(), DBHelper.TABLE_MANY_METTER);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        String name = "";

      switch (loader.getId()) {
            case MainActivity.PARSE_LOADER_DEBTORS:
                for(data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                    name = data.getString(data.getColumnIndex(DBHelper.D_ID_PARSE_DATA));
                    arrayListDebtorsFromDB.add(name);
                }
                getLoaderManager().initLoader(MainActivity.PARSE_LOADER_ARREARS, null, this);
                break;

            case MainActivity.PARSE_LOADER_ARREARS:
                for(data.moveToFirst(); !data.isAfterLast(); data.moveToNext()) {
                    name = data.getString(data.getColumnIndex(DBHelper.MM_ID_PARSE_DATA));
                    arrayListArrearsFromDB.add(name);
                }
                intentUpdateFromParseService.putStringArrayListExtra(UpdateFromParse.ARRAY_LIST_DEBTORS_FROM_DB,
                        arrayListDebtorsFromDB);

                intentUpdateFromParseService.putStringArrayListExtra(UpdateFromParse.ARRAY_LIST_ARREARS_FROM_DB,
                        arrayListArrearsFromDB);

                getActivity().startService(intentUpdateFromParseService);
                handler.sendEmptyMessage(HANDLE_JAMP_MENU);
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}
}

