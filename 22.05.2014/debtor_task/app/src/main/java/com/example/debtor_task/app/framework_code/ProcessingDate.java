package com.example.debtor_task.app.framework_code;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by Nikolay on 17.06.14.
 */
public class ProcessingDate {
    static Calendar calendar;

    public ProcessingDate(){
        calendar = Calendar.getInstance();
    }

    public static String getStringData(long unixFormat){
        calendar.clear();
        calendar.setTime(new Date(unixFormat));

        return calendar.get(Calendar.DAY_OF_MONTH)+"-"+
                (calendar.get(Calendar.MONTH)+1)+"-"+
                calendar.get(Calendar.YEAR);
    }
}
