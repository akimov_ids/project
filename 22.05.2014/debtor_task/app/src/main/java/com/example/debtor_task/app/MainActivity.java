package com.example.debtor_task.app;
import android.content.Context;
import android.content.Intent;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;

import com.example.debtor_task.app.display_fragments.ListDebtorFragment;
import com.example.debtor_task.app.display_fragments.AddDebtorFragment;
import com.example.debtor_task.app.display_fragments.ListDeptsFragment;
import com.example.debtor_task.app.display_fragments.LoginUserFragment;
import com.example.debtor_task.app.framework_code.ManageFiles;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends ActionBarActivity{

    public final static String NO_PHOTO_FILE = "no_photo.png";
    public final static String PATH = "path";

    public static final String BACK_STACK_ACCOUNT_USER = "BACK_STACK_ACCOUNT_USER";
    public static final String BACK_STACK_LOOK_DEPTOR = "BACK_STACK_LOOK_DEPTOR";
    public static final String BACK_STACK_MENU = "BACK_STACK_MENU";
    public static final String BACK_STACK_LOOK_ARREARS =  "BACK_STACK_LOOK_ARREARS";
    public static final String BACK_STACK_REDACKT_DEPTOR =  "BACK_STACK_REDACKT_DEPTOR";
    public static final String BACK_STACK_REDACKT_ARREARS = "BACK_STACK_REDACKT_ARREARS";
    public static final String DIR_NAME =  "myfotodeb";

    public static String ACTION_BROADCASST = "ACTION_BROADCASST";
    public static String ACTION_BROADCASST_LOGIN = "ACTION_BROADCASST_LOGIN";
    public static String ACTION_BROADCASST_REGISTER = "ACTION_BROADCASST_REGISTER";

    public static String KEY_TASK_BROADCASST = "KEY_TASK_BROADCASST";
    public static final int BORADCAST_LOAD_DATA_DEBTOR = 0;
    public static final int BORADCAST_LOAD_DATA_ARREAR = 1;

    public static final String PARSE_RESULT_COLUMN_OBJECTID = "objectId";
    public static final String LOGINED_USER_ID = "LOGINED_USER_ID";

    public static final int REQUEST_ACTION_PICK = 0;
    public static final int REQUEST_PHOTO = 1;
    //public static final int REQUEST_PHOTO_DEBTOR = 1;
    //public static final int  REQUEST_PHOTO_MATTER = 2;
    //public static final int  REQUEST_SELECT_PHOTO_DEBTOR = 3;

    public static final int INSERT_DEPTOR = 0;
    public static final int INSERT_MANY_MATTER = 1;
    public static final int PARSE_LOADER_DEBTORS = 5;
    public static final int PARSE_LOADER_ARREARS = 6;

    public static final String CLASS_NAME_DEBTOR = "debtors";
    public static final String CLASS_NAME_ARREARS = "arrears";

    public final static String PATH_IMAGE_FROM_CAMERA = "0";

    DebtorsAppMenu debtorsAppMenu;
    MyFragmentManager myFragmentManager;
    SharedPreferences prefs;
    ActionBar actionBar;
    MyFragmentManager fragmentManager;
    Fragment fragment;

    private ManageFiles manageFiles;

    static InputStream istr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = MyFragmentManager.getInstance();
        manageFiles = ManageFiles.getInstance();
        try{
            istr = getAssets().open("no_photo.png");
        }catch(IOException ex){ex.printStackTrace();}


        myFragmentManager = MyFragmentManager.getInstance();
        myFragmentManager.setFragmentManager(getSupportFragmentManager());
        debtorsAppMenu = new DebtorsAppMenu(getSupportFragmentManager());

        manageFiles.createDir(DIR_NAME);

        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        LoginUserFragment.LOGINING_ID = prefs.getString(LOGINED_USER_ID, "");

        if(LoginUserFragment.LOGINING_ID.equals(""))
            myFragmentManager.add(new LoginUserFragment(),R.id.frgmCont,null);
        else
            myFragmentManager.add(new ListDebtorFragment(),R.id.frgmCont,null);


        actionBar = getSupportActionBar();
        HashMap<Long,String> itemMap = new HashMap<Long,String>();
        itemMap.put((long)DebtorsAppMenu.LIST_DEBTORS,"List debtors");
        itemMap.put((long)DebtorsAppMenu.LIST_ARREARS,"List arrears");

        MenuBarAdapter aAdpt = new MenuBarAdapter(this, android.R.layout.simple_list_item_1, itemMap);
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setListNavigationCallbacks(aAdpt, new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int i, long id) {

                switch((int)id) {
                    case DebtorsAppMenu.LIST_ARREARS:
                        fragment = new ListDeptsFragment();
                        break;

                    case DebtorsAppMenu.LIST_DEBTORS:
                        fragment = new ListDebtorFragment();
                        break;
                }

                fragmentManager.replace(fragment, R.id.frgmCont, null);
                return false;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.logout) {
            LoginUserFragment.LOGINING_ID = "";
            prefs.edit().clear().commit();
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
            debtorsAppMenu.goToItem(DebtorsAppMenu.LOGINING,null);
            actionBar.setDisplayShowTitleEnabled(true);
        }
        return super.onOptionsItemSelected(item);
    }


    /********/
    public static Bitmap getCultivatedBitmap(String pathImage) {

        File image = new File(pathImage);
        Bitmap bmp = null;

        if (image.exists()&&image.isFile()&&image.canRead()) {
            int scale = 1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(image.getAbsolutePath(),options);

            if (options.outWidth > options.outHeight) {
                scale = options.outWidth/400;
            } else {
                scale = options.outHeight/400;
            }

            ExifInterface exif;
            int orientation = -1;
            try {
                exif = new ExifInterface(image.getAbsolutePath());
                orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, 1);
            } catch (IOException ex){ex.printStackTrace();}

            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);
            if (bmp != null && !bmp.isRecycled()) {
                Matrix matrix = new Matrix();
                switch(orientation){
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
                return Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
            }
        }
        return BitmapFactory.decodeStream(istr);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

   private class MenuBarAdapter extends ArrayAdapter<String>{

        HashMap<Long,String> mapItem;

        public MenuBarAdapter(Context context, int textViewResourceId, HashMap<Long,String> mapItem) {
            super(context, textViewResourceId, new ArrayList<String>(mapItem.values()));
            this.mapItem = mapItem;
        }

        @Override
        public long getItemId(int position){
            return getKeyFromValue(mapItem, getItem(position));
        }

        public Long getKeyFromValue(Map hm, Object value) {
            for (Object o : hm.keySet()) {
                if (hm.get(o).equals(value)) {
                    return (Long)o;
                }
            }
            return null;
        }

    }


}
