package com.example.debtor_task.app;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Nikolay on 17.06.14.
 */
public class MyFragmentManager {

    private static MyFragmentManager myFragmentManager = new MyFragmentManager();
    private FragmentTransaction fragmentTransaction;
    private FragmentManager fragmentManager;

    private MyFragmentManager(){}

    public void add(Fragment fragment, int idResource,Bundle bundle){
        fragmentTransaction = fragmentManager.beginTransaction();
        if(bundle != null)
            fragment.setArguments(bundle);
        fragmentTransaction.add(idResource,fragment);
        fragmentTransaction.commit();
    }

    public void replace(Fragment fragment, int idResource,Bundle bundle, String backStack){
        fragmentTransaction = fragmentManager.beginTransaction();
        if(bundle != null)
            fragment.setArguments(bundle);
        fragmentTransaction.addToBackStack(backStack);
        fragmentTransaction.replace(idResource, fragment);
        fragmentTransaction.commit();
    }

    public void replace(Fragment fragment, int idResource,Bundle bundle){
        fragmentTransaction = fragmentManager.beginTransaction();
        if(bundle != null)
            fragment.setArguments(bundle);
        fragmentTransaction.replace(idResource, fragment);
        fragmentTransaction.commit();
    }

    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }

    public static MyFragmentManager getInstance() {
        return myFragmentManager;
    }
}
