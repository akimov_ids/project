package com.example.debtor_task.app.framework_code;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.ImageView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.database_processing.AsyncTaskWriter;
import com.example.debtor_task.app.services.LoadToParseService;

import java.io.File;
import java.lang.reflect.Array;

/**
 * Created by Nikolay on 18.06.14.
 */
public abstract class FragmentManageData extends Fragment {

    final String LOG_TAG = "myLogs";

    public ImageView imageView;
    public boolean isHavePhoto = false;
    public String pathPhoto;

    ProgressDialog pdR;
    BroadcastReceiver br;
    Intent intentLoadToParseService;
    boolean tmIsStop = true;
    AsyncTaskWriterDebtor asynkWriter;
    ManageFiles manageFiles;

    public final static String MANAGER_DATA_BROADCASST = "MANAGER_DATA_BROADCASST";
    public final static String MANAGER_DATA_BROADCASST_IS_UPDATE = "MANAGER_DATA_BROADCASST_IS_UPDATE";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageFiles = ManageFiles.getInstance();
        intentLoadToParseService = new Intent(getActivity(), LoadToParseService.class);
        asynkWriter = new AsyncTaskWriterDebtor();
        asynkWriter.setDataBase(getActivity(), DBHelper.DB_NAME);
        pdR = new ProgressDialog(getActivity());
        pdR.setTitle(getString(R.string.load_data_in_cloud));
        pdR.setMessage(getString(R.string.wait));
        pdR.setCanceledOnTouchOutside(false);
        pdR.setCancelable(false);
    }

    final protected void addData(final ContentValues cv,final String tableQuery,Bundle args){
        tmIsStop = false;
        intentLoadToParseService.putExtras(args);
        pdR.show();
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                cv.put(DBHelper.D_ID_PARSE_DATA, intent.getStringExtra(LoadToParseService.LOAD_PARSE_SELECT_ID_PARSE_DEBTOR));
                asynkWriter.insert(cv,tableQuery);
            }
        };
        IntentFilter intentFilter = new IntentFilter(MANAGER_DATA_BROADCASST);
        getActivity().registerReceiver(br, intentFilter);
        getActivity().startService(intentLoadToParseService);
    }


    final public void updateData(final ContentValues cv,
                                    final String id,
                                    final String tableQuery,
                                    Bundle args){

        tmIsStop = false;
        intentLoadToParseService.putExtras(args);
        pdR.show();
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                asynkWriter.update(cv, tableQuery,new String[]{id});
            }
        };

        IntentFilter intentFilter = new IntentFilter(MANAGER_DATA_BROADCASST);
        getActivity().registerReceiver(br, intentFilter);
        getActivity().startService(intentLoadToParseService);
    }

    final protected void deleteData(final Long selectId, final String tableQuery,String idParseData){
        pdR.show();
        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Bundle args = new Bundle();
                args.putString(AsyncTaskWriter.SELECT_COLUMN,DBHelper.D_ID);
                args.putStringArray(AsyncTaskWriter.SELECT_ARG,new String[]{""+selectId});
                if(pathPhoto!=null)
                    new File(pathPhoto).delete();
                asynkWriter.delete(args,tableQuery);
            }
        };

        Intent intent = new Intent(getActivity(), LoadToParseService.class);
        intent.putExtra(LoadToParseService.LOAD_PARSE_OBJECTID,idParseData)
                .putExtra(LoadToParseService.LOAD_PARSE_CLASS_NAME, MainActivity.CLASS_NAME_ARREARS)
                .putExtra(LoadToParseService.KEY_SERVICE_TASK,LoadToParseService.SERVICE_TASK_DELETE);

        IntentFilter intentFilter = new IntentFilter(MANAGER_DATA_BROADCASST);
        getActivity().registerReceiver(br, intentFilter);
        getActivity().startService(intent);
    }




    protected void createPhoto() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, MainActivity.REQUEST_PHOTO);
    }

    protected void selectPhoto() {
        String dirPhoto = Environment.getExternalStorageDirectory()+"/myfotodeb";
        File photo = manageFiles.createFile(dirPhoto);
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        i.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo));
        startActivityForResult(i, MainActivity.REQUEST_PHOTO);
    }

    protected void onPostLoadData(){
        pdR.cancel();
        getActivity().unregisterReceiver(br);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == Activity.RESULT_OK && requestCode == MainActivity.REQUEST_PHOTO) {
            pathPhoto = manageFiles.getRealPathFromURI(data.getData(),getActivity());
            imageView.setImageBitmap(MainActivity.getCultivatedBitmap(pathPhoto));
            isHavePhoto = true;
        }
    }

    class AsyncTaskWriterDebtor extends AsyncTaskWriter {
        @Override
        protected void onPostExecute(Long result) {
            super.onPostExecute(result);
            onPostLoadData();
        }
    }
}