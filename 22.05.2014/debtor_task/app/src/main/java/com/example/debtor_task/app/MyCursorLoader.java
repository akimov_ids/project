package com.example.debtor_task.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.util.Log;

/**
 * Created by Nikolay on 12.05.14.
 */
public class MyCursorLoader extends CursorLoader {
    public static final String KEY_OPERATION = "KEY_OP";

    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";
    public static final String SELECTION = "SELECTION";
    public static final int QUERY = 1;
    public static final int QUERY_ARG = 2;
    public static final int JOIN_QUERY_ARG = 4;
    public static final int JOIN_QUERY = 5;
    public static final int JOIN_QUERY_ARG_N = 6;

    public static final String JOIIN_METTER = "OFF.";

    private DBHelper dbHelper;
    private Cursor cursor;
    private int OPERATION;
    private String tableQuery;
    private Bundle arg;

    public MyCursorLoader(Context context, String tableQuery) {
        super(context);
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        OPERATION = QUERY;
        cursor = null;
        this.tableQuery = tableQuery;
    }

    public MyCursorLoader(Context context, String tableQuery, Bundle arg, int KEY) {
        super(context);
        dbHelper = new DBHelper(context,DBHelper.DB_NAME,null,1);
        OPERATION = KEY;
        cursor = null;
        this.tableQuery = tableQuery;
        this.arg = arg;
    }

    @Override
    public Cursor loadInBackground() {

        switch (OPERATION){
            case QUERY:

                cursor = dbHelper.getReadableDatabase().query(tableQuery, null, null, null, null, null, null);
                 break;
            case QUERY_ARG:
                String[] ar = arg.getStringArray(SELECT_ARG);
                String[] sl = arg.getStringArray(SELECT_COLUMN);

                cursor = dbHelper.getReadableDatabase().query(tableQuery,
                                                              arg.getStringArray(SELECT_COLUMN),
                                                              arg.getString(SELECTION)+" = ?",
                                                              arg.getStringArray(SELECT_ARG),
                                                              null,null,null);
                break;
            case JOIN_QUERY_ARG:
                String table = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                String columns[] = { "DT."+DBHelper.D_NAME,
                                     "DT."+DBHelper.D_SURNAME,
                                     "DT."+DBHelper.D_EMAIL,
                                     "DT."+DBHelper.D_PHONE,
                                     "DT."+DBHelper.D_PATH,
                                     "OFF."+DBHelper.MM_PATH_PHOTO_MATTER,
                                     "OFF."+DBHelper.MM_SUMM,
                                     "OFF."+DBHelper.MM_DATE_TAKE,
                                     "OFF."+DBHelper.MM_DATE_RETURN,
                                     "OFF."+DBHelper.MM_ID_PARSE,
                                     "OFF."+DBHelper.MM_ID_PARSE_DATA};
                String selection = "OFF._id = ?";
                String[] selectionArgs = arg.getStringArray(SELECT_ARG);
                cursor = dbHelper.getReadableDatabase().query(table, columns, selection, selectionArgs, null, null, null);
                break;

            case JOIN_QUERY:
                String tableLS = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                String columnsLS[] = {
                        "DT."+DBHelper.D_PATH,
                        "DT."+DBHelper.D_NAME,
                        "OFF."+DBHelper.MM_SUMM,
                        "OFF."+DBHelper.MM_DATE_TAKE,
                        "OFF."+DBHelper.MM_DATE_RETURN,
                        "OFF."+DBHelper.MM_ID_PARSE};

                String[] selectionArgsLS = arg.getStringArray(SELECT_ARG);
                cursor = dbHelper.getReadableDatabase().query(tableLS, null, null, null, null, null, null);
                break;

            case JOIN_QUERY_ARG_N:
                table = "deptor as DT inner join many_metter as OFF on OFF.id_debtor = DT._id";
                String columnsN[] = { "DT."+DBHelper.D_NAME,
                        "DT."+DBHelper.D_SURNAME,
                        "DT."+DBHelper.D_EMAIL,
                        "DT."+DBHelper.D_PHONE,
                        "DT."+DBHelper.D_PATH,
                        "OFF."+DBHelper.MM_PATH_PHOTO_MATTER,
                        "OFF."+DBHelper.MM_SUMM,
                        "OFF."+DBHelper.MM_DATE_TAKE,
                        "OFF."+DBHelper.MM_DATE_RETURN,
                        "OFF."+DBHelper.MM_ID_PARSE_DATA,
                        "OFF."+DBHelper.MM_ID};
                selection = arg.getString(SELECT_COLUMN)+" = ?";
                String[] selectionArgsN = arg.getStringArray(SELECT_ARG);
                cursor = dbHelper.getReadableDatabase().query(table, columnsN, selection, selectionArgsN, null, null, null);
                break;

        }
        return cursor;
    }
}