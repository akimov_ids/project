package com.example.debtor_task.app.parse_object;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Nikolay on 02.06.14.
 */
public class DeptorParse {

    @Expose
    @SerializedName("username")
    public String username;

    @Expose
    @SerializedName("surname")
    public String surname;

    @Expose
    @SerializedName("phone")
    public String phone;

    @Expose
    @SerializedName("email")
    public String email;

    @Expose
    @SerializedName("path_photo")
    public String path_photo;

    @Expose
    @SerializedName("id_parse_user")
    public String id_parse_user;

    public void setUsername(String username) {
        this.username = username;
    }
    public void setSurname(String surname) {this.surname = surname;}
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPathPhoto(String path_photo) {
        this.path_photo = path_photo;
    }

    public void setIdParseUser(String id_parse_user) {
        this.id_parse_user = id_parse_user;
    }
}
