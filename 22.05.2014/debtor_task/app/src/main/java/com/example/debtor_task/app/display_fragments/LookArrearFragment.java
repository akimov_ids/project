package com.example.debtor_task.app.display_fragments;

//import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.DebtorsAppMenu;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.framework_code.ProcessingDate;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.database_processing.AsyncTaskWriter;
import com.example.debtor_task.app.display_fragments.AddDebtorFragment;
import com.example.debtor_task.app.framework_code.LookDataFragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Николай on 29.05.14.
 */
public class LookArrearFragment extends LookDataFragment {

    final String LOG_TAG = "myLogs";

    public final static String KEY_ID_MM = "key_id_mm";
    public final static String KEY_ID_DEBTOR = "KEY_ID";
    public final static String KEY_ID_PARSE_DATA_MM = "KEY_ID_PARSE_DATA_MM";


    private TextView textViewName;
    private TextView textViewDateTake;
    private TextView textViewDateReturn;
    ImageView imageView;
    ImageView imageViewMetter;

    FragmentTransaction fTrans;
    TextView textViewSumm;
    String idParseData;
    DebtorsAppMenu debtorsAppMenu;
    boolean isHavePhoto;

    String pathPhotoMetter;
    String takeDate;
    String returnDate;
    String summ;
    long unixTakeDate;
    long unixReturnDate;
    long id_debtor;
    Bundle args;
    ProcessingDate processingData;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actionBar = ((ActionBarActivity)activity).getSupportActionBar();
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debtorsAppMenu = new DebtorsAppMenu(getActivity().getSupportFragmentManager());
        processingData = new ProcessingDate();
        args = new Bundle();
    }

    @Override
    public void takeData(Cursor data) {

        if (data != null && data.getCount()>0) {
            data.moveToFirst();
            unixTakeDate = Long.parseLong(data.getString(data.getColumnIndex(DBHelper.MM_DATE_TAKE)));
            unixReturnDate = Long.parseLong(data.getString(data.getColumnIndex(DBHelper.MM_DATE_RETURN)));
            id_debtor = Long.parseLong(data.getString(data.getColumnIndex(DBHelper.MM_ID_DEBTOR)));

            takeDate = processingData.getStringData(unixTakeDate);
            returnDate = processingData.getStringData(unixReturnDate);

            textViewName.setText(getString(R.string.debtor)+data.getString(data.getColumnIndex(DBHelper.D_NAME)));
            textViewDateTake.setText(getString(R.string.take_date)+takeDate);
            textViewDateReturn.setText(getString(R.string.return_date)+returnDate);
            idParseData = data.getString(data.getColumnIndex(DBHelper.MM_ID_PARSE_DATA));

            if (data.getString(data.getColumnIndex(DBHelper.MM_SUMM)) == null ||
                    data.getString(data.getColumnIndex(DBHelper.MM_SUMM)).equals("")) {

                isHavePhoto = true;
                if (data.getString(data.getColumnIndex(DBHelper.MM_PATH_PHOTO_MATTER)) == null ||
                        data.getString(data.getColumnIndex(DBHelper.MM_PATH_PHOTO_MATTER)).equals("")) {
                    InputStream istr;
                    Bitmap bitmap = null;
                    try {
                        istr = getActivity().getAssets().open(MainActivity.NO_PHOTO_FILE);
                        bitmap = BitmapFactory.decodeStream(istr);
                        imageViewMetter.setImageBitmap(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    textViewSumm.setVisibility(View.GONE);
                    pathPhotoMetter = data.getString(data.getColumnIndex(DBHelper.MM_PATH_PHOTO_MATTER));
                    imageViewMetter.setImageBitmap(MainActivity.
                            getCultivatedBitmap(pathPhotoMetter));
                }
            } else {
                summ = data.getString(data.getColumnIndex(DBHelper.MM_SUMM));
                imageViewMetter.setVisibility(View.GONE);
                textViewSumm.setText(getString(R.string.summ)+summ);
                isHavePhoto = false;
            }
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.look_arrears_fragment, container, false);

        imageView = (ImageView) v.findViewById(R.id.imageView);

        textViewSumm = (TextView) v.findViewById(R.id.textView5);
        imageViewMetter = (ImageView)v.findViewById(R.id.imageView2);
        textViewName = (TextView) v.findViewById(R.id.textView);

        textViewDateTake = (TextView) v.findViewById(R.id.textView6);
        textViewDateReturn = (TextView) v.findViewById(R.id.textView7);
        actionBar.setTitle(getString(R.string.arrear));

        return v;
    }

    public void onResume() {
        super.onResume();
        args.clear();
        args.putString(LookDataFragment.SELECTION, DBHelper.MM_ID);
        args.putStringArray(LookDataFragment.SELECT_ARG,new String[]{""+getArguments().getLong(KEY_ID_MM)});
        args.putStringArray(LookDataFragment.SELECT_COLUMN,new String[]{DBHelper.MM_SUMM,
                DBHelper.MM_DATE_RETURN,
                DBHelper.MM_DATE_TAKE,
                DBHelper.MM_NAMING,
                DBHelper.MM_PATH_PHOTO_MATTER,
                DBHelper.D_NAME,
                DBHelper.D_SURNAME,
                DBHelper.D_EMAIL,
                DBHelper.D_PHONE,
                DBHelper.D_PATH,
                DBHelper.MM_ID_DEBTOR,
                DBHelper.MM_ID_PARSE_DATA});

        getData(args, DBHelper.TABLE_MANY_METTER, LookDataFragment.JOIN_QUERY_ARG);
    }

    @Override
    protected void onPostLoadData(){
        super.onPostLoadData();
        getFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.look_item_arrears, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.look_arrears_delete) {
            deleteData(getArguments().getLong(KEY_ID_MM),DBHelper.TABLE_MANY_METTER,idParseData);
            return true;
        } else if(id == R.id.look_arrears_redact){

            Bundle args = new Bundle();
            args.putLong(KEY_ID_MM, getArguments().getLong(KEY_ID_MM));

            args.putInt(AddDebtorFragment.KEY_OPERRATIONS, AddDebtorFragment.OPERRATION_REDACT);
            args.putLong(AddDebtorFragment.KEY_DATA_TAKE, unixTakeDate);
            args.putLong(AddDebtorFragment.KEY_DATA_RETURN, unixReturnDate);
            args.putLong(KEY_ID_DEBTOR, id_debtor);

            if(isHavePhoto)
                args.putString(AddDebtorFragment.KEY_PATH_PHOTO_METTER, pathPhotoMetter);
            else
                args.putString(AddDebtorFragment.KEY_SUMM, summ);

            debtorsAppMenu.goToItem(DebtorsAppMenu.ADD_ARREAR,args);
        }
        return super.onOptionsItemSelected(item);
    }
}
