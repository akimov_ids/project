package com.example.debtor_task.app;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Nikolay on 07.05.14.
 */
public class DBHelper extends SQLiteOpenHelper {

    public final static  String DB_NAME = "mydb";
    public final static  String TABLE_DEPTOR = "deptor";
    public final static  String TABLE_MANY_METTER = "many_metter";

    public final static  String D_ID = "_id";
    public final static  String D_NAME = "name";
    public final static  String D_EMAIL = "email";
    public final static  String D_SURNAME = "surname";
    public final static  String D_PHONE = "phone";
    public final static  String D_MANY_MATTER_ID = "many_metter_id";
    public final static  String D_ID_PARSE = "id_parse";
    public final static  String D_PATH = "path_photo";
    public final static  String D_ID_PARSE_DATA = "id_parse_data";

    public final static  String MM_NAMING = "naming";
    public final static  String MM_DATE_TAKE = "date_take";
    public final static  String MM_DATE_RETURN = "date_return";
    public final static  String MM_SUMM = "summ";
    public final static  String MM_ID_PARSE = "id_parse";
    public final static  String MM_ID_PARSE_DATA = "id_parse_data";
    public final static  String MM_ID = "_id";
    public final static  String MM_PATH_PHOTO_MATTER = "path_photo_matter";
    public final static  String MM_ID_DEBTOR = "id_debtor";

    private static final String DB_CREATE_DEBTOR = "create table if not exists deptor(_id integer primary key autoincrement, " +
                                                                                      "name text, " +
                                                                                      "path_photo text, " +
                                                                                      "surname text, " +
                                                                                      "phone text, " +
                                                                                      "email text, " +
                                                                                      "id_parse text," +
                                                                                      "id_parse_data text);";



    private static final String DB_CREATE_MANY_MATTER = "create table if not exists many_metter(_id integer primary key autoincrement, " +
            "                                                                                   date_take text, " +
            "                                                                                   date_return text, " +
            "                                                                                   summ text," +
            "                                                                                   id_debtor text," +
            "                                                                                   path_photo_matter text," +
            "                                                                                   id_parse text," +
            "                                                                                   id_parse_data,"+
            "                                          FOREIGN KEY (id_debtor) references deptor(_id) ON DELETE CASCADE);";


    public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_DEBTOR);
            db.execSQL(DB_CREATE_MANY_MATTER);
    }

    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }



}
