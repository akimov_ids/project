package com.example.debtor_task.app.display_fragments;


import android.app.Activity;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;

import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.DebtorsAppMenu;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.framework_code.FragmentManageDataList;
import com.example.debtor_task.app.framework_code.ListInitializer;
import com.example.debtor_task.app.services.LoadToParseService;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.io.File;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;


/**
 * Created by Nikolay on 22.05.14.
 */
public class ListDebtorFragment  extends FragmentManageDataList {

    ImageView imageView;
    ListView listView;
    private Uri uriContact;
    private String contactID;
    DisplayImageOptions options;
    DebtorsAppMenu debtorsAppMenu;
    Bundle args;

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        args = new Bundle();
        args.putString(MyCursorLoader.SELECTION, DBHelper.D_ID_PARSE);
        args.putStringArray(MyCursorLoader.SELECT_ARG,new String[]{LoginUserFragment.LOGINING_ID});
        args.putStringArray(MyCursorLoader.SELECT_COLUMN, new String[]{
                DBHelper.D_NAME,
                DBHelper.D_PHONE,
                DBHelper.D_EMAIL,
                DBHelper.D_SURNAME,
                DBHelper.D_PATH,
                DBHelper.D_ID,
        });
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_deptor_fragment, container, false);
        options = new DisplayImageOptions.Builder().showImageOnFail(R.drawable.no_photo).build();

        debtorsAppMenu = new DebtorsAppMenu(getActivity().getSupportFragmentManager());

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.edit().putString(MainActivity.LOGINED_USER_ID, LoginUserFragment.LOGINING_ID).commit();

        setHasOptionsMenu(true);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        listView = (ListView) v.findViewById(R.id.listView);

        return v;
    }

    public void onResume() {
        super.onResume();

        setInitializer(new ListInitializerDebtor());

        completeList(listView,args,DBHelper.TABLE_DEPTOR,MyCursorLoader.QUERY_ARG);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                args.putLong(LookDebtorFragment.KEY_ID_DEPTOR, id);
                debtorsAppMenu.goToItem(DebtorsAppMenu.LOOK_DEBTOR,args);
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String[] nameAnSurname;

        if(resultCode == Activity.RESULT_OK){
            if(requestCode == MainActivity.REQUEST_ACTION_PICK){
                uriContact  = data.getData();
                ContentValues cv = new ContentValues();

                Bundle args = new Bundle();
                args.putString(LoadToParseService.LOAD_PARSE_PHONE_DEBTOR, retrieveContactNumber());
                args.putString(LoadToParseService.LOAD_PARSE_EMAIL_DEBTOR, retrieveContactEmail());
                nameAnSurname = retrieveContactName();
                args.putString(LoadToParseService.LOAD_PARSE_NAME_DEBTOR,nameAnSurname[0]);
                args.putString(LoadToParseService.LOAD_PARSE_SURNAME_DEBTOR, nameAnSurname[1]);
                args.putString(LoadToParseService.LOAD_PARSE_ID_PARSE_USER, LoginUserFragment.LOGINING_ID);
                args.putInt(LoadToParseService.KEY_SERVICE_TASK, LoadToParseService.SERVICE_TASK_LOAD_DEBTOR);
                String pathPhoto = retrieveContactPhoto();
                args.putBoolean(LoadToParseService.LOAD_PARSE_IS_HAVE_PHOTO, (pathPhoto!=null));
                args.putString(LoadToParseService.LOAD_PARSE_PATH_PHOTO_DEBTOR, pathPhoto);

                cv.put(DBHelper.D_NAME,nameAnSurname[0]);
                cv.put(DBHelper.D_SURNAME,nameAnSurname[1]);
                cv.put(DBHelper.D_EMAIL,retrieveContactEmail());
                cv.put(DBHelper.D_PHONE,retrieveContactNumber());
                cv.put(DBHelper.D_ID_PARSE, LoginUserFragment.LOGINING_ID);
                cv.put(DBHelper.D_PATH, pathPhoto);

                addData(cv, DBHelper.TABLE_DEPTOR, args);
            }
        }
    }

    @Override
    protected void onPostLoadData() {
     super.onPostLoadData();
     recompleteList(listView, args, DBHelper.TABLE_DEPTOR, MyCursorLoader.QUERY_ARG);
    }

    private String retrieveContactPhoto() {

        Bitmap photo = null;
        String pathPhoto = null;
        String namePhoto;

            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(getActivity().getContentResolver(),
                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, new Long(contactID)));

            if (inputStream != null) {
                photo = BitmapFactory.decodeStream(inputStream);
                OutputStream outputStream = null;

                    namePhoto = new Date().getTime()+".jpg";
                    pathPhoto = Environment.getExternalStorageDirectory()+"/"+MainActivity.DIR_NAME+"/"+namePhoto;

                    try{
                        outputStream =
                                new FileOutputStream(new File(pathPhoto));
                        photo.compress(Bitmap.CompressFormat.JPEG, 100, outputStream);

                    }catch(FileNotFoundException ex){ex.printStackTrace();}
            }
        return pathPhoto;
    }

    private String retrieveContactNumber() {

        String contactNumber = null;
        Cursor cursorID = getActivity().getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();
        Cursor cursorPhone = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,new String[]{contactID},
                            null);

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();
        return contactNumber;
    }

    private String retrieveContactEmail() {
        String email = null;

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{contactID}, null);
                while (cur1.moveToNext()) {
                    email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                }
                cur1.close();
            }
        }
        return email;
    }


    private String[] retrieveContactName() {

        String contactName = null;
        String contactSecondName = null;
        ContentResolver contentResolver = getActivity().getContentResolver();

        String structuredNameWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
        String[] structuredNameWhereParams = new String[]{contactID, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE};
        Cursor cursor = contentResolver.query(ContactsContract.Data.CONTENT_URI, null, structuredNameWhere, structuredNameWhereParams, null);

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
            contactSecondName  = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
        }

        cursor.close();
        return new String[]{contactName,contactSecondName};
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.to_add_dabtor_from_contacts:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                getActivity().getSupportLoaderManager().destroyLoader(0);
                startActivityForResult(intent, MainActivity.REQUEST_ACTION_PICK);
                return true;

            case R.id.to_add:
                Bundle args = new Bundle();
                args.putLong(AddDebtorFragment.KEY_ID,AddDebtorFragment.NO_ID);
                args.putInt(AddDebtorFragment.KEY_OPERRATIONS, AddDebtorFragment.OPERRATION_ADD);
                debtorsAppMenu.goToItem(DebtorsAppMenu.ADD_DEBTOR,args);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.list_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    class ListInitializerDebtor implements ListInitializer {
        TextView textViewName;
        TextView textViewTakeDate;
        TextView textViewReturnDate;
        ImageView img;

        @Override
        public void initialize(Cursor cursor) {
            textViewName.setText(cursor.getString(cursor.getColumnIndex(DBHelper.D_NAME)));
            textViewTakeDate.setText(cursor.getString(cursor.getColumnIndex(DBHelper.D_SURNAME)));
            textViewReturnDate.setText(cursor.getString(cursor.getColumnIndex(DBHelper.D_EMAIL)));
        }

        @Override
        public void setView(View view) {
            img = (ImageView)view.findViewById(R.id.image);
            textViewName = (TextView)view.findViewById(R.id.text);
            textViewTakeDate = (TextView)view.findViewById(R.id.textView);
            textViewReturnDate = (TextView)view.findViewById(R.id.textView2);
        }

        @Override
        public TextView getTextView() {
            return textViewName;
        }

        @Override
        public ImageView getImageView() {
            return img;
        }

        @Override
        public String getColumn() {
            return DBHelper.D_PATH;
        }

    }
}
