package com.example.debtor_task.app.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.display_fragments.LoginUserFragment;
import com.example.debtor_task.app.display_fragments.LookArrearFragment;
import com.example.debtor_task.app.parse_object.ArrearsParse;
import com.example.debtor_task.app.parse_object.DeptorParse;
import com.example.debtor_task.app.framework_code.FragmentManageData;
import com.example.debtor_task.app.framework_code.ManageWeb;
import com.example.debtor_task.app.framework_code.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Николай on 17.05.14.
 */
public class LoadToParseService extends Service {

    final String LOG_TAG = "myLogs";

    public final static String LOAD_PARSE_NAME_DEBTOR = "LOAD_PARSE_NAME_DEBTOR";
    public final static String LOAD_PARSE_SURNAME_DEBTOR = "LOAD_PARSE_SURNAME_DEBTOR";
    public final static String LOAD_PARSE_EMAIL_DEBTOR = "LOAD_PARSE_EMAIL_DEBTOR";
    public final static String LOAD_PARSE_PHONE_DEBTOR = "LOAD_PARSE_PHONE_DEBTOR";

    public final static String LOAD_PARSE_SUMM = "LOAD_PARSE_SUMM";
    public final static String LOAD_PARSE_TAKE_DATE = "LOAD_PARSE_TAKE_DATE";
    public final static String LOAD_PARSE_RETURN_DATE = "LOAD_PARSE_RETURN_DATE";
    public final static String LOAD_PARSE_MONEY_FLAG = "LOAD_PARSE_MONEY_FLAG";
    public final static String LOAD_PARSE_ID_ARREARS_PARSE = "LOAD_PARSE_ID_ARREARS_PARSE";
    public final static String LOAD_PARSE_SELECT_ID_PARSE_DEBTOR = "LOAD_PARSE_SELECT_ID_PARSE_DEBTOR";
    public final static String LOAD_PARSE_PATH_PHOTO_METTER = "LOAD_PARSE_PATH_PHOTO_METTER";
    public final static String LOAD_PARSE_IS_HAVE_PHOTO = "LOAD_PARSE_IS_HAVE_PHOTO";
    public final static String LOAD_PARSE_PATH_PHOTO_DEBTOR = "LOAD_PARSE_PATH_PHOTO_DEBTOR";
    public final static String LOAD_PARSE_ID_PARSE_USER = "LOAD_PARSE_ID_PARSE_USER";
    public final static String LOAD_PARSE_OBJECTID = "LOAD_PARSE_OBJECTID";
    public final static String LOAD_PARSE_CLASS_NAME = "LOAD_PARSE_CLASS_NAME";
    public final static String KEY_SERVICE_TASK = "KEY_SERVICE_TASK";

    public final static String T_CONST_DABTOR_CNAME = "debtors";
    public final static String T_CONST_DABTOR_ARREARS = "arrears";

    public final static int SERVICE_TASK_LOAD_DEBTOR = 0;
    public final static int SERVICE_TASK_LOAD_ARREAR = 1;
    public final static int SERVICE_TASK_DELETE = 2;
    public final static int SERVICE_TASK_UPDATE_DEBTOR = 3;

    ThreadLoanToParse threadLoadToParse;
    String parseDeptorId;
    ExecutorService es;
    ManageWeb manageWeb;

    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        manageWeb = new ManageWeb(Constants.PARSE_ID,Constants.PARSE_KEY);

        threadLoadToParse = new ThreadLoanToParse(intent);
        es = Executors.newFixedThreadPool(1);
        es.execute(threadLoadToParse);
        return Service.START_NOT_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        es.shutdown();
        stopSelf();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    class ThreadLoanToParse implements Runnable {

        String parseArrearsId;
        int KEY;
        boolean isHavePhoto = false;
        Intent intent;

        ThreadLoanToParse(Intent intent) {
            KEY = intent.getIntExtra(KEY_SERVICE_TASK,-1);
            this.intent = intent;
        }


        @Override
        public void run() {
            switch (KEY) {
                case SERVICE_TASK_LOAD_DEBTOR:
                    loadDebtor(intent);
                    break;
                case SERVICE_TASK_LOAD_ARREAR:
                    loadArrear(intent);
                    break;
                case SERVICE_TASK_DELETE:
                    delete(intent);
                    break;
            }
        }

        public void loadDebtor(Intent intent) {
            String pathLF;

            isHavePhoto = intent.getBooleanExtra(LOAD_PARSE_IS_HAVE_PHOTO, false);

            DeptorParse deptorParse = new DeptorParse();
            deptorParse.setUsername(intent.getStringExtra(LOAD_PARSE_NAME_DEBTOR));
            deptorParse.setSurname(intent.getStringExtra(LOAD_PARSE_SURNAME_DEBTOR));
            deptorParse.setEmail(intent.getStringExtra(LOAD_PARSE_EMAIL_DEBTOR));
            deptorParse.setPhone(intent.getStringExtra(LOAD_PARSE_PHONE_DEBTOR));
            deptorParse.setIdParseUser(LoginUserFragment.LOGINING_ID);

            if (isHavePhoto) {
                pathLF = manageWeb.uploadFile(intent.getStringExtra(LOAD_PARSE_PATH_PHOTO_DEBTOR));
                deptorParse.setPathPhoto(pathLF);
            }


            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            parseDeptorId = manageWeb.uploadObject(gson.toJson(deptorParse), T_CONST_DABTOR_CNAME);

            intent = new Intent(FragmentManageData.MANAGER_DATA_BROADCASST);
            intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                    FragmentManageData.MANAGER_DATA_BROADCASST);
            intent.putExtra(LOAD_PARSE_SELECT_ID_PARSE_DEBTOR,parseDeptorId);
            sendBroadcast(intent);
        }

        public void loadArrear(Intent intent) {

            isHavePhoto = intent.getBooleanExtra(LOAD_PARSE_IS_HAVE_PHOTO, false);

            ArrearsParse arrearsParse = new ArrearsParse();
            arrearsParse.setDateReturn(intent.getStringExtra(LoadToParseService.LOAD_PARSE_RETURN_DATE));
            arrearsParse.setDateTake(intent.getStringExtra(LoadToParseService.LOAD_PARSE_TAKE_DATE));
            arrearsParse.setIdParseUser(LoginUserFragment.LOGINING_ID);

            arrearsParse.setIdParseDeptor(intent.getStringExtra(LoadToParseService.LOAD_PARSE_SELECT_ID_PARSE_DEBTOR));

            if (isHavePhoto) {
                arrearsParse.setPathPhotoMetter(manageWeb
                        .uploadFile(intent
                                .getStringExtra(LoadToParseService.LOAD_PARSE_PATH_PHOTO_METTER)));
            } else {
                arrearsParse.setSumm(intent.getStringExtra(LoadToParseService.LOAD_PARSE_SUMM));
            }


            Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
            parseArrearsId = manageWeb.uploadObject(gson.toJson(arrearsParse), T_CONST_DABTOR_ARREARS);
            //manageWeb.updateObject(gson.toJson(arrearsParse), T_CONST_DABTOR_ARREARS,"");
            intent = new Intent(FragmentManageData.MANAGER_DATA_BROADCASST);
            intent.putExtra(MainActivity.KEY_TASK_BROADCASST,MainActivity.BORADCAST_LOAD_DATA_ARREAR)
                  .putExtra(LoadToParseService.LOAD_PARSE_SELECT_ID_PARSE_DEBTOR,parseArrearsId);

            sendBroadcast(intent);
        }

        public void delete(Intent intent) {
            manageWeb.deleteData(intent.getStringExtra(LoadToParseService.LOAD_PARSE_CLASS_NAME),
                                    intent.getStringExtra(LoadToParseService.LOAD_PARSE_OBJECTID));
            intent = new Intent(FragmentManageData.MANAGER_DATA_BROADCASST);
            sendBroadcast(intent);
        }

        public void stop(){
            stopSelf();
        }

    }

}