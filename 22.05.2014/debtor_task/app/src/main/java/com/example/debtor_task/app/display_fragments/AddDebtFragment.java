package com.example.debtor_task.app.display_fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.CalendarContract;
import android.provider.MediaStore;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.MyDatePickerDialog;
import com.example.debtor_task.app.framework_code.Dialoger;
import com.example.debtor_task.app.framework_code.ManageFiles;
import com.example.debtor_task.app.framework_code.ManageImage;
import com.example.debtor_task.app.framework_code.ManageWeb;
import com.example.debtor_task.app.framework_code.ProcessingDate;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.framework_code.FragmentManageData;
import com.example.debtor_task.app.services.LoadToParseService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Николай on 28.05.14.
 */
public class AddDebtFragment extends FragmentManageData implements LoaderManager.LoaderCallbacks<Cursor>, DialogInterface.OnClickListener{

    TextView textView;
    Spinner spinnerDebtors;

    Button butAddPhotMatter;
    Button butSelectMetter;
    Button butAddData;
    Button butAddReturnDate;

    EditText editTextName;
    EditText editTextSurname;
    EditText editTextEmail;
    EditText editTextPhone;

    EditText editText;
    boolean moneyFlag = true;
    String parseArrearsId;
    String SELECT_ID_PARSE_DEBTOR;
    RadioGroup radiogroup;

    ContentValues cvManyMatter;
    SimpleCursorAdapter scAdapterOff;
    long unixDateTake;
    long unixDateReturn;
    long unixDateNow;

    Calendar calendar;
    ProcessingDate processingData;
    ActionBar actionBar;
    InputMethodManager imm;
    Dialoger dialoger;

    String[] fromOff = new String[] {DBHelper.D_SURNAME, DBHelper.D_NAME};
    int[] to = new int[] { R.id.image, R.id.text};
    int myYear = 2011;
    int myMonth = 02;
    int myDay = 03;

    String takeDate;
    String returnDate;
    IntentFilter intentFilter;
    long SELECT_ID_DEBTOR = -1;
    private ManageFiles manageFiles;
    private ManageWeb manageWeb;
    private ManageImage manageImage;

    //public static final String KEY_ID = "KEY_ID";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        actionBar = ((ActionBarActivity)activity).getSupportActionBar();

        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getString(R.string.add_new_arrears));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        dialoger = new Dialoger(getActivity());
        manageWeb = new ManageWeb();
        manageImage = ManageImage.getInstance();
        manageImage.setResources(getResources());

        //==
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());

        manageFiles = ManageFiles.getInstance();

        myDay = calendar.get(Calendar.DAY_OF_MONTH);
        myMonth = calendar.get(Calendar.MONTH);
        myYear = calendar.get(Calendar.YEAR);

        calendar.set(myYear,myMonth,myDay);
        unixDateNow = calendar.getTimeInMillis();
        unixDateTake = unixDateNow;
        unixDateReturn = unixDateNow;
        //==


        scAdapterOff = new SimpleCursorAdapter(getActivity(), R.layout.item, null, fromOff, to, 0);
        getActivity().getSupportLoaderManager().initLoader(8888, null, this);

        processingData = new ProcessingDate();
        intentFilter = new IntentFilter(MainActivity.ACTION_BROADCASST);
    }

    void calendarInitializer(long unixDate, TextView view) {
        calendar.clear();
        calendar.setTimeInMillis(unixDate);
        myCallBack.setView(view);

        DatePickerDialog tpd = new DatePickerDialog(getActivity(), myCallBack, calendar.get(Calendar.YEAR),
                                                                               calendar.get(Calendar.MONTH),
                                                                               calendar.get(Calendar.DAY_OF_MONTH));
        tpd.show();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_debt_fragment, container, false);

        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        radiogroup = (RadioGroup) v.findViewById(R.id.radioGroup);
        radiogroup.check(R.id.radioButton2);
        editText = (EditText) v.findViewById(R.id.editText);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        textView = new TextView(getActivity());
        textView.setText(editText.getText().toString());

        spinnerDebtors = (Spinner) v.findViewById(R.id.spinner);
        calendar = Calendar.getInstance();

        butAddData = (Button) v.findViewById(R.id.button);
        butAddData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarInitializer(unixDateTake,butAddData);
            }
        });

        butAddReturnDate = (Button) v.findViewById(R.id.button4);
        butAddReturnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calendarInitializer(unixDateReturn,butAddReturnDate);
                butAddReturnDate.setText(ProcessingDate.getStringData(unixDateReturn));
            }
        });

        cvManyMatter = new ContentValues();
        setHasOptionsMenu(true);

        editTextName = (EditText) v.findViewById(R.id.editText);
        editTextSurname = (EditText) v.findViewById(R.id.editText2);
        editTextEmail = (EditText) v.findViewById(R.id.editText3);
        editTextPhone = (EditText) v.findViewById(R.id.editText4);

        butSelectMetter = (Button)v.findViewById(R.id.button3);
        butSelectMetter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPhoto();
            }
        });

        butAddPhotMatter = (Button)v.findViewById(R.id.button2);
        butAddPhotMatter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isHavePhoto)
                    manageFiles.deleteFile(pathPhoto);
                selectPhoto();
            }
        });

        activeMoneyMode();
        imageView.setImageBitmap(manageImage.getFailPhoto(R.drawable.no_photo));

        radiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton:
                        activeMatterMode();
                        break;
                    case R.id.radioButton2:
                        activeMoneyMode();
                        break;
                }
            }
        });

        if(getArguments().getInt(AddDebtorFragment.KEY_OPERRATIONS) == AddDebtorFragment.OPERRATION_REDACT)
            setRedactData();
        return v;
    }

    @Override
    protected void onPostLoadData(){
        super.onPostLoadData();
        Dialog dl= onCreateDialog(0);
        dl.show();
    }

    public void setRedactData(){
        unixDateTake = getArguments().getLong(AddDebtorFragment.KEY_DATA_TAKE);
        unixDateReturn = getArguments().getLong(AddDebtorFragment.KEY_DATA_RETURN);

        takeDate = processingData.getStringData(unixDateTake);
        returnDate = processingData.getStringData(unixDateReturn);

        butAddData.setText(takeDate);
        butAddReturnDate.setText(returnDate);
        if(getArguments().getString(AddDebtorFragment.KEY_PATH_PHOTO_METTER)!=null){
            radiogroup.check(R.id.radioButton);
            imageView.setImageBitmap(MainActivity.
                    getCultivatedBitmap(getArguments().getString(AddDebtorFragment.KEY_PATH_PHOTO_METTER)));
        }else{
            editText.setText(getArguments().getString(AddDebtorFragment.KEY_SUMM));
        }
    }

    private void activeMoneyMode(){
        butAddPhotMatter.setVisibility(View.GONE);
        butSelectMetter.setVisibility(View.GONE);
        imageView.setVisibility(View.GONE);
        editText.setVisibility(View.VISIBLE);
        moneyFlag = true;
    }

    private void activeMatterMode(){
        butAddPhotMatter.setVisibility(View.VISIBLE);
        butSelectMetter.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        editText.setVisibility(View.GONE);
        moneyFlag = false;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if(which == Dialog.BUTTON_POSITIVE) {
            Intent intent = new Intent(Intent.ACTION_EDIT);
            intent.setType("vnd.android.cursor.item/event");
            intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, unixDateReturn+60*60*24*1000);
            intent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,unixDateReturn+60*60*24*1000);
            intent.putExtra(CalendarContract.Events.TITLE, getString(R.string.debtors_must_return_debt));
            startActivity(intent);
        }
        getActivity().getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.add_arrears_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add_arrears_ok:
                if(manageWeb.hasConnection(getActivity())) {

                    imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
                    if(isCorrectData()){
                        ContentValues cv = new ContentValues();
                        Bundle args = new Bundle();

                        args.putString(LoadToParseService.LOAD_PARSE_SUMM, editText.getText().toString());
                        args.putString(LoadToParseService.LOAD_PARSE_TAKE_DATE, ""+unixDateTake);
                        args.putString(LoadToParseService.LOAD_PARSE_RETURN_DATE, ""+unixDateReturn);
                        args.putBoolean(LoadToParseService.LOAD_PARSE_MONEY_FLAG, moneyFlag);
                        args.putString(LoadToParseService.LOAD_PARSE_ID_ARREARS_PARSE, parseArrearsId);
                        args.putString(LoadToParseService.LOAD_PARSE_SELECT_ID_PARSE_DEBTOR, SELECT_ID_PARSE_DEBTOR);
                        args.putString(LoadToParseService.LOAD_PARSE_PATH_PHOTO_METTER, pathPhoto);
                        args.putInt(LoadToParseService.KEY_SERVICE_TASK, LoadToParseService.SERVICE_TASK_LOAD_ARREAR);
                        args.putBoolean(LoadToParseService.LOAD_PARSE_IS_HAVE_PHOTO, isHavePhoto);

                        cv.put(DBHelper.MM_DATE_RETURN, unixDateReturn);
                        cv.put(DBHelper.MM_DATE_TAKE, unixDateTake);

                        cv.put(DBHelper.MM_ID_PARSE, LoginUserFragment.LOGINING_ID);
                        cv.put(DBHelper.MM_ID_DEBTOR, SELECT_ID_DEBTOR);

                        if(moneyFlag){
                            cv.put(DBHelper.MM_PATH_PHOTO_MATTER, "");
                            cv.put(DBHelper.MM_SUMM, editText.getText().toString());
                        }else{
                            cv.put(DBHelper.MM_PATH_PHOTO_MATTER, pathPhoto);
                            cv.put(DBHelper.MM_SUMM, "");
                        }

                        if(getArguments().getInt(AddDebtorFragment.KEY_OPERRATIONS) == AddDebtorFragment.OPERRATION_REDACT)
                            updateData(cv,""+getArguments()
                                    .getLong(LookArrearFragment.KEY_ID_MM),DBHelper.TABLE_MANY_METTER,args);
                        else
                            addData(cv,DBHelper.TABLE_MANY_METTER,args);
                    }
        }else{
                    dialoger.alertDialor(getString(R.string.no_internet_connection));
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public boolean isCorrectData(){
        if(takeDate == null||
           returnDate == null){
            dialoger.alertDialor(getString(R.string.enter_all_values));
        }else if(unixDateReturn - unixDateTake < 0||
                unixDateReturn < unixDateNow ||
                unixDateTake > unixDateNow) {
                    dialoger.alertDialor(getString(R.string.enter_correct_data));
        } else if(moneyFlag){
            if(editText.getText().toString().equals("")) {
                dialoger.alertDialor(getString(R.string.enter_all_values));
            } else {
                return true;
            }
        } else {
            return true;
        }
        return false;
    }

    public void onDestroyView() {
        super.onDestroyView();
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }

    public void onDestroy() {
        super.onDestroy();
        getActivity().getSupportLoaderManager().destroyLoader(8888);
    }

    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        adb.setTitle(getString(R.string.recall));
        adb.setMessage(getString(R.string.want_include_reminder));
        adb.setIcon(android.R.drawable.ic_dialog_info);
        adb.setPositiveButton(getString(R.string.yes), this);
        adb.setNegativeButton(getString(R.string.no), this);
        return adb.create();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        Bundle arg = new Bundle();
        arg.putStringArray(MyCursorLoader.SELECT_COLUMN, new String[]{
                DBHelper.D_NAME,
                DBHelper.D_PHONE,
                DBHelper.D_EMAIL,
                DBHelper.D_SURNAME,
                DBHelper.D_PATH,
                DBHelper.D_ID,
                DBHelper.D_ID_PARSE_DATA
        });

        arg.putString(MyCursorLoader.SELECTION,DBHelper.D_ID_PARSE);
        arg.putStringArray(MyCursorLoader.SELECT_ARG,new String[]{LoginUserFragment.LOGINING_ID});

        return new MyCursorLoader(getActivity(), DBHelper.TABLE_DEPTOR,arg
                ,MyCursorLoader.QUERY_ARG);
    }

    HashMap<Long,String> mapId = new HashMap<Long, String>();
    @Override
    public void onLoadFinished(Loader<Cursor> loader,Cursor data) {

        scAdapterOff.swapCursor(data);
        spinnerDebtors.setAdapter(scAdapterOff);

        if(data.getCount()>0){
            do{
               mapId.put(data.getLong(data.getColumnIndex(DBHelper.D_ID)),
                        data.getString(data.getColumnIndex(DBHelper.D_ID_PARSE_DATA)));

                if(data.getLong(data.getColumnIndex(DBHelper.D_ID)) == getArguments().getLong(LookArrearFragment.KEY_ID_DEBTOR))
                        spinnerDebtors.setSelection(data.getPosition());
            }while (data.moveToNext());


            spinnerDebtors.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view,
                                           int position, long id) {
                    SELECT_ID_DEBTOR = id;
                    SELECT_ID_PARSE_DEBTOR = mapId.get(id);
                }
                @Override
                public void onNothingSelected(AdapterView<?> arg0) {}
            });
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {}

    MyDatePickerDialog myCallBack = new MyDatePickerDialog() {
        TextView view;
        long unixDate;
        @Override
        public void setView(TextView view) {
            this.view = view;
        }

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.clear();
            calendar.set(year,monthOfYear,dayOfMonth);
            unixDate = calendar.getTimeInMillis();
            takeDate = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
            if (this.view != null) {
                this.view.setText(takeDate);
            }
        }
    };

}

