package com.example.debtor_task.app.display_fragments;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.MainActivity;
//import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.database_processing.AsyncTaskWriter;
import com.example.debtor_task.app.framework_code.Dialoger;
import com.example.debtor_task.app.framework_code.FragmentManageData;
import com.example.debtor_task.app.framework_code.ManageFiles;
import com.example.debtor_task.app.framework_code.ManageWeb;
import com.example.debtor_task.app.services.LoadToParseService;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Nikolay on 22.05.14.
 */
public class AddDebtorFragment extends FragmentManageData{

    Button butAddPhotMatter;
    Button butPhotoDeptor;
    Button butSelectPhotoDeptor;

    EditText editTextName;
    EditText editTextSurname;
    EditText editTextEmail;
    EditText editTextPhone;
    String parseDeptorId;

    boolean isHavePhoto = false;
    Intent intentLoadToParseService;
    int actionDataBas;
    ActionBar actionBar;
    ContentValues cvDeptor;
    Date d;
    InputMethodManager imm;
    Dialoger dialoger;

    private ManageFiles manageFiles;
    private ManageWeb manageWeb;
    public static final long NO_ID = -1;
    public final static int OPERRATION_ADD = 0;
    public final static int OPERRATION_REDACT = 1;
    public final static String KEY_ID = "KEY_ID";
    public final static String KEY_OPERRATIONS = "KEY_OPERRATIONS";
    public final static String KEY_DATA_TAKE = "KEY_DATA_TAKE";
    public final static String KEY_DATA_RETURN = "KEY_DATA_RETURN";
    public final static String KEY_SUMM = "KEY_SUMM";
    public final static String KEY_PATH_PHOTO_METTER = "KEY_PATH_PHOTO_METTER";

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        actionBar = ((ActionBarActivity)activity).getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.add_new_debtor));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actionDataBas = AsyncTaskWriter.INSERT;
        manageFiles = ManageFiles.getInstance();
        manageWeb = new ManageWeb();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.add_deptor_fragment, container, false);

        dialoger = new Dialoger(getActivity());

        d = new Date();
        imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imageView = (ImageView) v.findViewById(R.id.imageView);
        try{
            imageView.setImageBitmap(BitmapFactory.decodeStream(getActivity().getAssets()
                    .open(MainActivity.NO_PHOTO_FILE)));
        }catch (IOException ex){ex.printStackTrace();}

        cvDeptor = new ContentValues();
        setHasOptionsMenu(true);

        editTextName = (EditText) v.findViewById(R.id.editText);
        editTextSurname = (EditText) v.findViewById(R.id.editText2);
        editTextEmail = (EditText) v.findViewById(R.id.editText3);
        editTextPhone = (EditText) v.findViewById(R.id.editText4);

        if(getArguments().getInt(KEY_OPERRATIONS) == OPERRATION_REDACT) {
            editTextName.setText(getArguments().getString(LookDebtorFragment.KEY_NAME));
            editTextSurname.setText(getArguments().getString(LookDebtorFragment.KEY_SURNAME));
            editTextEmail.setText(getArguments().getString(LookDebtorFragment.KEY_EMAIL));
            editTextPhone.setText(getArguments().getString(LookDebtorFragment.KEY_PHONE));
            pathPhoto = getArguments().getString(LookDebtorFragment.KEY_PATH_PHOTO_DEBTOR);

            if(pathPhoto != null)
                imageView.setImageBitmap(MainActivity.getCultivatedBitmap(pathPhoto));

            actionDataBas = AsyncTaskWriter.UPDATE;
        }

        butAddPhotMatter = new Button(getActivity());

        butSelectPhotoDeptor = (Button) v.findViewById(R.id.button);
        butSelectPhotoDeptor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                createPhoto();
            }
        });

         butPhotoDeptor = (Button) v.findViewById(R.id.button2);
         butPhotoDeptor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isHavePhoto)
                    manageFiles.deleteFile(pathPhoto);

                selectPhoto();
            }
        });
        intentLoadToParseService = new Intent(getActivity(), LoadToParseService.class);
      return v;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.my_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_ok:
                imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
                if( isCorrectData(editTextName.getText().toString(),
                        editTextEmail.getText().toString())){

                    if(manageWeb.hasConnection(getActivity())){
                        ContentValues cv = new ContentValues();
                        Bundle args = new Bundle();

                        args.putString(LoadToParseService.LOAD_PARSE_NAME_DEBTOR,editTextName.getText().toString());
                        args.putString(LoadToParseService.LOAD_PARSE_SURNAME_DEBTOR, editTextSurname.getText().toString());
                        args.putString(LoadToParseService.LOAD_PARSE_EMAIL_DEBTOR, editTextEmail.getText().toString());
                        args.putString(LoadToParseService.LOAD_PARSE_PHONE_DEBTOR, editTextPhone.getText().toString());
                        args.putString(LoadToParseService.LOAD_PARSE_ID_PARSE_USER, LoginUserFragment.LOGINING_ID);
                        args.putInt(LoadToParseService.KEY_SERVICE_TASK, LoadToParseService.SERVICE_TASK_LOAD_DEBTOR);
                        args.putBoolean(LoadToParseService.LOAD_PARSE_IS_HAVE_PHOTO, isHavePhoto);
                        args.putString(LoadToParseService.LOAD_PARSE_PATH_PHOTO_DEBTOR, pathPhoto);

                        cv.put(DBHelper.D_NAME,editTextName.getText().toString());
                        cv.put(DBHelper.D_SURNAME,editTextSurname.getText().toString());
                        cv.put(DBHelper.D_EMAIL,editTextEmail.getText().toString());
                        cv.put(DBHelper.D_PHONE,editTextPhone.getText().toString());
                        cv.put(DBHelper.D_ID_PARSE, LoginUserFragment.LOGINING_ID);
                        cv.put(DBHelper.D_PATH, pathPhoto);
                        cv.put(DBHelper.D_ID_PARSE_DATA, parseDeptorId);

                        if(getArguments().getInt(KEY_OPERRATIONS) == OPERRATION_REDACT)
                            updateData(cv,""+getArguments()
                                    .getLong(KEY_ID),DBHelper.TABLE_DEPTOR,args);
                        else
                            addData(cv,DBHelper.TABLE_DEPTOR,args);
                    }else{
                        dialoger.alertDialor(getResources().getString(R.string.no_internet_connection));
                    }
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onPostLoadData(){
        super.onPostLoadData();
        getFragmentManager().popBackStackImmediate();
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public boolean isCorrectData(String name,String email) {
        if(name.equals("")){
           dialoger.alertDialor(getResources().getString(R.string.field_name_must_enter));
        }else if(!email.equals("")&&!isEmailValid(email)){
           dialoger.alertDialor(getResources().getString(R.string.no_correct_email));
        }else{
            return true;
        }
        return false;
    }

    public void onDestroy() {
        super.onDestroy();
        imm.hideSoftInputFromWindow(getActivity().getWindow().getDecorView().getWindowToken(), 0);
    }
}

