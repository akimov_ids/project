package com.example.debtor_task.app;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.example.debtor_task.app.display_fragments.AddDebtorFragment;
import com.example.debtor_task.app.display_fragments.ListDebtorFragment;
import com.example.debtor_task.app.display_fragments.LoginUserFragment;
import com.example.debtor_task.app.display_fragments.LookDebtorFragment;
import com.example.debtor_task.app.display_fragments.RegisterUserFragment;
import com.example.debtor_task.app.display_fragments.AddDebtFragment;
import com.example.debtor_task.app.display_fragments.ListDeptsFragment;
import com.example.debtor_task.app.display_fragments.LookArrearFragment;

/**
 * Created by Nikolay on 17.06.14.
 */
public class DebtorsAppMenu {

    public static final String BACK_STACK_ADD_DEPTOR = "BACK_STACK_ADD_DEPTOR";
    public static final String BACK_STACK_ADD_ARREAR = "BACK_STACK_ADD_ARREAR";
    public static final String BACK_STACK_REGISTER = "BACK_STACK_REGISTER";
    public static final String BACK_STACK_LOOK_ARREAR = "BACK_STACK_LOOK_ARREAR";
    public static final String BACK_STACK_LOOK_DEBTOR = "BACK_STACK_LOOK_DEBTOR";

    public static final int LIST_ARREARS = 1;
    public static final int LIST_DEBTORS = 2;
    public static final int REGISTRATION = 3;
    public static final int LOGINING = 4;
    public static final int ADD_DEBTOR = 5;
    public static final int ADD_ARREAR = 6;
    public static final int LOOK_DEBTOR = 7;
    public static final int LOOK_ARREARS = 8;

    private MyFragmentManager myFragmentManager;

    public DebtorsAppMenu(FragmentManager fragmentManager){
        myFragmentManager = MyFragmentManager.getInstance();
    }

    public void goToItem(int selectedItem, Bundle bundle){
        myFragmentManager.replace(getFragment(selectedItem), R.id.frgmCont, bundle);
    }

    public void goToItem(int selectedItem, Bundle bundle, String backStack){
        myFragmentManager.replace(getFragment(selectedItem), R.id.frgmCont, bundle,backStack);
    }

    private Fragment getFragment(int selectedItem){
        switch(selectedItem){
            case ADD_DEBTOR:
                return new AddDebtorFragment();

            case ADD_ARREAR:
                return new AddDebtFragment();

            case LOGINING:
                return new LoginUserFragment();

            case REGISTRATION:
                return new RegisterUserFragment();

            case LIST_ARREARS:
                return new ListDeptsFragment();

            case LIST_DEBTORS:
                return new ListDebtorFragment();

            case LOOK_DEBTOR:
                return new LookDebtorFragment();

            case LOOK_ARREARS:
                return new LookArrearFragment();
            default:
                return null;
        }
    }
}
