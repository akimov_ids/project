package com.example.debtor_task.app.display_fragments;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.framework_code.Dialoger;
import com.example.debtor_task.app.framework_code.ManageWeb;
import com.example.debtor_task.app.services.LoginRegisterService;

/**
 * Created by Николай on 23.05.14.
 */
public class RegisterUserFragment extends Fragment {

    EditText editTextLogin;
    EditText editTextPass;
    EditText editTextRepass;
    EditText editTextEmail;
    Button butGoRegister;
    BroadcastReceiver br;
    ProgressDialog pd;
    Dialoger dialoger;

    private ManageWeb manageWeb;

    public static final int BORADCAST_SUCCESSFUL_REGISTRATION = 0;
    public static final int BORADCAST_ALREADY_REGISTER_LOGIN = 1;
    public static final String RESULT_MASSAGE = "RESULT_MASSAGE";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        manageWeb = new ManageWeb();


        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                pd.cancel();
                dialoger.alertDialor(intent.getStringExtra(RESULT_MASSAGE));

                if (intent.getIntExtra(MainActivity.KEY_TASK_BROADCASST,-1)
                        == BORADCAST_SUCCESSFUL_REGISTRATION) {
                    getActivity().getSupportFragmentManager().popBackStackImmediate();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter(MainActivity.ACTION_BROADCASST_REGISTER);
        getActivity().registerReceiver(br, intentFilter);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.register_user_fragment, container, false);

        dialoger = new Dialoger(getActivity());
        editTextLogin = (EditText) v.findViewById(R.id.editText);
        editTextPass = (EditText) v.findViewById(R.id.editText2);
        editTextRepass = (EditText) v.findViewById(R.id.editText3);
        editTextEmail = (EditText) v.findViewById(R.id.editText4);

        pd = new ProgressDialog(getActivity());
        pd.setTitle(getResources().getString(R.string.logining));
        pd.setMessage(getResources().getString(R.string.wait));
        pd.setCanceledOnTouchOutside(false);
        pd.setCancelable(false);

        butGoRegister = (Button) v.findViewById(R.id.button);
        butGoRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isCorrectEnterData()){
                    if(manageWeb.hasConnection(getActivity())){
                        pd.show();
                        Intent intent = new Intent(getActivity(), LoginRegisterService.class);

                        intent.putExtra(LoginRegisterService.LOGINED_PARSE_USERNAME,editTextLogin.getText().toString())
                                .putExtra(LoginRegisterService.LOGINED_PARSE_PASS, editTextPass.getText().toString())
                                .putExtra(LoginRegisterService.KEY_SERVICE_TASK, LoginRegisterService.SERVICE_TASK_REGISTRATION);

                        getActivity().startService(intent);
                    }else{
                        dialoger.alertDialor(getResources().getString(R.string.no_internet_connection));
                    }
                }
            }
        });

        return v;
    }

    boolean isCorrectEnterData(){

        boolean pass = editTextPass.getText().toString().equals(
                editTextRepass.getText().toString());

        boolean filledField = !(editTextLogin.getText().toString().equals("")||
                                editTextPass.getText().toString().equals("")||
                                editTextRepass.getText().toString().equals(""));

        if(filledField){
            if(pass)
                return true;
            else
                dialoger.alertDialor(getResources().getString(R.string.password_not_match));
        }else{
            dialoger.alertDialor(getResources().getString(R.string.enter_all_values));
        }
        return false;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onDestroyView() {
        super.onDestroyView();
        getActivity().unregisterReceiver(br);
    }


}

