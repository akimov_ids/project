package com.example.debtor_task.app.framework_code;

import android.content.Context;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;


import com.example.debtor_task.app.R;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Nikolay on 02.07.14.
 */
public class ManageImage {
    private static ManageImage manageImage = new ManageImage();
    private Context context;
    private Resources resources;

    public static ManageImage getInstance() {
        return manageImage;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }
    /* //==OLD VERSION==//
    public static Bitmap getCultivatedBitmap(String pathImage) {

        File image = new File(pathImage);
        int scale = 1;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(image.getAbsolutePath(),options);

        if(options.outWidth > options.outHeight){
            scale = options.outWidth/400;
        }else{
            scale = options.outHeight/400;
        }

        ExifInterface exif;
        int orientation = -1;
        try{
            exif = new ExifInterface(image.getAbsolutePath());
            orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, 1);
        }catch (IOException ex){ex.printStackTrace();}

        options = new BitmapFactory.Options();
        options.inSampleSize = scale;
        options.inPreferredConfig = Bitmap.Config.RGB_565;

        Bitmap bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);
        Matrix matrix = new Matrix();

        switch(orientation){
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.postRotate(90);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.postRotate(180);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.postRotate(270);
                break;
        }
        bmp = Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        return bmp;
    }*/

    /*public static Bitmap getCultivatedBitmap(String pathImage) {

        File image = new File(pathImage);
        Bitmap bmp = null;

        if (image.exists()&&image.isFile()&&image.canRead()) {
            int scale = 1;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(image.getAbsolutePath(),options);

            if (options.outWidth > options.outHeight) {
                scale = options.outWidth/400;
            } else {
                scale = options.outHeight/400;
            }

            ExifInterface exif;
            int orientation = -1;
            try {
                exif = new ExifInterface(image.getAbsolutePath());
                orientation = exif.getAttributeInt( ExifInterface.TAG_ORIENTATION, 1);
            } catch (IOException ex){ex.printStackTrace();}

            options = new BitmapFactory.Options();
            options.inSampleSize = scale;
            options.inPreferredConfig = Bitmap.Config.RGB_565;

            bmp = BitmapFactory.decodeFile(image.getAbsolutePath(),options);
            if (bmp != null && !bmp.isRecycled()) {
                Matrix matrix = new Matrix();
                switch(orientation){
                    case ExifInterface.ORIENTATION_ROTATE_90:
                        matrix.postRotate(90);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_180:
                        matrix.postRotate(180);
                        break;
                    case ExifInterface.ORIENTATION_ROTATE_270:
                        matrix.postRotate(270);
                        break;
                }
                return Bitmap.createBitmap(bmp , 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
            }
        }
        return BitmapFactory.decodeStream(istr);
    }*/
    //==============================================================================================//

    public Bitmap getBitmap(InputStream istr, int resId){
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        if (bitmap!=null) {
            return bitmap;
        } else {
            return getFailPhoto(resId);
        }
    }

    public Bitmap getFailPhoto(int resId){
        return BitmapFactory.decodeResource(resources, resId);
    }
}
