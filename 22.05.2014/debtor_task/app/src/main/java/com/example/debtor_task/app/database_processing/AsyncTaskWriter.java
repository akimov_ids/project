package com.example.debtor_task.app.database_processing;

import android.content.ContentValues;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.debtor_task.app.DBHelper;

/**
 * Created by Николай on 27.05.14.
 */
public class AsyncTaskWriter extends AsyncTask<Integer, Void, Long> {

    public final static int INSERT = 0;
    public final static int UPDATE = 1;
    public final static int DELETE = 2;
    public final static int INSERT_ARREARS_FROM_PARSE = 3;
    public final static int INSERT_DEBTORS_FROM_PARSE = 4;

    int KEY;
    public static final String SELECT_COLUMN = "COL";
    public static final String SELECT_ARG = "ARG";

    ContentValues cv;
    String tableQuery;
    Bundle args;
    String[] selectedArg;
    private static DBHelper dbHelper;

    public static void setDataBase(Context context, String databaseName){
        dbHelper = new DBHelper(context,databaseName,null,1);
    }

    public void insert(ContentValues cv, String tableQuery) {
        this.cv = cv;
        this.tableQuery = tableQuery;
        this.execute(new Integer[]{INSERT});
    }

    public void update(ContentValues cv, String tableQuery,String[] selectedArg) {
        this.cv = cv;
        this.tableQuery = tableQuery;
        this.selectedArg = selectedArg;
        this.execute(new Integer[]{UPDATE});
    }

    public void delete(Bundle args, String tableQuery) {
        this.args = args;
        this.tableQuery = tableQuery;
        this.execute(new Integer[]{DELETE});
    }


    @Override
    protected Long doInBackground(Integer... key) {
        long id = -1;

        switch(key[0]){
            case INSERT:
                id =  dbHelper.getWritableDatabase().insert(tableQuery,null,cv);
                dbHelper.close();
                cv.clear();
                break;
            case UPDATE:
                id =  dbHelper.getWritableDatabase().update(tableQuery,cv,"_id = ?",selectedArg);
                break;
            case DELETE:
                id =  dbHelper.getWritableDatabase().delete(tableQuery,args.getString(SELECT_COLUMN)+" = ?",
                                                                 args.getStringArray(SELECT_ARG));
                break;
        }
        return id;
    }
}
