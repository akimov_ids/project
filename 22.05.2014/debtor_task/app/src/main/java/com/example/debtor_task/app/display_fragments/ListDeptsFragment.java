package com.example.debtor_task.app.display_fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.DebtorsAppMenu;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.MyCursorLoader;
import com.example.debtor_task.app.framework_code.FragmentManageDataList;
import com.example.debtor_task.app.framework_code.ListInitializer;
import com.example.debtor_task.app.framework_code.ProcessingDate;
import com.example.debtor_task.app.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class ListDeptsFragment extends FragmentManageDataList{

    ImageView imageView;
    DisplayImageOptions options;
    ListView listView;
    ImageLoader imageLoader;
    Bundle argss;
    DebtorsAppMenu debtorsAppMenu;
    ProcessingDate processingDate;
    BroadcastReceiver br;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        imageLoader = ImageLoader.getInstance();
        imageLoader.init(ImageLoaderConfiguration.createDefault(activity));
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        processingDate = new ProcessingDate();

        argss = new Bundle();
        argss.putString(MyCursorLoader.SELECTION,DBHelper.MM_ID_PARSE);
        argss.putStringArray(MyCursorLoader.SELECT_ARG,new String[]{LoginUserFragment.LOGINING_ID});
        argss.putString(MyCursorLoader.SELECT_COLUMN,MyCursorLoader.JOIIN_METTER+DBHelper.MM_ID_PARSE);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_deptor_fragment, container, false);

        setHasOptionsMenu(true);
        debtorsAppMenu = new DebtorsAppMenu(getActivity().getSupportFragmentManager());
        imageView = (ImageView) v.findViewById(R.id.imageView);
        options = new DisplayImageOptions.Builder().showImageOnFail(R.drawable.money).build();
        listView = (ListView) v.findViewById(R.id.listView);

        br = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                recompleteList(listView, argss, DBHelper.TABLE_MANY_METTER, MyCursorLoader.JOIN_QUERY_ARG_N);
            }
        };
        IntentFilter intentFilter = new IntentFilter(MANAGER_DATA_BROADCASST_IS_UPDATE);
        getActivity().registerReceiver(br, intentFilter);

        return v;
    }

    public void onResume() {
        super.onResume();
        setInitializer(new ListInitializerArrears());
        completeList(listView,argss,DBHelper.TABLE_MANY_METTER,MyCursorLoader.JOIN_QUERY_ARG_N);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                args.putLong(LookArrearFragment.KEY_ID_MM, id);
                debtorsAppMenu.goToItem(DebtorsAppMenu.LOOK_ARREARS,args);
            }
        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()){
            case R.id.to_add_dabtor_from_contacts:
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                getActivity().getSupportLoaderManager().destroyLoader(0);
                startActivityForResult(intent, MainActivity.REQUEST_ACTION_PICK);
                return true;

            case R.id.to_add_arrears:
                Bundle args = new Bundle();
                args.putLong(AddDebtorFragment.KEY_ID,AddDebtorFragment.NO_ID);
                args.putInt(AddDebtorFragment.KEY_OPERRATIONS, AddDebtorFragment.OPERRATION_ADD);
                debtorsAppMenu.goToItem(DebtorsAppMenu.ADD_ARREAR,args);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView(){
        super.onDestroyView();
        getActivity().unregisterReceiver(br);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.list_arrears_menu, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    class ListInitializerArrears implements ListInitializer {

        TextView textViewName;
        TextView textViewTakeDate;
        TextView textViewReturnDate;
        ImageView img;

        @Override
        public void initialize(Cursor cursor) {
            textViewName.setText(cursor.getString(cursor.getColumnIndex(DBHelper.D_NAME)));
            textViewTakeDate.setText(processingDate.getStringData(Long.parseLong(cursor.getString(cursor.getColumnIndex(DBHelper.MM_DATE_TAKE)))));
            textViewReturnDate.setText(processingDate.getStringData(Long.parseLong(cursor.getString(cursor.getColumnIndex(DBHelper.MM_DATE_RETURN)))));
        }

        @Override
        public void setView(View view) {
            img = (ImageView)view.findViewById(R.id.image);
            textViewName = (TextView)view.findViewById(R.id.text);
            textViewTakeDate = (TextView)view.findViewById(R.id.textView);
            textViewReturnDate = (TextView)view.findViewById(R.id.textView2);
        }

        @Override
        public TextView getTextView() {
            return textViewName;
        }

        @Override
        public ImageView getImageView() {
            return img;
        }

        @Override
        public String getColumn() {
            return DBHelper.MM_PATH_PHOTO_MATTER;
        }
    }
}

