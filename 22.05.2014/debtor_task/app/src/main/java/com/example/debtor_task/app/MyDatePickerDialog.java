package com.example.debtor_task.app;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Николай on 30.05.14.
 */
public interface MyDatePickerDialog extends DatePickerDialog.OnDateSetListener {
    public final int ACTION_SET_TAKE_DATE = 0;
    public final int ACTION_SET_RETURN_DATE = 1;
    public void setView(TextView view);
}
