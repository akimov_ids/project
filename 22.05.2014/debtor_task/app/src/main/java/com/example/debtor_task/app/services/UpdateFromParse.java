package com.example.debtor_task.app.services;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.util.Log;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.display_fragments.LoginUserFragment;
import com.example.debtor_task.app.framework_code.Constants;
import com.example.debtor_task.app.framework_code.FragmentManageData;
import com.example.debtor_task.app.framework_code.ManageWeb;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Nikolay on 11.06.14.
 */
public class UpdateFromParse  extends Service {

    final String LOG_TAG = "myLogs";

    public final static String ARRAY_LIST_ARREARS_FROM_DB = "ARRAY_LIST_ARREARS_FROM_DB";
    public final static String ARRAY_LIST_DEBTORS_FROM_DB = "ARRAY_LIST_DEBTORS_FROM_DB";

    int count;

    static final String UTF8 = "utf-8";
    static final String PARSE_COLL_OBJECT_ID = "objectId";

    static final String PARSE_COLL_EMAIL = "email";
    static final String PARSE_COLL_NAME = "username";
    static final String PARSE_COLL_SURNAME = "surname";
    static final String PARSE_COLL_PHONE = "phone";
    static final String PARSE_COLL_ID_USER = "id_parse_user";

    static final String PARSE_COLL_DATE_RETURN = "dateReturn";
    static final String PARSE_COLL_DATE_TAKE = "dateTake";
    static final String PARSE_COLL_ID_USER_PARSE_DEBTOR = "idParseDebtor";
    static final String PARSE_COLL_SUMM = "summ";
    static final String PARSE_COLL_PATH_PHOTO = "path_photo";
    static final String PARSE_COLL_PATH_PHOTO_METTER = "path_photo_metter";

    String pathPhoto = "";
    String pathPhotoFolder = Environment.getExternalStorageDirectory()+"/myfotodeb";

    ContentValues cv;
    private DBHelper dbHelper;

    ExecutorService es;

    JSONObject response;
    JSONArray responseDebtorsArr;
    JSONArray responseArrearsArr;
     ManageWeb manageWeb;

    HashMap<String,Long> idMap = new HashMap<String, Long>();
    ArrayList<String> arrayListArrearsFromDB = new ArrayList<String>();
    ArrayList<String> arrayListDebtorsFromDB = new ArrayList<String>();


    public void onCreate() {
        super.onCreate();
    }

    public int onStartCommand(Intent intent, int flags, int startId) {

        cv = new ContentValues();
        dbHelper = new DBHelper(this,DBHelper.DB_NAME,null,1);

        UpdateRunnable updateRunnable = new UpdateRunnable();
        arrayListArrearsFromDB = intent.getStringArrayListExtra(ARRAY_LIST_ARREARS_FROM_DB);
        arrayListDebtorsFromDB = intent.getStringArrayListExtra(ARRAY_LIST_DEBTORS_FROM_DB);

        manageWeb = new ManageWeb(Constants.PARSE_ID,Constants.PARSE_KEY);

        es = Executors.newFixedThreadPool(1);
        es.execute(updateRunnable);

        return Service.START_NOT_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        es.shutdown();
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

   class UpdateRunnable implements Runnable{

        @Override
        public void run() {
            responseDebtorsArr =
                    manageWeb.loadObject("https://api.parse.com/1/classes/debtors?where=", LoginUserFragment.LOGINING_ID);
            responseArrearsArr =
                    manageWeb.loadObject("https://api.parse.com/1/classes/arrears?where=", LoginUserFragment.LOGINING_ID);
        load();
        stopSelf();
    }

        public void load(){

            String name;
            ContentValues cvvArrears = new ContentValues();

            try{
                for(int i = 0; i<responseDebtorsArr.length(); i++){
                    response = responseDebtorsArr.optJSONObject(i);

                    name = response.getString(PARSE_COLL_OBJECT_ID);
                    if(!arrayListDebtorsFromDB.contains(name)){
                           if(!response.isNull(PARSE_COLL_PATH_PHOTO)){
                                URL url = new URL(response.getString(PARSE_COLL_PATH_PHOTO));
                                URLConnection conection = url.openConnection();
                                conection.connect();
                                InputStream input = new BufferedInputStream(url.openStream(),
                                        1024);
                                pathPhoto = pathPhotoFolder+"/"+(new Date().getTime())+".jpg";
                                OutputStream output = new FileOutputStream(pathPhoto);

                                byte data[] = new byte[1024];
                                long total = 0;

                                while ((count = input.read(data)) != -1) {
                                    total += count;
                                    output.write(data, 0, count);
                                }

                                output.flush();
                                output.close();
                                input.close();

                                cv.put(DBHelper.D_PATH,pathPhoto);
                            }

                            cv.put(DBHelper.D_EMAIL,response.getString(PARSE_COLL_EMAIL));
                            cv.put(DBHelper.D_NAME,response.getString(PARSE_COLL_NAME));
                            cv.put(DBHelper.D_SURNAME,response.getString(PARSE_COLL_SURNAME));
                            cv.put(DBHelper.D_PHONE,response.getString(PARSE_COLL_PHONE));
                            cv.put(DBHelper.D_ID_PARSE, response.getString(PARSE_COLL_ID_USER));
                            cv.put(DBHelper.D_ID_PARSE_DATA, name);

                            idMap.put(name,
                                    dbHelper.getWritableDatabase().insert(DBHelper.TABLE_DEPTOR,
                                            null,cv));
                    }
                }


                for(int i = 0; i<responseArrearsArr.length(); i++){
                    response = responseArrearsArr.optJSONObject(i);
                    name = response.getString(PARSE_COLL_OBJECT_ID);
                    if(!arrayListArrearsFromDB.contains(name)){
                        Log.d("","");
                        if(response.getString(PARSE_COLL_ID_USER).
                                equals(LoginUserFragment.LOGINING_ID)){
                            Log.d("","");

                            if(!response.isNull(PARSE_COLL_PATH_PHOTO_METTER)){
                                URL url = new URL(response.getString(PARSE_COLL_PATH_PHOTO_METTER));
                                URLConnection conection = url.openConnection();
                                conection.connect();
                                InputStream input = new BufferedInputStream(url.openStream(),
                                        1024);

                                pathPhoto = pathPhotoFolder+"/"+(new Date().getTime())+".jpg";
                                OutputStream output = new FileOutputStream(pathPhoto);

                                byte data[] = new byte[1024];
                                long total = 0;

                                while ((count = input.read(data)) != -1) {
                                    total += count;
                                    output.write(data, 0, count);
                                }

                                output.flush();
                                output.close();
                                input.close();}

                            cvvArrears.clear();
                            cvvArrears.put(DBHelper.MM_DATE_RETURN,
                                    response.getString(PARSE_COLL_DATE_RETURN));
                            cvvArrears.put(DBHelper.MM_DATE_TAKE,
                                    response.getString(PARSE_COLL_DATE_TAKE));

                            cvvArrears.put(DBHelper.MM_ID_DEBTOR,
                                    idMap.get(response.getString(PARSE_COLL_ID_USER_PARSE_DEBTOR)));

                            cvvArrears.put(DBHelper.MM_ID_PARSE,
                                    LoginUserFragment.LOGINING_ID);

                            if(response.isNull(PARSE_COLL_SUMM))
                                cvvArrears.put(DBHelper.MM_PATH_PHOTO_MATTER,pathPhoto);
                            else
                                cvvArrears.put(DBHelper.MM_SUMM,response.getString(PARSE_COLL_SUMM));
                            dbHelper.getWritableDatabase().insert(DBHelper.TABLE_MANY_METTER,null,cvvArrears);
                        }
                    }
                }
                Intent intent = new Intent(FragmentManageData.MANAGER_DATA_BROADCASST_IS_UPDATE);
                intent.putExtra(MainActivity.KEY_TASK_BROADCASST,
                        FragmentManageData.MANAGER_DATA_BROADCASST_IS_UPDATE);

                sendBroadcast(intent);

            }catch (JSONException ex){
                ex.printStackTrace();
            }catch (IOException ex){
                ex.printStackTrace();
            }
        }



    }
}