package com.example.debtor_task.app.display_fragments;

//import android.app.ActionBar;
import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.debtor_task.app.DBHelper;
import com.example.debtor_task.app.DebtorsAppMenu;
import com.example.debtor_task.app.MainActivity;
import com.example.debtor_task.app.R;
import com.example.debtor_task.app.database_processing.AsyncTaskWriter;
import com.example.debtor_task.app.framework_code.LookDataFragment;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Nikolay on 22.05.14.
 */
public class LookDebtorFragment  extends LookDataFragment {

    public final static String KEY_ID_DEPTOR = "key_id_deptor";
    public final static String KEY_NAME = "KEY_NAME";
    public final static String KEY_SURNAME = "KEY_SURNAME";
    public final static String KEY_EMAIL = "KEY_EMAIL";
    public final static String KEY_PHONE = "KEY_PHONE";
    public final static String KEY_PATH_PHOTO_DEBTOR = "PATH_PHOTO_DEBTOR";

    private TextView textViewName;
    private TextView textViewSurname;
    private TextView textViewEmail;
    private TextView textViewPhone;

    ImageView imageView;
    String pathPhoto;
    String idParseData;
    DebtorsAppMenu debtorsAppMenu;
    Bundle args;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        debtorsAppMenu = new DebtorsAppMenu(getActivity().getSupportFragmentManager());
    }

    @Override
    protected void onPostLoadData(){
        super.onPostLoadData();
        getFragmentManager().popBackStackImmediate();
    }

    @Override
    public void takeData(Cursor data) {
        data.moveToFirst();

        textViewName.setText(data.getString(data.getColumnIndex(DBHelper.D_NAME)));
        textViewSurname.setText(data.getString(data.getColumnIndex(DBHelper.D_SURNAME)));
        textViewEmail.setText(data.getString(data.getColumnIndex(DBHelper.D_EMAIL)));
        textViewPhone.setText(data.getString(data.getColumnIndex(DBHelper.D_PHONE)));
        idParseData = data.getString(data.getColumnIndex(DBHelper.D_ID_PARSE_DATA));

        pathPhoto = data.getString(data.getColumnIndex(DBHelper.D_PATH));

        if(pathPhoto==null||pathPhoto.equals("")) {
            InputStream istr;
            Bitmap bitmap = null;
            try {
                istr = getActivity().getAssets().open(MainActivity.NO_PHOTO_FILE);
                bitmap = BitmapFactory.decodeStream(istr);
                imageView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else {
            imageView.setImageBitmap(MainActivity.getCultivatedBitmap(data.getString(data.getColumnIndex(DBHelper.D_PATH))));
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.look_debtor_fragment, container, false);
        imageView = (ImageView)v.findViewById(R.id.imageView);

        textViewName = (TextView) v.findViewById(R.id.textView);
        textViewSurname = (TextView) v.findViewById(R.id.textView2);
        textViewEmail = (TextView) v.findViewById(R.id.textView3);
        textViewPhone = (TextView) v.findViewById(R.id.textView4);
        actionBar.setTitle(getString(R.string.debtor));
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public void onResume() {
        super.onResume();
        args = new Bundle();
        args.putString(LookDataFragment.SELECTION,DBHelper.D_ID);
        args.putStringArray(LookDataFragment.SELECT_ARG,new String[]{""+getArguments().getLong(KEY_ID_DEPTOR)});
        args.putStringArray(LookDataFragment.SELECT_COLUMN,new String[]{DBHelper.D_PATH,
                DBHelper.D_SURNAME,
                DBHelper.D_NAME,
                DBHelper.D_EMAIL,
                DBHelper.D_PHONE,
                DBHelper.D_ID_PARSE_DATA});

        getData(args, DBHelper.TABLE_DEPTOR, LookDataFragment.QUERY_ARG);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.look_item, menu);
        super.onCreateOptionsMenu(menu,inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.look_redact) {

            args.clear();
            args.putLong(AddDebtorFragment.KEY_ID, getArguments()
                    .getLong(KEY_ID_DEPTOR));
            args.putString(KEY_NAME,textViewName.getText().toString());
            args.putString(KEY_SURNAME,textViewSurname.getText().toString());
            args.putString(KEY_EMAIL,textViewEmail.getText().toString());
            args.putString(LookDebtorFragment.KEY_PHONE,textViewPhone.getText().toString());
            args.putString(KEY_PATH_PHOTO_DEBTOR,pathPhoto);

            args.putInt(AddDebtorFragment.KEY_OPERRATIONS, AddDebtorFragment.OPERRATION_REDACT);
            debtorsAppMenu.goToItem(DebtorsAppMenu.ADD_DEBTOR,args);


            return true;
        }else if(id == R.id.look_delete){
            deleteData(getArguments().getLong(KEY_ID_DEPTOR),DBHelper.TABLE_DEPTOR,idParseData);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
