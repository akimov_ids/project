package com.example.task1new.app;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;

public class SecondActivitys extends ActionBarActivity implements onSomeEventListener{

    FragmentOne frag1;
    FragmentTwo frag2;
    FragmentTransaction fTrans;
    CheckBox chbStack;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second_activitys);

        frag1 = new FragmentOne();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.fr_one, frag1);
        fTrans.commit();

        frag2 = new FragmentTwo();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.fr_two, frag2);
        fTrans.commit();


    }

    @Override
    public void someEvent(String s) {
        
    }
}