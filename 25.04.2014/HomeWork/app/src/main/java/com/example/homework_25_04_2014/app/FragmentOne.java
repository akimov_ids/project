package com.example.homework_25_04_2014.app;

import android.content.Context;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.support.v4.app.Fragment;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Николай on 21.04.14.
 */
public class FragmentOne extends Fragment {

    String htmlContent = "";
    final String LOG_TAG = "myLogs";
    long size;
    String resString;
    Handler h;
    Activity activ;
    static final int IDD__HORIZONTAL_PROGRESS = 0;
    static final int IDD_WHEEL_PROGRESS = 1;
    static final int IDD_THREAD_PROGRESS = 2;

    private ProgressDialog mProgressDialog;
    private ProgressThread progressThread;

    Message msg;

    public interface onSomeEventListener {
        public void someEvent(String s);
    }
    onSomeEventListener someEventListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activ = activity;
        try {
            someEventListener = (onSomeEventListener) activity;
            mProgressDialog = new ProgressDialog(activity);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setMessage("Загружаю. Подождите...");
            activ.showDialog(IDD_THREAD_PROGRESS);
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement onSomeEventListener");
        }catch(Exception ex){}
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_one, null);

        Button button = (Button) v.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mProgressDialog.setProgress(0);
                mProgressDialog.show();
                progressThread = new ProgressThread(handler);
                progressThread.start();
            }
        });

        return v;
    }

    final Handler handler = new Handler() {
        int total;
        public void handleMessage(Message msg) {
            total = msg.arg1;
            if (total > 0){
                progressThread.setState(ProgressThread.STATE_DONE);
                mProgressDialog.dismiss();
                someEventListener.someEvent(resString);
            }
        }
    };


    private class ProgressThread extends Thread {
        Handler mHandler;
        final static int STATE_DONE = 0;
        final static int STATE_RUNNING = 1;
        int mState;
        int total;
        InputStream in = null;

        ProgressThread(Handler h) {
            mHandler = h;
        }

        public void run() {
            try {
                //OAFDBDE5A3
                HttpClient httpclient = new DefaultHttpClient();
                HttpGet httpget = new HttpGet("http://zvirec.com/");
                HttpResponse response = httpclient.execute(httpget);
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "cp1251"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;

                size= entity.getContentLength();

                while ((line = reader.readLine()) != null){
                    sb.append(line + "\n");

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        Log.e("ERROR", "Thread Interrupted");
                    }

                    msg = mHandler.obtainMessage();
                    msg.arg1 = 0;
                    mHandler.sendMessage(msg);

                }

                msg = mHandler.obtainMessage();
                msg.arg1 = 1;
                mHandler.sendMessage(msg);

                resString = sb.toString();
                Log.d("myTag", resString);
                is.close();

        }catch (Exception ex){
                Log.d("myTag", "Error = "+ex.getMessage());
            }
        }

        public void setState(int state) {
            mState = state;
        }
    }
}
