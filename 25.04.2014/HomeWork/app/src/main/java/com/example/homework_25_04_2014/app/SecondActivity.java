package com.example.homework_25_04_2014.app;

//import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v4.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class SecondActivity extends ActionBarActivity implements FragmentOne.onSomeEventListener {

        ProgressDialog pd;
        Handler h;

@Override
protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

       Fragment frag2 = new FragmentOne();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.frag, frag2);
        ft.commit();
    }


@Override
public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_activity2, menu);
        return true;
        }

@Override
public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
        return true;
        }
        return super.onOptionsItemSelected(item);
        }

@Override
public void someEvent(String s) {
    Log.d("MY_TAG", "String = " + s);

    Intent intent = new Intent();
    intent.putExtra(MainActivity.KEY_PAGE, s);
    setResult(RESULT_OK, intent);
    finish();
    }
}
