package com.example.task1new.app;

import android.annotation.TargetApi;
//import android.app.Fragment;
//import android.app.FragmentTransaction;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends ActionBarActivity implements MyListener{

    FragmentOne frag1;
    FragmentTwo frag2;
    FragmentTransaction fTrans;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        frag1 = new FragmentOne();
        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmContt, frag1);
        fTrans.addToBackStack("one");
        fTrans.commit();
    }

    @Override
    public void myEvent(Fragment one, Fragment two) {
      /*  fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.remove(one);
        fTrans.commit();



        fTrans = getSupportFragmentManager().beginTransaction();
        fTrans.add(R.id.frgmContt, two);
        fTrans.commit();*/
    }

}