package com.example.task1new.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by Nikolay on 25.04.14.
 */
public class FragmentTwo extends Fragment {

    FragmentTransaction fTrans;

    final String LOG_TAG = "myLogs";
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onCreate");
    }

    public interface onSomeEventListener {
        public void someEvent(String s);
    }

    onSomeEventListener someEventListener;
    MyListener myListener;
    //onSomeEventListener someEventListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            //someEventListener = (onSomeEventListener) activity;
            myListener = (MyListener)activity;
        } catch (ClassCastException e) {
            System.err.println("Error = "+activity.toString() + " must implement onSomeEventListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(LOG_TAG, "Fragment1 onCreateView");
        View v = inflater.inflate(R.layout.fragment_two, null);
        //editText = (EditText) v.findViewById(R.id.editText);
        Button button = (Button) v.findViewById(R.id.button);
        //EditText editText = (EditText) v.findViewById(R.id.editText);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //getFragmentManager().beginTransaction().replace(R.id.frgmContt,new FragmentThree());

                //FragmentTransaction fTrans;

                fTrans = getFragmentManager().beginTransaction();
                fTrans.replace(R.id.frgmContt, new FragmentThree());
                fTrans.addToBackStack("three");
                fTrans.commit();

                //myListener.myEvent(FragmentTwo.this,new FragmentThree());
           }
        });
        return v;
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(LOG_TAG, "Fragment1 onActivityCreated");
    }
}